codeunit 70328077 "i95 Pull Webservice"
{

    procedure ProcessPullData(CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID)
    var
        i95ProcessPulledDataCU: Codeunit "i95 Process Pulled Data";
        BodyContent: Text;
    begin
        Clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        clear(APILogEntryNo);
        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := i95SyncLogEntry.InsertSyncLogEntry(CurrentAPIType, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::i95);
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, CurrentAPIType, SchedulerType::PullData, SyncSource::i95);

        CreateBodyContent.AddContextHeader(BodyContent, 'PullData');
        CreateBodyContent.AddContextFooter(BodyContent);

        i95SyncLogEntry.reset();
        if i95SyncLogEntry.get(SyncLogEntryNo) then;
        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        i95SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(CurrentAPIType, SchedulerType::PullData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        case CurrentAPIType of
            CurrentAPIType::Customer:
                PullDataSource := PullDataSource::Customer;
            CurrentAPIType::CustomerGroup:
                PullDataSource := PullDataSource::CustomerGroup;
            CurrentAPIType::SalesOrder:
                PullDataSource := PullDataSource::SalesOrder;
            CurrentAPIType::Product:
                PullDataSource := PullDataSource::Product;
            CurrentAPIType::SchedulerID:
                PullDataSource := PullDataSource::SchedulerID;
        end;

        i95WebserviceExecuteCU.SetPullDataSource(PullDataSource);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Sync", LogStatus::"In Progress", CurrentAPIType);

        if CurrentAPIType = CurrentAPIType::SchedulerID then begin
            Commit();
            i95ProcessPulledDataCU.SetParamaters(SyncLogEntryNo);
            if i95ProcessPulledDataCU.Run() then;
        end;

    end;

    procedure ProcessPushResponse(CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID)
    var
        Customer: Record Customer;
        CustomerPriceGroup: Record "Customer Price Group";
        SalesHeader: Record "Sales Header";
        Item: Record Item;
        BodyContent: Text;
    begin
        Clear(i95WebserviceExecuteCU);
        i95Setup.get();
        Checki95SetupData();

        i95WebserviceExecuteCU.GetAPIUrl(CurrentAPIType, SchedulerType::PushResponse);
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, CurrentAPIType, SchedulerType::PushResponse, SyncSource::i95);

        i95SyncLogEntry.reset();
        if i95SyncLogEntry.get(SyncLogEntryNo) then;

        CreateBodyContent.AddContextHeader(BodyContent, 'PushResponse');
        DetailedSyncLogEntry.Reset();
        DetailedSyncLogEntry.SetCurrentKey("API Type", "Sync Log Entry No", "Sync Status");
        DetailedSyncLogEntry.SetRange("API Type", CurrentAPIType);
        DetailedSyncLogEntry.SetRange("Sync Log Entry No", i95SyncLogEntry."Entry No");
        DetailedSyncLogEntry.SetRange("Sync Status", DetailedSyncLogEntry."Sync Status"::"Waiting for Response");
        if DetailedSyncLogEntry.FindSet() then
            repeat
                case CurrentAPIType of
                    currentAPIType::Customer:
                        if Customer.get(DetailedSyncLogEntry."Source Record ID") then
                            CreateBodyContent.CustomerPushResponseData(Customer, BodyContent, DetailedSyncLogEntry);
                    CurrentAPIType::CustomerGroup:
                        if CustomerPriceGroup.get(DetailedSyncLogEntry."Source Record ID") then
                            CreateBodyContent.CustomerPrieGroupPushResponseData(CustomerPriceGroup, BodyContent, DetailedSyncLogEntry);
                    CurrentAPIType::SalesOrder:
                        If SalesHeader.get(DetailedSyncLogEntry."Source Record ID") then
                            CreateBodyContent.SalesOrderPushResponseData(SalesHeader, BodyContent, DetailedSyncLogEntry);
                    CurrentAPIType::Product:
                        If Item.get(DetailedSyncLogEntry."Source Record ID") then
                            CreateBodyContent.ProductPushResponseData(Item, BodyContent, DetailedSyncLogEntry);
                end;
            Until DetailedSyncLogEntry.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        i95SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Response Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        commit();

        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Sync Complete", LogStatus::Completed, CurrentAPIType);
    end;

    procedure SetSynclogEntryNo(EntryNo: Integer);
    begin
        SyncLogEntryNo := EntryNo;
    end;

    procedure Checki95SetupData()
    begin
        i95Setup.TestField("Base Url");
        i95Setup.TestField("Client ID");
        i95Setup.testfield("Subscription Key");
        i95Setup.TestField("Endpoint Code");
        i95Setup.TestField(Authorization);
    end;


    procedure SetTestMode(TestModeFlag: Boolean)
    begin
        TestMode := TestModeFlag;
    end;

    procedure SetMockResponseText(MockResponseText: Text)
    begin
        TestModeResponseTxt := MockResponseText;
    end;

    Procedure ExecuteAPIAndUpdateLogs(CurrentSyncStatus: Integer;
                                    CurrentLogStatus: Option " ",New,"In Progress",Completed,Error;
                                    CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID)
    var
        SourceRecordID: RecordId;
    begin
        if i95WebserviceExecuteCU.run() then begin
            HttpReasonCode := i95WebserviceExecuteCU.GetResponseStatusCode();
            i95SyncLogEntry."Error Message" := Copystr(ResultDataBlankMsg, 1, 300);

            i95SyncLogEntry.UpdateSyncLogEntry(i95SyncLogEntry."Sync Status", LogStatus::"In Progress", HttpReasonCode, i95WebserviceExecuteCU.GetAPIResponseResultText(),
                               i95WebserviceExecuteCU.GetAPIResponseMessageText(), i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID",
                               i95SyncLogEntry."Status ID"::"Response Received", SyncSource::i95);
            APILogEntry.UpdateAPILogEntry(HttpReasonCode, copystr(ResultDataBlankMsg, 1, 300));

            MessageID := i95WebserviceExecuteCU.GetResultMessageID();
            SourceID := i95WebserviceExecuteCU.GetSourceID();
            StatusID := i95WebserviceExecuteCU.GetStatusID();

            case i95SyncLogEntry."Sync Status" of
                i95SyncLogEntry."Sync Status"::"Waiting for Sync":
                    i95SyncLogEntry.WriteToBlobField(i95WebserviceExecuteCU.GetWebServiceResponseJson(), CalledByAPI::"i95 Sync Result");
                i95SyncLogEntry."Sync Status"::"Waiting for Response":
                    i95SyncLogEntry.WriteToBlobField(i95WebserviceExecuteCU.GetWebServiceResponseJson(), CalledByAPI::"i95 Response Result");
            end;

            APILogEntry.WriteToBlobField(i95WebserviceExecuteCU.GetWebServiceResponseJson(), BlobToUpdate::"i95 API Result");

            i95SyncLogEntry.UpdateDataStatus(DataStatus::"Data Received");

            If (SourceId <> '') or (StatusId <> 0) or (MessageId <> 0) then
                If (i95WebserviceExecuteCU.GetAPIResponseResultText() <> 'false') and (i95WebserviceExecuteCU.GetAPIResponseMessageText() = '') then begin
                    i95SyncLogEntry."Error Message" := '';
                    i95SyncLogEntry.UpdateSyncLogEntry(CurrentSyncStatus, CurrentLogStatus, HttpReasonCode,
                                                    i95WebserviceExecuteCU.GetAPIResponseResultText(), i95WebserviceExecuteCU.GetAPIResponseMessageText(),
                                                    MessageID, SourceID, StatusID, SyncSource::i95);

                    ResultDataJsonArray := i95WebserviceExecuteCU.GetResultDataJsonArray();

                    foreach ResultDataJsonToken in ResultDataJsonArray do begin
                        ResultDataJsonObject := ResultDataJsonToken.AsObject();


                        if (CurrentSyncStatus <> 0) then begin

                            GetSourceRecordID(SourceRecordID, i95WebserviceExecuteCU.ProcessJsonTokenasCode('TargetId', ResultDataJsonObject), CurrentAPIType);

                            DetailedSyncLogEntry.Reset();
                            DetailedSyncLogEntry.SetCurrentKey("Sync Log Entry No");
                            DetailedSyncLogEntry.SetRange("Sync Log Entry No", SyncLogEntryNo);
                            DetailedSyncLogEntry.SetRange("Source Record ID", SourceRecordID);
                            if DetailedSyncLogEntry.FindSet() then
                                DetailedSyncLogEntry.UpdateSyncLogEntry(CurrentSyncStatus, CurrentLogStatus, HttpReasonCode,
                                i95WebserviceExecuteCU.GetAPIResponseResultText(), i95WebserviceExecuteCU.GetAPIResponseMessageText(),
                                i95WebserviceExecuteCU.ProcessJsonTokenasCode('SourceId', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsontokenasInteger('MessageId', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsonTokenasText('Message', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsonTokenasInteger('StatusId', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsonTokenasCode('TargetId', ResultDataJsonObject), SyncSource::i95);

                            UpdateSourceRecord(SourceRecordID, Format(i95WebserviceExecuteCU.ProcessJsonTokenasInteger('SourceId', ResultDataJsonObject)), CurrentAPIType, CurrentSyncStatus);
                        end;
                        APILogEntry.UpdateAPILogEntry(HttpReasonCode, '');
                    end;
                end else
                    If (i95WebserviceExecuteCU.GetAPIResponseResultText() = 'false') and (i95WebserviceExecuteCU.GetAPIResponseMessageText() <> '') then begin
                        i95SyncLogEntry."Sync Status" := i95SyncLogEntry."Sync Status"::"No Response";
                        i95SyncLogEntry."Log Status" := i95SyncLogEntry."Log Status"::Cancelled;
                        i95SyncLogEntry."Error Message" := '';
                        i95SyncLogEntry.modify();

                        APILogEntry.UpdateAPILogEntry(HttpReasonCode, i95WebserviceExecuteCU.GetAPIResponseMessageText());
                    end;
        end else begin
            i95SyncLogEntry.UpdateSyncLogEntry(i95SyncLogEntry."Sync Status", LogStatus::Error, HttpReasonCode, i95WebserviceExecuteCU.GetAPIResponseResultText(), i95WebserviceExecuteCU.GetAPIResponseMessageText(), MessageID, SourceID, i95SyncLogEntry."Status ID"::Error, SyncSource::i95);
            i95SyncLogEntry."Error Message" := copystr(GetLastErrorText(), 1, 300);
            i95SyncLogEntry.Modify();

            APILogEntry.UpdateAPILogEntry(HttpReasonCode, copystr(GetLastErrorText(), 1, 300));
        end;
    end;

    procedure GetSourceRecordID(Var SourceRecordID: RecordId; CurrentTargetID: Code[20]; CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID)
    var
        Item: Record Item;
        SalesHeader: Record "Sales Header";
        Customer: Record Customer;
        CustomerGroup: Record "Customer Price Group";
        SalesShipment: Record "Sales Shipment Header";
        SalesInvoice: Record "Sales Invoice Header";
    begin
        case CurrentAPIType of
            CurrentAPIType::Product,
            CurrentAPIType::Inventory,
            CurrentAPIType::TierPrices:
                begin
                    item.Get(CurrentTargetID);
                    SourceRecordID := Item.RecordId();
                end;
            CurrentAPIType::SalesOrder:
                begin
                    SalesHeader.get(SalesHeader."Document Type"::Order, CurrentTargetID);
                    SourceRecordID := SalesHeader.RecordId();
                end;
            CurrentAPIType::Customer:
                begin
                    Customer.get(CurrentTargetID);
                    SourceRecordID := Customer.RecordId();
                end;
            CurrentAPIType::CustomerGroup:
                begin
                    CustomerGroup.Get(CurrentTargetID);
                    SourceRecordID := CustomerGroup.RecordId();
                end;
            CurrentAPIType::Shipment:
                begin
                    SalesShipment.Get(CurrentTargetID);
                    SourceRecordID := SalesShipment.RecordId();
                end;
            CurrentAPIType::Invoice:
                begin
                    SalesInvoice.get(CurrentTargetID);
                    SourceRecordID := SalesInvoice.RecordId();
                end;
        end;
    end;

    procedure UpdateSourceRecord(SourceRecordID: RecordId; i95SourceCode: Code[20]; CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID;
                                  CurrentSyncStatus: Integer)
    var
        Item: Record Item;
        SalesHeader: Record "Sales Header";
        Customer: Record Customer;
        CustomerPriceGroup: Record "Customer Price Group";
        ShipToAddress: Record "Ship-to Address";
        SalesShipmentHeader: Record "Sales Shipment Header";
        SalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        case CurrentAPIType of
            CurrentAPIType::Product:
                begin
                    item.Get(SourceRecordID);
                    Item.Seti95APIUpdateCall(true);
                    Item.Updatei95SyncStatus(SyncSource::i95, CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::Inventory:
                begin
                    item.Get(SourceRecordID);
                    Item.Seti95APIUpdateCall(true);
                    Item.Updatei95InventorySyncStatus(CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::Customer:
                begin
                    Customer.Get(SourceRecordID);
                    Customer.Seti95APIUpdateCall(true);
                    Customer.Updatei95SyncStatus(SyncSource::i95, CurrentSyncStatus + 1, i95SourceCode);

                    ShipToAddress.Updatei95SyncStatus(SyncSource::i95, Customer."No.");
                end;
            CurrentAPIType::CustomerGroup:
                begin
                    CustomerPriceGroup.Get(SourceRecordID);
                    CustomerPriceGroup.Seti95APIUpdateCall(true);
                    CustomerPriceGroup.Updatei95SyncStatus(SyncSource::i95, CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::TierPrices:
                begin
                    item.Get(SourceRecordID);
                    Item.Seti95APIUpdateCall(true);
                    Item.Updatei95SalesPriceSyncStatus(SyncSource::i95, CurrentSyncStatus + 1);
                end;
            CurrentAPIType::SalesOrder:
                begin
                    SalesHeader.Get(SourceRecordID);
                    SalesHeader.Seti95PullRequestAPICall(true);
                    SalesHeader.Updatei95SyncStatus(SyncSource::i95, CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::Shipment:
                begin
                    SalesShipmentHeader.Get(SourceRecordID);
                    SalesShipmentHeader.Updatei95SyncStatus(CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::Invoice:
                begin
                    SalesInvoiceHeader.get(SourceRecordID);
                    SalesInvoiceHeader.Updatei95SyncStatus(CurrentSyncStatus + 1, i95SourceCode);
                end;
        end;
    end;

    var
        i95SyncLogEntry: Record "i95 Sync Log Entry";
        APILogEntry: Record "i95 API Call Log Entry";
        i95Setup: Record "i95 Setup";
        DetailedSyncLogEntry: Record "i95 Detailed Sync Log Entry";
        i95WebserviceExecuteCU: codeunit "i95 Webservice Execute";
        CreateBodyContent: Codeunit "i95 Create Body Content";
        MessageID: Integer;
        SourceID: Code[20];
        StatusID: Integer;
        SyncLogEntryNo: Integer;
        APILogEntryNo: Integer;
        TestModeResponseTxt: Text;
        PullDataSource: Option "",Customer,CustomerGroup,SalesOrder,Product,SchedulerID;
        SyncStatus: Option "Waiting for Sync","Waiting for Response","Waiting for Acknowledgement","Sync Complete";
        LogStatus: Option " ",New,"In Progress",Completed,Error;
        CalledByAPI: Option "i95 Sync Request","i95 Sync Result","i95 Response Request","i95 Response Result","i95 Acknowledgement Request","i95 Acknowledgement Result";
        SyncSource: Option "","Business Central",i95;
        SchedulerType: Option " ",PushData,PullResponse,PullResponseAck,PullData,PushResponse;
        DataStatus: Option "","Data Received","Data Updated";
        BlobToUpdate: Option "i95 API Request","i95 API Result";
        TestMode: Boolean;
        HttpReasonCode: Text;
        ResultDataJsonArray: JsonArray;
        ResultDataJsonObject: JsonObject;
        ResultDataJsonToken: JsonToken;
        ResultDataBlankMsg: Label 'Result Data Blank.';
}