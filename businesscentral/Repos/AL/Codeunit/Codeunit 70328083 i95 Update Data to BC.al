codeunit 70328083 "i95 Update Data to BC"
{
    trigger OnRun()
    begin
        Clear(HttpReasonCode);
        Clear(ResponseResultText);

        if i95SyncLogEntry.get(i95SyncLogEntryNo) then begin
            WebServiceJsonObject.ReadFrom(i95SyncLogEntry.ReadFromBlobField(APIDatatoRead::"i95 Sync Result"));
            if i95SyncLogEntry."API Type" = i95SyncLogEntry."API Type"::SchedulerID then
                i95WebServiceExecuteCU.ProcessJsonResponseforSchedulerIDPull(WebServiceJsonObject)
            else
                i95WebServiceExecuteCU.ProcessJsonResponseforPullData(WebServiceJsonObject);

            HttpReasonCode := i95SyncLogEntry."Http Response Code";
            ResponseResultText := i95SyncLogEntry."Response Result";

            case i95SyncLogEntry."API Type" of
                i95SyncLogEntry."API Type"::Customer:
                    UpdateCustomerData();
                i95SyncLogEntry."API Type"::CustomerGroup:
                    UpdateCustomerGroupData();
                i95SyncLogEntry."API Type"::SalesOrder:
                    UpdateSalesOrderData();
                i95SyncLogEntry."API Type"::Product:
                    UpdateProductData();
                i95SyncLogEntry."API Type"::SchedulerID:
                    UpdateSchedulerID();
            end;
        end;
    end;

    procedure SetParamaters(EntryNo: Integer)
    begin
        i95SyncLogEntryNo := EntryNo;
    end;

    procedure UpdateCustomerData()
    var
        NewCustomer: Record customer;
        ShiptoAddress: Record "Ship-to Address";
        SourceNo: Code[20];
        Email: Text;
        FirstName: Text;
        LastName: text;
        CustPriceGroup: Code[20];
        SourceAddressID: Code[20];
        ShipToFirstName: Text;
        ShipToLastName: Text;
        Address: text[50];
        Address2: text[50];
        City: text[30];
        PhoneNo: text[30];
        RegionCode: Code[10];
        CountryCode: Code[10];
        PostCode: code[20];
        IsDefaultBilling: Boolean;
        IsDefaultShipping: Boolean;
    begin

        ResultDataJsonArray := i95WebserviceExecuteCU.GetResultDataJsonArray();
        ResultJsonToken := i95WebserviceExecuteCU.GetResultJsonToken();
        ResultDataJsonToken := i95WebserviceExecuteCU.GetResultDataJsonToken();
        ResultDataJsonObject := i95WebserviceExecuteCU.GetResultDataJsonObject();

        i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"No Response", LogStatus::Cancelled, i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);

        foreach ResultDataJsonToken in ResultDataJsonArray do begin
            SourceNo := '';

            ResultDataJsonObject := ResultDataJsonToken.AsObject();
            SourceNo := i95WebserviceExecuteCU.ProcessJsonTokenasCode('SourceId', ResultDataJsonObject);
            TargetID := i95WebserviceExecuteCU.ProcessJsonTokenasCode('TargetId', ResultDataJsonObject);
            MessageID := i95WebServiceExecuteCU.ProcessJsontokenasInteger('MessageId', ResultDataJsonObject);
            StatusID := i95WebServiceExecuteCU.ProcessJsonTokenasInteger('StatusId', ResultDataJsonObject);
            SourceID := i95WebServiceExecuteCU.ProcessJsonTokenasCode('SourceId', ResultDataJsonObject);
            i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"Waiting for Sync", LogStatus::New, HttpReasonCode, ResponseResultText, ResponseMessageText, MessageID, SourceID, StatusID, SyncSource::i95);
            if ResultDataJsonObject.Contains('InputData') then begin
                ResultDataJsonObject.get('InputData', ResultDataJsonToken);
                ResultDataJsonObject := ResultDataJsonToken.AsObject();
                Email := i95WebserviceExecuteCU.ProcessJsonTokenasText('email', ResultDataJsonObject);
                FirstName := i95WebserviceExecuteCU.ProcessJsonTokenasText('firstName', ResultDataJsonObject);
                LastName := i95WebserviceExecuteCU.ProcessJsonTokenasText('lastName', ResultDataJsonObject);
                CustPriceGroup := i95WebserviceExecuteCU.ProcessJsonTokenasCode('customerGroup', ResultDataJsonObject);
            end;


            clear(SourceRecordID);
            clear(SecSourceRecordID);
            SalesReceivablesSetup.get();
            i95Setup.get();
            If SourceNo <> '' then begin
                NewCustomer.reset();
                NewCustomer.SetCurrentKey("i95 Reference ID");
                NewCustomer.SetRange("i95 Reference ID", SourceNo);
                If not NewCustomer.FindFirst() then begin
                    NewCustomer.init();
                    If i95Setup."Customer Nos." <> '' then
                        NewCustomer.validate("No.", NoSeriesMgt.GetNextNo(i95Setup."Customer Nos.", 0D, true))
                    else
                        NewCustomer.validate("No.", NoSeriesMgt.GetNextNo(SalesReceivablesSetup."Customer Nos.", 0D, true));
                    NewCustomer."i95 Created By" := 'i95';
                    NewCustomer."i95 Created DateTime" := CurrentDateTime();
                    NewCustomer."i95 Creation Source" := NewCustomer."i95 Creation Source"::i95;
                    NewCustomer.insert();
                end;
                NewCustomer.validate("E-Mail", Email);
                NewCustomer.validate(Name, copystr(FirstName + ' ' + LastName, 1, 100));
                NewCustomer.validate("i95 Reference ID", SourceNo);
                NewCustomer.validate("Customer Posting Group", i95Setup."i95 Customer Posting Group");
                NewCustomer.validate("Gen. Bus. Posting Group", i95Setup."i95 Gen. Bus. Posting Group");
                NewCustomer.validate(NewCustomer."Customer Price Group", CustPriceGroup);

                if ResultDataJsonObject.Contains('addresses') then begin
                    ResultDataJsonObject.get('addresses', ResultDataJsonToken);
                    ResultDataJsonArray := ResultDataJsonToken.AsArray();
                end;

                foreach ResultDataJsonToken in ResultDataJsonArray do begin
                    ResultDataJsonObject := ResultDataJsonToken.AsObject();

                    SourceAddressID := i95WebserviceExecuteCU.ProcessJsonTokenasCode('sourceId', ResultDataJsonObject);
                    ShipToFirstName := i95WebserviceExecuteCU.ProcessJsonTokenasText('firstName', ResultDataJsonObject);
                    ShipToLastName := i95WebserviceExecuteCU.ProcessJsonTokenasText('lastName', ResultDataJsonObject);
                    Address := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('street', ResultDataJsonObject), 1, 50);
                    Address2 := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('street2', ResultDataJsonObject), 1, 50);
                    RegionCode := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasCode('regionId', ResultDataJsonObject), 1, 10);
                    City := i95WebserviceExecuteCU.ProcessJsonTokenasCode('city', ResultDataJsonObject);
                    CountryCode := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasCode('countryId', ResultDataJsonObject), 1, 10);
                    PostCode := i95WebserviceExecuteCU.ProcessJsonTokenasCode('postcode', ResultDataJsonObject);
                    PhoneNo := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('telephone', ResultDataJsonObject), 1, 30);
                    IsDefaultBilling := i95WebserviceExecuteCU.ProcessJsonTokenasBoolean('isDefaultBilling', ResultDataJsonObject);
                    IsDefaultShipping := i95WebserviceExecuteCU.ProcessJsonTokenasBoolean('isDefaultShipping', ResultDataJsonObject);

                    If IsDefaultBilling then begin
                        NewCustomer.validate(Address, Address);
                        NewCustomer.validate("Address 2", Address2);
                        NewCustomer.validate(City, City);
                        NewCustomer.validate("Phone No.", PhoneNo);
                        NewCustomer.validate(County, RegionCode);
                        NewCustomer.validate("Country/Region Code", CountryCode);
                        NewCustomer.validate("Phone No.", PhoneNo);
                        NewCustomer.validate(City, City);
                        NewCustomer.validate("Post Code", PostCode);
                        NewCustomer.County := RegionCode;
                    end;

                    If not IsDefaultBilling then begin
                        If not ShiptoAddress.get(NewCustomer."No.", SourceAddressID) then begin
                            ShiptoAddress.init();
                            ShiptoAddress.validate("Customer No.", NewCustomer."No.");
                            ShiptoAddress.validate(Code, SourceAddressID);
                            ShiptoAddress."i95 Created By" := 'i95';
                            ShiptoAddress."i95 Created DateTime" := CurrentDateTime();
                            ShiptoAddress."i95 Creation Source" := ShiptoAddress."i95 Creation Source"::i95;
                            ShiptoAddress.insert();
                        end;

                        ShiptoAddress.Validate(Name, CopyStr(ShipToFirstName + ' ' + ShipToLastName, 1, 100));
                        ShiptoAddress.validate(Address, Address);
                        ShiptoAddress.validate("Address 2", Address2);
                        ShiptoAddress.validate(City, City);
                        ShiptoAddress.validate("Phone No.", PhoneNo);
                        ShiptoAddress.validate(County, RegionCode);
                        ShiptoAddress.validate("Country/Region Code", CountryCode);
                        ShiptoAddress.validate(City, City);
                        ShiptoAddress.validate(County, RegionCode);
                        ShiptoAddress.validate("Post Code", PostCode);
                        ShiptoAddress.validate("Phone No.", PhoneNo);
                        ShiptoAddress.County := RegionCode;
                        SecSourceRecordID := ShiptoAddress.RecordId();
                        ShiptoAddress."i95 Is Default Shipping" := IsDefaultShipping;
                        ShiptoAddress."i95 Last Modification DateTime" := CurrentDateTime();
                        ShiptoAddress."i95 Last Modified By" := copystr(UserId(), 1, 80);
                        ShiptoAddress."i95 Last Sync DateTime" := CurrentDateTime();
                        ShiptoAddress."i95 Last Modification Source" := ShiptoAddress."i95 Last Modification Source"::i95;
                        ShiptoAddress.County := RegionCode;
                        ShiptoAddress.Modify(false);
                    end else begin
                        If not ShiptoAddress.get(NewCustomer."No.", 'I95DEFAULT') then begin
                            ShiptoAddress.init();
                            ShiptoAddress.validate("Customer No.", NewCustomer."No.");
                            ShiptoAddress.validate(Code, 'I95DEFAULT');
                            ShiptoAddress."i95 Created By" := 'i95';
                            ShiptoAddress."i95 Created DateTime" := CurrentDateTime();
                            ShiptoAddress."i95 Creation Source" := ShiptoAddress."i95 Creation Source"::i95;
                            ShiptoAddress.insert();
                        end;

                        ShiptoAddress.Validate(Name, CopyStr(ShipToFirstName + ' ' + ShipToLastName, 1, 100));
                        ShiptoAddress.validate(Address, Address);
                        ShiptoAddress.validate("Address 2", Address2);
                        ShiptoAddress.validate(City, City);
                        ShiptoAddress.validate("Phone No.", PhoneNo);
                        ShiptoAddress.validate(County, RegionCode);
                        ShiptoAddress.validate("Country/Region Code", CountryCode);
                        ShiptoAddress.validate(City, City);
                        ShiptoAddress.validate(County, RegionCode);
                        ShiptoAddress.validate("Post Code", PostCode);
                        ShiptoAddress.validate("Phone No.", PhoneNo);
                        ShiptoAddress.Validate("E-Mail", Email);
                        ShiptoAddress.County := RegionCode;
                        SecSourceRecordID := ShiptoAddress.RecordId();
                        ShiptoAddress."i95 Is Default Shipping" := IsDefaultShipping;
                        ShiptoAddress."i95 Last Modification DateTime" := CurrentDateTime();
                        ShiptoAddress."i95 Last Modified By" := copystr(UserId(), 1, 80);
                        ShiptoAddress."i95 Last Sync DateTime" := CurrentDateTime();
                        ShiptoAddress."i95 Last Modification Source" := ShiptoAddress."i95 Last Modification Source"::i95;
                        ShiptoAddress.County := RegionCode;
                        ShiptoAddress.Modify(false);
                    end;
                end;

                SourceRecordID := NewCustomer.RecordId();
                NewCustomer.validate("i95 ShiptoAddress Code", ShiptoAddress.Code);
                NewCustomer.Seti95APIUpdateCall(true);
                NewCustomer."i95 Last Modification DateTime" := CurrentDateTime();
                NewCustomer."i95 Last Modified By" := copystr(UserId(), 1, 80);
                NewCustomer."i95 Last Sync DateTime" := CurrentDateTime();
                NewCustomer."i95 Sync Status" := NewCustomer."i95 Sync Status"::"Waiting for Response";
                NewCustomer."i95 Last Modification Source" := NewCustomer."i95 Last Modification Source"::i95;
                NewCustomer."i95 Reference ID" := SourceNo;
                NewCustomer.County := RegionCode;
                NewCustomer.Modify(false);

                DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::Customer, SyncStatus::"Waiting for Response", LogStatus::New, SyncSource::i95, i95SyncLogEntryNo, NewCustomer."No.", NewCustomer.Name, '', NewCustomer.RecordId(), Database::customer);

                i95SyncLogEntry."Error Message" := '';
                i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"Waiting for Response", LogStatus::"In-Progress", i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);
                i95SyncLogEntry.SetSourceRecordID(SourceRecordID);
                i95SyncLogEntry.SetSecondarySourceRecordID(SecSourceRecordID);
                i95SyncLogEntry.UpdateDataStatus(DataStatus::"Data Updated");
            end else
                i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"No Response", LogStatus::Cancelled, i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);
        end;
    end;

    procedure UpdateCustomerGroupData()
    var
        CustomerGroup: Record "Customer price Group";
        CustomerGroupCode: code[20];
        CustomerGroupDescription: text;
        InputJsonObject: JsonObject;
    begin
        ResultDataJsonArray := i95WebserviceExecuteCU.GetResultDataJsonArray();

        i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"No Response", LogStatus::Cancelled, i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);

        foreach ResultDataJsonToken in ResultDataJsonArray do begin
            CustomerGroupCode := '';

            ResultDataJsonObject := ResultDataJsonToken.AsObject();
            if ResultDataJsonObject.Contains('InputData') then begin
                ResultDataJsonObject.get('InputData', ResultJsonToken);
                InputJsonObject := ResultJsonToken.AsObject();
                CustomerGroupCode := i95WebserviceExecuteCU.ProcessJsonTokenasCode('customerGroup', InputJsonObject);
                CustomerGroupDescription := i95WebserviceExecuteCU.ProcessJsonTokenasText('groupDescription', InputJsonObject);
                TargetID := i95WebserviceExecuteCU.ProcessJsonTokenasCode('TargetId', InputJsonObject);
                MessageID := i95WebServiceExecuteCU.ProcessJsontokenasInteger('MessageId', ResultDataJsonObject);
                StatusID := i95WebServiceExecuteCU.ProcessJsonTokenasInteger('StatusId', ResultDataJsonObject);
                SourceID := i95WebServiceExecuteCU.ProcessJsonTokenascode('SourceId', ResultDataJsonObject);
                i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"Waiting for Sync", LogStatus::New, HttpReasonCode, ResponseResultText, ResponseMessageText, MessageID, SourceID, StatusID, SyncSource::i95);
            end;

            clear(SourceRecordID);
            If CustomerGroupCode <> '' then begin
                If not CustomerGroup.get(CustomerGroupCode) then begin
                    CustomerGroup.init();
                    CustomerGroup.validate(CustomerGroup.Code, CustomerGroupCode);
                    CustomerGroup.validate(Description, CustomerGroupDescription);
                    CustomerGroup."i95 Created By" := 'i95';
                    CustomerGroup."i95 Created DateTime" := CurrentDateTime();
                    CustomerGroup."i95 Creation Source" := CustomerGroup."i95 Creation Source"::i95;
                    CustomerGroup.insert();
                    SourceRecordID := CustomerGroup.RecordId();
                end else begin
                    CustomerGroup.validate(Description, CustomerGroupDescription);
                    CustomerGroup.Seti95APIUpdateCall(true);
                    CustomerGroup.Modify();
                    SourceRecordID := CustomerGroup.RecordId();
                end;
                CustomerGroup.Seti95APIUpdateCall(true);
                CustomerGroup."i95 Last Modification DateTime" := CurrentDateTime();
                CustomerGroup."i95 Last Modified By" := copystr(UserId(), 1, 80);
                CustomerGroup."i95 Last Sync DateTime" := CurrentDateTime();
                CustomerGroup."i95 Sync Status" := CustomerGroup."i95 Sync Status"::"Waiting for Response";
                CustomerGroup."i95 Last Modification Source" := CustomerGroup."i95 Last Modification Source"::i95;
                CustomerGroup."i95 Reference ID" := CustomerGroupCode;
                CustomerGroup.Modify(false);

                DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::CustomerGroup, SyncStatus::"Waiting for Response", LogStatus::New, SyncSource::i95, i95SyncLogEntryNo, CustomerGroup.Code, CustomerGroup.Description, '', CustomerGroup.RecordId(), Database::"Customer Price Group");

                i95SyncLogEntry."Error Message" := '';
                i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"Waiting for Response", LogStatus::"In-Progress", i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);
                i95SyncLogEntry.SetSourceRecordID(SourceRecordID);
                i95SyncLogEntry.UpdateDataStatus(DataStatus::"Data Updated");
            end else
                i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"No Response", LogStatus::Cancelled, i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);
        end;
    end;

    procedure UpdateSalesOrderData()
    var
        SalesHeader: Record "Sales Header";
        Customer: Record Customer;
        GenLedgerSetup: Record "General Ledger Setup";
        ShippingAgentMapping: Record "i95 Shipping Agent Mapping";
        PaymentMethodsMapping: Record "i95 Payment Methods Mapping";
        SalesOrderNo: code[20];
        CurrencyCode: code[20];
        TaxAmount: Decimal;
        OrderAmount: Decimal;
        SourceOrderStatus: text;
        OrderDateText: Text;
        OrderDate: Date;
        LastUpdatedDateText: Text;
        LastUpdatedDate: date;
        ShippingAmount: Decimal;
        CustomerId: code[20];
        CustomerPostingGroup: code[20];
        Email: text;
        ShippingAgentCode: code[50];
        BCShippingAgentCode: Code[10];
        PaymentMethodCode: code[50];
        BCPaymentMethodCode: Code[10];
        BCShippingAgentServiceCode: Code[10];
        IsGuest: Boolean;
        Month: Integer;
        Day: Integer;
        Year: Integer;
        CustomerIdBlankErr: Label 'Customer No. is blank!';
        BillToCity: Text[30];
        BillToCountryId: Code[10];
        BillToName: text[50];
        BillToName2: Text[50];
        BillToPostCode: code[20];
        BillToRegion: Text[30];
        BillToAddress: Text[50];
        BillToAddress2: Text[50];
        BillToPhone: Text[50];
        ShipToCity: Text[30];
        ShipToCountryId: Code[10];
        ShipToName: text[50];
        ShipToName2: Text[50];
        ShipToPostCode: code[20];
        ShipToRegion: Text[30];
        ShipToAddress: Text[50];
        ShipToAddress2: Text[50];
        ShipToPhone: Text[50];
        ShippingAgentMappingErrorTxt: Label 'Shipping Method Mapping not available for %1 in order %2.';
        PaymentMethodMappingErrorTxt: Label 'Payment Method Mapping not available for %1 in order %2.';
    begin
        clear(ItemNo);
        Clear(QuantityOrdered);
        Clear(ItemPrice);
        Clear(SpecialPrice);
        Clear(RetailVariantId);
        Clear(ParentSku);
        i95Setup.get();
        SalesReceivablesSetup.get();

        SalesInputDataJsonArray := i95WebserviceExecuteCU.GetResultDataJsonArray();
        SalesInputJsonToken := i95WebserviceExecuteCU.GetResultJsonToken();
        SalesInputDataJsonToken := i95WebserviceExecuteCU.GetResultDataJsonToken();
        SalesInputDataJsonObject := i95WebserviceExecuteCU.GetResultDataJsonObject();

        i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"No Response", LogStatus::Cancelled, i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);

        foreach SalesInputDataJsonToken in SalesInputDataJsonArray do begin
            SalesOrderNo := '';
            SalesInputDataJsonObject := SalesInputDataJsonToken.AsObject();

            TargetId := i95WebserviceExecuteCU.ProcessJsonTokenasCode('TargetId', SalesInputDataJsonObject);
            MessageID := i95WebServiceExecuteCU.ProcessJsontokenasInteger('MessageId', SalesInputDataJsonObject);
            StatusID := i95WebServiceExecuteCU.ProcessJsonTokenasInteger('StatusId', SalesInputDataJsonObject);
            SourceID := i95WebServiceExecuteCU.ProcessJsonTokenascode('SourceId', SalesInputDataJsonObject);
            i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"Waiting for Sync", LogStatus::New, HttpReasonCode, ResponseResultText, ResponseMessageText, MessageID, SourceID, StatusID, SyncSource::i95);
            If SalesInputDataJsonObject.Contains('InputData') then begin
                SalesInputDataJsonObject.get('InputData', SalesInputDataJsonToken);
                SalesInputDataJsonObject := SalesInputDataJsonToken.AsObject();
                SalesOrderNo := i95WebserviceExecuteCU.ProcessJsonTokenasCode('sourceId', SalesInputDataJsonObject);
                CurrencyCode := i95WebserviceExecuteCU.ProcessJsonTokenasCode('currency', SalesInputDataJsonObject);
                TaxAmount := i95WebserviceExecuteCU.ProcessJsonTokenasDecimal('taxAmount', SalesInputDataJsonObject);
                OrderAmount := i95WebserviceExecuteCU.ProcessJsonTokenasDecimal('orderDocumentAmount', SalesInputDataJsonObject);
                SourceOrderStatus := i95WebserviceExecuteCU.ProcessJsonTokenasText('sourceOrderStatus', SalesInputDataJsonObject);
                OrderDateText := i95WebserviceExecuteCU.ProcessJsonTokenasText('orderCreatedDate', SalesInputDataJsonObject);
                If strlen(OrderDateText) >= 10 then begin
                    evaluate(Day, CopyStr(OrderDateText, 9, 2));
                    evaluate(Month, CopyStr(OrderDateText, 6, 2));
                    evaluate(Year, CopyStr(OrderDateText, 1, 4));
                    OrderDate := DMY2Date(Day, Month, Year);
                end;

                LastUpdatedDateText := i95WebserviceExecuteCU.ProcessJsonTokenasText('lastUpdatedDate', SalesInputDataJsonObject);
                If strlen(LastUpdatedDateText) >= 10 then begin
                    evaluate(Day, CopyStr(LastUpdatedDateText, 9, 2));
                    evaluate(Month, CopyStr(LastUpdatedDateText, 6, 2));
                    evaluate(Year, CopyStr(LastUpdatedDateText, 1, 4));
                    LastUpdatedDate := DMY2Date(Day, Month, Year);
                end;

                ShippingAmount := i95WebserviceExecuteCU.ProcessJsonTokenasDecimal('shippingAmount', SalesInputDataJsonObject);
                CustomerID := i95WebserviceExecuteCU.ProcessJsonTokenasCode('targetCustomerId', SalesInputDataJsonObject);
                CustomerPostingGroup := i95WebserviceExecuteCU.ProcessJsonTokenasCode('customerGroup', SalesInputDataJsonObject);
                ShippingAgentCode := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('shippingMethod', SalesInputDataJsonObject), 1, 50);

                If SalesInputDataJsonObject.Contains('customer') then begin
                    SalesInputDataJsonObject.get('customer', SalesInputDataJsonToken);
                    SOAddressJsonObject := SalesInputDataJsonToken.AsObject();
                    Email := i95WebserviceExecuteCU.ProcessJsonTokenasText('email', SOAddressJsonObject);
                    IsGuest := i95WebserviceExecuteCU.ProcessJsonTokenasBoolean('isGuest', SOAddressJsonObject);
                end;

                If SalesInputDataJsonObject.Contains('billingAddress') then begin
                    SalesInputDataJsonObject.get('billingAddress', SalesInputDataJsonToken);
                    SOAddressJsonObject := SalesInputDataJsonToken.AsObject();

                    BillToCity := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('city', SOAddressJsonObject), 1, 30);
                    BillToCountryId := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasCode('countryId', SOAddressJsonObject), 1, 10);
                    BillToName := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('firstName', SOAddressJsonObject), 1, 50);
                    BillToName2 := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('lastName', SOAddressJsonObject), 1, 50);
                    BillToPostCode := i95WebserviceExecuteCU.ProcessJsonTokenasCode('postcode', SOAddressJsonObject);
                    BillToRegion := copystr(i95WebserviceExecuteCU.ProcessJsonTokenastext('region', SOAddressJsonObject), 1, 30);
                    BillToAddress := copystr(i95WebserviceExecuteCU.ProcessJsonTokenastext('street', SOAddressJsonObject), 1, 50);
                    BillToAddress2 := copystr(i95WebserviceExecuteCU.ProcessJsonTokenastext('street2', SOAddressJsonObject), 1, 50);
                    BillToPhone := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('telephone', SOAddressJsonObject), 1, 50);
                end;

                If SalesInputDataJsonObject.Contains('shippingAddress') then begin
                    SalesInputDataJsonObject.get('shippingAddress', SalesInputDataJsonToken);
                    SOAddressJsonObject := SalesInputDataJsonToken.AsObject();
                    ShipToCity := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('city', SOAddressJsonObject), 1, 30);
                    ShipToCountryId := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasCode('countryId', SOAddressJsonObject), 1, 10);
                    ShipToName := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('firstName', SOAddressJsonObject), 1, 50);
                    ShipToName2 := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('lastName', SOAddressJsonObject), 1, 50);
                    ShipToPostCode := i95WebserviceExecuteCU.ProcessJsonTokenasCode('postcode', SOAddressJsonObject);
                    ShipToRegion := copystr(i95WebserviceExecuteCU.ProcessJsonTokenastext('region', SOAddressJsonObject), 1, 30);
                    ShipToAddress := copystr(i95WebserviceExecuteCU.ProcessJsonTokenastext('street', SOAddressJsonObject), 1, 50);
                    ShipToAddress2 := copystr(i95WebserviceExecuteCU.ProcessJsonTokenastext('street2', SOAddressJsonObject), 1, 50);
                    ShipToPhone := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('telephone', SOAddressJsonObject), 1, 50);
                end;
            end;

            if SalesInputDataJsonObject.Contains('payment') then begin
                SalesInputDataJsonObject.get('payment', PaymentInputDataJsonToken);
                PaymentInputDataJsonarray := PaymentInputDataJsonToken.AsArray();
            end;
            foreach PaymentInputDataJsonToken in PaymentInputDataJsonarray do begin
                PaymentInputDataJsonObject := PaymentInputDataJsonToken.AsObject();
                PaymentMethodCode := copystr(i95WebserviceExecuteCU.ProcessJsonTokenastext('paymentMethod', PaymentInputDataJsonObject), 1, 50);
            end;

            //Check Shipment Method and Payment Method Mapping
            If ShippingAgentCode <> '' then begin
                ShippingAgentMapping.Reset();
                ShippingAgentMapping.SetRange(ShippingAgentMapping."E-Com Shipping Method Code", ShippingAgentCode);
                If ShippingAgentMapping.FindFirst() then begin
                    BCShippingAgentCode := ShippingAgentMapping."BC Shipping Agent Code";
                    BCShippingAgentServiceCode := ShippingAgentMapping."BC Shipping Agent Service Code";
                end else
                    Error(StrSubstNo(ShippingAgentMappingErrorTxt, ShippingAgentCode, SalesOrderNo));
            end;

            If PaymentMethodCode <> '' then begin
                PaymentMethodsMapping.Reset();
                PaymentMethodsMapping.SetRange(PaymentMethodsMapping."E-Commerce Payment Method Code", PaymentMethodCode);
                PaymentMethodsMapping.SetRange(PaymentMethodsMapping."Ecommerce to BC Default", true);
                If PaymentMethodsMapping.FindFirst() then
                    BCPaymentMethodCode := PaymentMethodsMapping."BC Payment Method Code"
                else begin
                    PaymentMethodsMapping.SetRange(PaymentMethodsMapping."Ecommerce to BC Default");
                    If paymentMethodsMapping.FindFirst() then
                        BCPaymentMethodCode := PaymentMethodsMapping."BC Payment Method Code"
                    else
                        error(StrSubstNo(PaymentMethodMappingErrorTxt, PaymentMethodCode, SalesOrderNo));
                end;
            end;

            clear(SourceRecordID);
            If SalesOrderNo <> '' then begin
                SalesHeader.init();
                SalesHeader.validate("Document Type", SalesHeader."Document Type"::Order);
                SalesHeader."No." := '';
                SalesHeader.Insert(true);

                If IsGuest and (i95Setup."Default Guest Customer No." <> '') then
                    SalesHeader.validate("Sell-to Customer No.", i95Setup."Default Guest Customer No.")
                else
                    if (CustomerId <> '') then begin
                        Customer.reset();
                        Customer.SetCurrentKey("i95 Reference ID");
                        Customer.SetRange("i95 Reference ID", CustomerId);
                        if Customer.FindFirst() then
                            CustomerId := Customer."No.";

                        SalesHeader.validate("Sell-to Customer No.", CustomerId);
                    end else
                        Error(CustomerIdBlankErr);

                SalesHeader."i95 Created By" := 'i95';
                SalesHeader."i95 Created DateTime" := CurrentDateTime();
                SalesHeader."i95 Creation Source" := SalesHeader."i95 Creation Source"::i95;

                GenLedgerSetup.get();
                if GenLedgerSetup."LCY Code" <> CurrencyCode then
                    SalesHeader.validate("Currency Code", CurrencyCode);
                SalesHeader.validate("Order Date", OrderDate);

                SalesHeader."Ship-to Name" := copystr(ShipToName + ' ' + ShipToName2, 1, 99);
                SalesHeader."Ship-to Address" := ShipToAddress;
                SalesHeader."Ship-to Address 2" := ShipToAddress2;
                SalesHeader."Ship-to City" := ShipToCity;
                SalesHeader."Ship-to Post Code" := ShipToPostCode;
                SalesHeader."Ship-to County" := ShipToRegion;
                SalesHeader."Ship-to Country/Region Code" := ShipToCountryId;
                SalesHeader."Ship-to Contact" := ShipToPhone;

                SalesHeader."Bill-to Name" := Copystr(BillToName + ' ' + BillToName2, 1, 99);
                SalesHeader."Bill-to Address" := BillToAddress;
                SalesHeader."Bill-to Address 2" := BillToAddress2;
                SalesHeader."Bill-to City" := BillToCity;
                SalesHeader."Bill-to Post Code" := BillToPostCode;
                SalesHeader."Bill-to County" := BillToRegion;
                SalesHeader."Bill-to Country/Region Code" := BillToCountryId;
                SalesHeader."Bill-to Contact" := BillToPhone;

                SalesHeader."Sell-to Customer Name" := Copystr(BillToName + ' ' + BillToName2, 1, 99);
                SalesHeader."Sell-to Address" := BillToAddress;
                SalesHeader."Sell-to Address 2" := BillToAddress2;
                SalesHeader."Sell-to City" := BillToCity;
                SalesHeader."Sell-to Post Code" := BillToPostCode;
                SalesHeader."Sell-to County" := BillToRegion;
                SalesHeader."Sell-to Country/Region Code" := BillToCountryId;
                SalesHeader."Sell-to Contact" := BillToPhone;

                SalesHeader.validate("Shipping Agent Code", BCShippingAgentCode);
                SalesHeader.validate("Shipping Agent Service Code", BCShippingAgentServiceCode);
                SalesHeader.validate("Payment Method Code", BCPaymentMethodCode);

                If i95Setup."i95 Default Warehouse" <> '' then
                    SalesHeader.validate("Location Code", i95Setup."i95 Default Warehouse");

                If IsGuest then
                    SalesHeader."Sell-to E-Mail" := CopyStr(Email, 1, 80);
                SalesHeader.validate("i95 Reference ID", SalesOrderNo);
                SalesHeader.Seti95PullRequestAPICall(true);
                SalesHeader.Modify();

                if SalesInputDataJsonObject.Contains('orderItems') then begin
                    SalesInputDataJsonObject.get('orderItems', SalesLineInputDataJsonToken);
                    SalesLineInputDataJsonArray := SalesLineInputDataJsonToken.AsArray();
                end;
                Clear(LineNo);
                foreach SalesLineInputDataJsonToken in SalesLineInputDataJsonArray do begin
                    SalesLineInputDataJsonObject := SalesLineInputDataJsonToken.AsObject();
                    ItemNo := i95WebserviceExecuteCU.ProcessJsonTokenasCode('sku', SalesLineInputDataJsonObject);
                    ItemPrice := i95WebserviceExecuteCU.ProcessJsonTokenasDecimal('price', SalesLineInputDataJsonObject);
                    QuantityOrdered := i95WebserviceExecuteCU.ProcessJsonTokenasDecimal('qty', SalesLineInputDataJsonObject);
                    SpecialPrice := i95WebserviceExecuteCU.ProcessJsonTokenasDecimal('specialPrice', SalesLineInputDataJsonObject);
                    RetailVariantId := i95WebServiceExecuteCU.ProcessJsonTokenasCode('retailVariantId', SalesLineInputDataJsonObject);
                    ParentSku := i95WebServiceExecuteCU.ProcessJsonTokenasCode('parentSku', SalesLineInputDataJsonObject);

                    //Discount Amount
                    clear(LineDiscountAmount);
                    if SalesInputDataJsonObject.Contains('discount') then begin
                        SalesInputDataJsonObject.get('discount', DiscountAmountInputDataJsonToken);
                        DiscountAmountInputDataJsonArray := DiscountAmountInputDataJsonToken.AsArray();
                    end;

                    foreach DiscountAmountInputDataJsonToken in DiscountAmountInputDataJsonArray do begin
                        DiscountAmountInputDataJsonObject := DiscountAmountInputDataJsonToken.AsObject();
                        LineDiscountAmount := i95WebserviceExecuteCU.ProcessJsonTokenasDecimal('discountAmount', DiscountAmountInputDataJsonObject);
                    end;

                    CreateSalesOrderLines(SalesHeader);
                end;

                If (ShippingAmount <> 0) and (i95Setup."i95 Shipping Charge G/L Acc" <> '') then
                    CreateShippingChargeSalesOrderLine(SalesHeader, ShippingAmount);

                SourceRecordID := SalesHeader.RecordId();
                SalesHeader.Seti95PullRequestAPICall(true);

                SalesHeader."i95 Last Modification DateTime" := CurrentDateTime();
                SalesHeader."i95 Last Modified By" := copystr(UserId(), 1, 80);
                SalesHeader."i95 Last Sync DateTime" := CurrentDateTime();
                SalesHeader."i95 Sync Status" := SalesHeader."i95 Sync Status"::"Waiting for Response";
                SalesHeader."i95 Last Modification Source" := SalesHeader."i95 Last Modification Source"::i95;
                SalesHeader."i95 Reference ID" := SalesOrderNo;
                SalesHeader.Modify(false);

                DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::SalesOrder, SyncStatus::"Waiting for Response", LogStatus::New, SyncSource::i95, i95SyncLogEntryNo, SalesHeader."No.", SalesHeader."Sell-to Customer No.", '', SalesHeader.RecordId(), Database::"Sales Header");

                i95SyncLogEntry."Error Message" := '';
                i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"Waiting for Response", LogStatus::"In-Progress", i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);

                i95SyncLogEntry.SetSourceRecordID(SourceRecordID);
                i95SyncLogEntry.UpdateDataStatus(DataStatus::"Data Updated");
            end else
                i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"No Response", LogStatus::Cancelled, i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);
        end;
    end;

    procedure CreateSalesOrderLines(SalHdr: Record "Sales Header")
    var
        SalesLine: Record "Sales Line";
        Item: Record Item;
        ItemVariant: Record "Item Variant";
    begin
        i95Setup.get();

        SalesLine.init();
        SalesLine.Validate("Document Type", SalesLine."Document Type"::Order);
        SalesLine.validate("Document No.", SalHdr."No.");
        SalesLine.Validate("Sell-to Customer No.", SalHdr."Sell-to Customer No.");
        SalesLine.validate("Line No.", LineNo + 10000);
        SalesLine.insert();
        SalesLine.validate(Type, SalesLine.Type::Item);

        if strpos(ItemNo, i95Setup."i95 Item Variant Seperator") <> 0 then begin
            ParentSku := copystr(CopyStr(ItemNo, 1, StrPos(ItemNo, i95Setup."i95 Item Variant Seperator") - 1), 1, 20);
            RetailVariantId := copystr(CopyStr(ItemNo, StrPos(ItemNo, i95Setup."i95 Item Variant Seperator") + 1), 1, 20);
            ItemVariant.Reset();
            ItemVariant.SetRange(ItemVariant."Item No.", ParentSku);
            ItemVariant.SetRange(ItemVariant.Code, RetailVariantId);
            If ItemVariant.FindFirst() then begin
                SalesLine.Validate("No.", ItemVariant."Item No.");
                SalesLine.Validate("Variant Code", ItemVariant.Code);
            end else begin
                ItemVariant.Reset();
                ItemVariant.SetCurrentKey("i95 Reference ID");
                ItemVariant.SetRange(ItemVariant."i95 Reference ID", ParentSku);
                If ItemVariant.FindFirst() then begin
                    SalesLine.Validate("No.", ItemVariant."Item No.");
                    SalesLine.Validate("Variant Code", ItemVariant.Code);
                end else
                    If Item.get(ItemNo) then
                        SalesLine.validate("No.", ItemNo)
                    else begin
                        If ItemNo <> '' then begin
                            Item.reset();
                            Item.SetRange(Item."i95 Reference ID", ItemNo);
                            If Item.FindFirst() then
                                ItemNo := Item."No.";
                        end;
                        SalesLine.validate("No.", ItemNo);
                    end;
            end;
        end else
            If Item.get(ItemNo) then
                SalesLine.validate("No.", ItemNo)
            else begin
                If ItemNo <> '' then begin
                    Item.reset();
                    Item.SetRange(Item."i95 Reference ID", ItemNo);
                    If Item.FindFirst() then
                        ItemNo := Item."No.";
                end;
                SalesLine.validate("No.", ItemNo);
            end;

        SalesLine.validate(Quantity, QuantityOrdered);

        if i95Setup."i95 Default Warehouse" <> '' then
            SalesLine.Validate("Location Code", i95Setup."i95 Default Warehouse");

        If SpecialPrice = 0 then
            SalesLine.validate("Unit Price", ItemPrice)
        else
            SalesLine.validate("Unit Price", SpecialPrice);

        If LineDiscountAmount <> 0 then
            SalesLine.validate("Line Discount Amount", LineDiscountAmount);

        SalesLine.i95SetAPIUpdateCall(true);

        LineNo := SalesLine."Line No.";

        SalesLine.Modify(false);
    end;

    procedure CreateShippingChargeSalesOrderLine(SalHdr: Record "Sales Header"; ShippingChargeAmount: Decimal)
    var
        SalesLine: Record "Sales Line";
        GLAcc: Record "G/L Account";
    begin
        i95Setup.get();

        SalesLine.init();
        SalesLine.Validate("Document Type", SalesLine."Document Type"::Order);
        SalesLine.validate("Document No.", SalHdr."No.");
        SalesLine.Validate("Sell-to Customer No.", SalHdr."Sell-to Customer No.");
        SalesLine.validate("Line No.", LineNo + 10000);
        SalesLine.insert();
        SalesLine.validate(Type, SalesLine.Type::"G/L Account");

        If GLAcc.get(i95Setup."i95 Shipping Charge G/L Acc") then
            SalesLine.validate("No.", i95Setup."i95 Shipping Charge G/L Acc");

        SalesLine.validate(Quantity, 1);

        if i95Setup."i95 Default Warehouse" <> '' then
            SalesLine.Validate("Location Code", i95Setup."i95 Default Warehouse");

        SalesLine.validate("Unit Price", ShippingChargeAmount);
        SalesLine.i95SetAPIUpdateCall(true);

        LineNo := SalesLine."Line No.";

        SalesLine.Modify(false);
    end;

    procedure UpdateProductData()
    var
        InventorySetup: Record "Inventory Setup";
        ItemLedgEntry: Record "Item Ledger Entry";
        NewItem: Record Item;
        SourceNo: Code[20];
        Name: Text;
        Price: Decimal;
        Weight: Decimal;
        Cost: Decimal;
        Description: Text;
        Sku: Code[20];
    begin
        ResultDataJsonArray := i95WebserviceExecuteCU.GetResultDataJsonArray();
        ResultJsonToken := i95WebserviceExecuteCU.GetResultJsonToken();
        ResultDataJsonToken := i95WebserviceExecuteCU.GetResultDataJsonToken();
        ResultDataJsonObject := i95WebserviceExecuteCU.GetResultDataJsonObject();

        i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"No Response", LogStatus::Cancelled, i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);

        foreach ResultDataJsonToken in ResultDataJsonArray do begin
            SourceNo := '';

            ResultDataJsonObject := ResultDataJsonToken.AsObject();
            SourceNo := i95WebserviceExecuteCU.ProcessJsonTokenasCode('SourceId', ResultDataJsonObject);
            TargetID := i95WebserviceExecuteCU.ProcessJsonTokenasCode('TargetId', ResultDataJsonObject);
            MessageID := i95WebServiceExecuteCU.ProcessJsontokenasInteger('MessageId', ResultDataJsonObject);
            StatusID := i95WebServiceExecuteCU.ProcessJsonTokenasInteger('StatusId', ResultDataJsonObject);
            SourceID := i95WebServiceExecuteCU.ProcessJsonTokenascode('SourceId', ResultDataJsonObject);
            i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"Waiting for Sync", LogStatus::New, HttpReasonCode, ResponseResultText, ResponseMessageText, MessageID, SourceID, StatusID, SyncSource::i95);
            if ResultDataJsonObject.Contains('InputData') then begin
                ResultDataJsonObject.get('InputData', ResultDataJsonToken);
                ResultDataJsonObject := ResultDataJsonToken.AsObject();

                Name := i95WebserviceExecuteCU.ProcessJsonTokenasText('name', ResultDataJsonObject);
                Sku := i95WebServiceExecuteCU.ProcessJsonTokenasCode('sku', ResultDataJsonObject);
                Price := i95WebServiceExecuteCU.ProcessJsonTokenasDecimal('price', ResultDataJsonObject);
                Weight := i95WebServiceExecuteCU.ProcessJsonTokenasDecimal('weight', ResultDataJsonObject);
                Cost := i95WebServiceExecuteCU.ProcessJsonTokenasDecimal('cost', ResultDataJsonObject);
                Description := i95WebServiceExecuteCU.ProcessJsonTokenasText('description', ResultDataJsonObject);

                clear(SourceRecordID);
                InventorySetup.Get();
                i95Setup.get();
                If SourceNo <> '' then begin
                    NewItem.reset();
                    NewItem.SetCurrentKey("i95 Reference ID");
                    NewItem.SetRange("i95 Reference ID", SourceNo);
                    If not NewItem.FindFirst() then begin
                        NewItem.init();

                        If i95Setup."i95 Use Item Nos. from E-COM" then
                            NewItem.Validate("No.", Sku)
                        else
                            if i95Setup."Product Nos." <> '' then
                                NewItem.validate("No.", NoSeriesMgt.GetNextNo(i95Setup."Product Nos.", 0D, true))
                            else
                                NewItem.validate("No.", NoSeriesMgt.GetNextNo(InventorySetup."Item Nos.", 0D, true));

                        NewItem."i95 Created By" := 'i95';
                        NewItem."i95 Created DateTime" := CurrentDateTime();
                        NewItem."i95 Creation Source" := NewItem."i95 Creation Source"::i95;
                        NewItem.insert();
                    end;

                    NewItem.Validate(NewItem.Description, Name);
                    NewItem.validate(NewItem."Unit Price", Price);

                    If NewItem."Costing Method" = NewItem."Costing Method"::Standard then
                        NewItem.validate(NewItem."Unit Cost", Cost)
                    else begin
                        ItemLedgEntry.SETCURRENTKEY("Item No.");
                        ItemLedgEntry.SETRANGE("Item No.", NewItem."No.");
                        IF ItemLedgEntry.ISEMPTY() THEN
                            NewItem.validate(NewItem."Unit Cost", Cost);
                    end;

                    NewItem.Validate(NewItem."Gross Weight", Weight);
                    If i95Setup."Default UOM" <> '' then
                        NewItem.Validate(NewItem."Base Unit of Measure", i95Setup."Default UOM");
                    //NewItem.Validate(NewItem."Tax Group Code", TaxGroupCode);
                    If i95Setup."i95 Gen. Prod. Posting Group" <> '' then
                        NewItem.Validate(NewItem."Gen. Prod. Posting Group", i95Setup."i95 Gen. Prod. Posting Group");
                    if i95Setup."i95 Inventory Posting Group" <> '' then
                        NewItem.Validate(NewItem."Inventory Posting Group", i95Setup."i95 Inventory Posting Group");
                    If i95Setup."i95 Tax Group Code" <> '' then
                        NewItem.Validate(NewItem."Tax Group Code", i95Setup."i95 Tax Group Code");
                    NewItem.Validate(NewItem."Description 2", Description);
                    NewItem.Modify(false);

                    SourceRecordID := NewItem.RecordId();

                    NewItem.Seti95APIUpdateCall(true);
                    NewItem."i95 Last Modification DateTime" := CurrentDateTime();
                    NewItem."i95 Last Modified By" := copystr(UserId(), 1, 80);
                    NewItem."i95 Last Sync DateTime" := CurrentDateTime();
                    NewItem."i95 Sync Status" := NewItem."i95 Sync Status"::"Waiting for Response";
                    NewItem."i95 Last Modification Source" := NewItem."i95 Last Modification Source"::i95;
                    NewItem."i95 Reference ID" := SourceNo;
                    NewItem.Modify(false);

                    DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::Product, SyncStatus::"Waiting for Response", LogStatus::New, SyncSource::i95, i95SyncLogEntryNo, NewItem."No.", NewItem.Description, '', NewItem.RecordId(), Database::Item);

                    i95SyncLogEntry."Error Message" := '';
                    i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"Waiting for Response", LogStatus::"In-Progress", i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);
                    i95SyncLogEntry.SetSourceRecordID(SourceRecordID);
                    i95SyncLogEntry.UpdateDataStatus(DataStatus::"Data Updated");
                end else
                    i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"No Response", LogStatus::Cancelled, i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);
            end;
        end;
    end;

    procedure UpdateSchedulerID()
    var
        i95DevSetup: Record "i95 Setup";
        InputJsonObject: JsonObject;
        SchedulerIDTxt: Text;
    begin
        ResultDataJsonArray := i95WebserviceExecuteCU.GetResultDataJsonArray();
        InputJsonObject := i95WebserviceExecuteCU.GetResultDataJsonObject();
        SchedulerIDTxt := '';
        SchedulerIDTxt := i95WebserviceExecuteCU.ProcessJsonTokenasText('SchedulerId', InputJsonObject);

        clear(SourceRecordID);
        If SchedulerIDTxt <> '' then begin
            If not i95DevSetup.get() then begin
                i95DevSetup.init();
                i95DevSetup."Schedular ID" := copystr(SchedulerIDTxt, 1, 50);
                i95DevSetup.insert();
                SourceRecordID := i95DevSetup.RecordId();
            end else begin
                i95DevSetup."Schedular ID" := copystr(SchedulerIDTxt, 1, 50);
                i95DevSetup.Modify(false);
                SourceRecordID := i95DevSetup.RecordId();
            end;

            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::SchedulerID, SyncStatus::"Waiting for Response", LogStatus::New, SyncSource::i95, i95SyncLogEntryNo, i95DevSetup."Schedular ID", '', '', i95DevSetup.RecordId(), Database::"i95 Setup");
            DetailedSyncLogEntry.UpdateSyncLogEntry(SyncStatus::"Sync Complete", LogStatus::Completed, i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Message ID", '', i95SyncLogEntry."Status ID"::Complete, '', SyncSource::i95);

            i95SyncLogEntry."Error Message" := '';
            i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"Sync Complete", LogStatus::Completed, i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID"::Complete, SyncSource::i95);
            i95SyncLogEntry.SetSourceRecordID(SourceRecordID);
            i95SyncLogEntry.UpdateDataStatus(DataStatus::"Data Updated");
        end else
            i95SyncLogEntry.UpdateSyncLogEntry(SyncStatus::"No Response", LogStatus::Cancelled, i95SyncLogEntry."Http Response Code", i95SyncLogEntry."Response Result", i95SyncLogEntry."Response Message", i95SyncLogEntry."Message ID", i95SyncLogEntry."i95 Source ID", i95SyncLogEntry."Status ID", SyncSource::i95);
    end;

    var
        i95SyncLogEntry: Record "i95 Sync Log Entry";
        i95Setup: Record "i95 Setup";
        SalesReceivablesSetup: Record "Sales & Receivables Setup";
        DetailedSyncLogEntry: Record "i95 Detailed Sync Log Entry";
        i95WebServiceExecuteCU: Codeunit "i95 Webservice Execute";
        NoSeriesMgt: Codeunit NoSeriesManagement;
        SourceRecordID: RecordId;
        SecSourceRecordID: RecordId;
        WebServiceJsonObject: JsonObject;
        ResultDataJsonArray: JsonArray;
        ResultJsonToken: JsonToken;
        ResultDataJsonToken: JsonToken;
        ResultDataJsonObject: JsonObject;
        MessageID: Integer;
        SourceID: code[20];
        StatusID: Option "Request Received","Request Inprocess","Error","Response Received","Response Transferred","Complete";
        APIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID;
        SyncStatus: Option "Waiting for Sync","Waiting for Response","Waiting for Acknowledgement","Sync Complete","No Response";
        LogStatus: Option " ",New,"In-Progress",Completed,Error,Cancelled;
        SyncSource: Option "","Business Central",i95;
        SalesInputDataJsonArray: JsonArray;
        SalesInputJsonToken: JsonToken;
        SalesInputDataJsonToken: JsonToken;
        SalesInputDataJsonObject: JsonObject;
        SOAddressJsonObject: JsonObject;
        SalesLineInputDataJsonArray: JsonArray;
        SalesLineInputDataJsonToken: JsonToken;
        SalesLineInputDataJsonObject: JsonObject;
        PaymentInputDataJsonarray: JsonArray;
        PaymentInputDataJsonToken: JsonToken;
        PaymentInputDataJsonObject: JsonObject;
        DiscountAmountInputDataJsonArray: JsonArray;
        DiscountAmountInputDataJsonToken: JsonToken;
        DiscountAmountInputDataJsonObject: JsonObject;
        LineNo: Integer;
        ItemNo: code[20];
        ItemPrice: Decimal;
        QuantityOrdered: Decimal;
        SpecialPrice: Decimal;
        LineDiscountAmount: Decimal;
        TargetId: code[20];
        DataStatus: Option "","Data Received","Data Updated";
        i95SyncLogEntryNo: Integer;
        APIDatatoRead: Option "i95 Sync Request","i95 Sync Result","i95 Response Request","i95 Response Result","i95 Acknowledgement Request","i95 Acknowledgement Result";
        HttpReasonCode: text[100];
        ResponseResultText: Text[30];
        ResponseMessageText: Text[100];
        RetailVariantId: Code[20];
        ParentSku: Code[20];
}