Codeunit 70328081 "i95 Event Subscriber"
{
    [EventSubscriber(ObjectType::Codeunit, codeunit::"Item Jnl.-Post Line", 'OnAfterInitItemLedgEntry', '', False, False)]
    local procedure OnAfterInitItemLedgEntry(VAR NewItemLedgEntry: Record "Item Ledger Entry"; ItemJournalLine: Record "Item Journal Line"; VAR ItemLedgEntryNo: Integer)
    begin
        SetInventoryPendingSync(NewItemLedgEntry);
    end;

    [EventSubscriber(ObjectType::Codeunit, codeunit::"Sales-Post", 'OnRunOnBeforeFinalizePosting', '', false, false)]
    local procedure OnRunOnBeforeFinalizePosting(Var SalesHeader: Record "Sales Header"; var SalesShipmentHeader: Record "Sales Shipment Header"; var SalesInvoiceHeader: Record "Sales Invoice Header"; var SalesCrMemoHeader: Record "Sales Cr.Memo Header")
    begin
        Updatei95FieldsOnSalesPosting(SalesShipmentHeader, SalesInvoiceHeader);
    end;

    [EventSubscriber(ObjectType::Codeunit, codeunit::"Shipment Header - Edit", 'OnBeforeSalesShptHeaderModify', '', false, false)]
    local procedure OnBeforeSalesShptHeaderModify(var SalesShptHeader: Record "Sales Shipment Header"; FromSalesShptHeader: Record "Sales Shipment Header")
    begin
        Updatei95SyncStatus(SalesShptHeader);
    end;

    [EventSubscriber(ObjectType::Codeunit, codeunit::"Sales-Post", 'OnBeforePostSalesDoc', '', false, false)]
    local procedure OnBeforePostSalesDoc(var SalesHeader: Record "Sales Header"; CommitIsSuppressed: Boolean; PreviewMode: Boolean; var HideProgressWindow: Boolean)
    begin
        SalesHeader.SetCalledFromPosting(true);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostLines', '', false, false)]
    local procedure OnBeforePostLines(var SalesLine: Record "Sales Line"; SalesHeader: Record "Sales Header"; CommitIsSuppressed: Boolean; PreviewMode: Boolean)
    begin
        SalesLine.SetCalledFromPosting(true);
    end;

    local procedure SetInventoryPendingSync(ItemLedgEntry: Record "Item Ledger Entry")
    var
        Item: Record Item;
        ItemVariant: Record "Item Variant";
        i95Setup: Record "i95 Setup";
    begin
        i95Setup.get();
        if (i95Setup."i95 Default Warehouse" <> '') and (i95Setup."i95 Default Warehouse" <> ItemLedgEntry."Location Code") then
            exit;

        if (ItemLedgEntry."Variant Code" = '') and (Item.get(ItemLedgEntry."Item No.")) then
            Item.Seti95InventoryPendingSync()
        else
            if (ItemLedgEntry."Variant Code" <> '') and (ItemVariant.get(ItemLedgEntry."Item No.", ItemLedgEntry."Variant Code")) then
                ItemVariant.Seti95InventoryPendingSync();
    end;

    local procedure Updatei95FieldsOnSalesPosting(Var SalesShipmentHeader: Record "Sales Shipment Header"; Var SalesInvoiceHeader: Record "Sales Invoice Header")
    begin
        if (SalesShipmentHeader."No." <> '') then begin
            SalesShipmentHeader."i95 Created Date Time" := CurrentDateTime();
            SalesShipmentHeader."i95 Sync Status" := SalesShipmentHeader."i95 Sync Status"::"Waiting for Sync";
            SalesShipmentHeader.Modify();
        end;

        if (SalesInvoiceHeader."No." <> '') then begin
            SalesInvoiceHeader."i95 Created Date Time" := CurrentDateTime();
            SalesInvoiceHeader."i95 Sync Status" := SalesInvoiceHeader."i95 Sync Status"::"Waiting for Sync";
            SalesInvoiceHeader.Modify();
        end;
    end;

    [EventSubscriber(ObjectType::Table, 36, 'OnAfterGetNoSeriesCode', '', false, false)]
    local procedure OnAfterGetNoSeriesCode(var SalesHeader: Record "Sales Header"; SalesReceivablesSetup: Record "Sales & Receivables Setup"; var NoSeriesCode: Code[20])
    var
        i95Setup: Record "i95 Setup";
    begin
        i95Setup.Get();

        if i95Setup."Order Nos." <> '' then
            NoSeriesCode := i95Setup."Order Nos.";

    end;

    local procedure Updatei95SyncStatus(var SalShipmentHdr: Record "Sales Shipment Header")
    begin
        SalShipmentHdr.Updatei95Fields();
    end;
}