codeunit 70328075 "i95 Push Webservice"
{
    Permissions = tabledata "Sales Shipment Header" = rm, tabledata "Sales Shipment Line" = rm, tabledata "Sales Invoice Header" = rm, tabledata "Sales Invoice Line" = rm;

    procedure ProductPushData(Var Item: Record Item)
    var
        BodyContent: text;
    begin
        Clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.Get();
        Checki95SetupData();

        CalledByItemVariant := False;

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::Product, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::Product, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.ProductPushData(Item, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::Product, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, Item."No.", Item.Description, '', item.RecordId(), Database::Item);
        until Item.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        SyncLogEntry.SetSourceRecordID(Item.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::Product, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::Product);
    end;

    procedure InventoryPushData(var Item: Record Item)
    var
        BodyContent: text;
        InventoryString: Text[30];
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);
        Clear(InventoryString);

        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::Inventory, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::Inventory, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            if i95Setup."i95 Default Warehouse" <> '' then
                Item.SetFilter(item."Location Filter", '%1', i95Setup."i95 Default Warehouse");
            Item.CalcFields(Item.Inventory);

            InventoryString := format(Item.Inventory);
            InventoryString := DelChr(InventoryString, '=', ',');
            CreateBodyContent.InventoryPushData(Item, InventoryString, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::Inventory, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, Item."No.", Item.Description, '', item.RecordId(), Database::Item);
        until Item.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);


        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        SyncLogEntry.SetSourceRecordID(Item.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::Inventory, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::Inventory);
    end;

    procedure CustomerPushData(var Customer: Record Customer)
    var
        ShipToAddress: Record "Ship-to Address";
        BodyContent: Text;
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);


        i95Setup.get();
        Checki95SetupData();
        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::Customer, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::Customer, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            If not ShipToAddress.get(Customer."No.", 'I95DEFAULT') then
                Customer.Createi95DefaultShiptoAddress();

            CreateBodyContent.CustomerPushData(Customer, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::Customer, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, Customer."No.", Customer.Name, '', Customer.RecordId(), Database::customer);
        until Customer.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        SyncLogEntry.SetSourceRecordID(Customer.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::Customer, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::Customer);

    end;

    procedure CustomerPriceGroupPushData(var CustPriceGroup: Record "Customer Price Group")
    var
        BodyContent: Text;
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::CustomerGroup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::CustomerGroup, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.CustomerPriceGroupPushData(CustPriceGroup, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::CustomerGroup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, CustPriceGroup.Code, CustPriceGroup.Description, '', CustPriceGroup.RecordId(), Database::"Customer Price Group");
        until CustPriceGroup.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

        SyncLogEntry.SetSourceRecordID(CustPriceGroup.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::CustomerGroup, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::CustomerGroup);
    end;

    procedure SalesPricePushData(var Item: Record Item)
    var
        BodyContent: Text;
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::TierPrices, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::TierPrices, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.SalesPricePushData(Item, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::TierPrices, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, Item."No.", Item.Description, '', Item.RecordId(), Database::item);
        until Item.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

        SyncLogEntry.SetSourceRecordID(Item.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::TierPrices, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::TierPrices);
    end;

    procedure TaxBusPostingGrpPushData(Var TaxBusPostingGrp: Record "VAT Business Posting Group")
    var
        BodyContent: Text;
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::TaxBusPostingGroup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::TaxBusPostingGroup, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.TaxBusPostingGroupPushData(TaxBusPostingGrp, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::TaxBusPostingGroup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, TaxBusPostingGrp.Code, TaxBusPostingGrp.Description, '', TaxBusPostingGrp.RecordId(), Database::"VAT Business Posting Group");
        until TaxBusPostingGrp.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

        SyncLogEntry.SetSourceRecordID(TaxBusPostingGrp.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::TaxBusPostingGroup, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;
        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::TaxBusPostingGroup);
    end;

    procedure TaxProdPostingGrpPushData(Var TaxProdPostingGrp: Record "VAT Product Posting Group")
    var
        BodyContent: Text;
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::TaxProductPostingGroup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::TaxProductPostingGroup, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.TaxProdPostingGroupPushData(TaxProdPostingGrp, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::TaxProductPostingGroup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, TaxProdPostingGrp.Code, TaxProdPostingGrp.Description, '', TaxProdPostingGrp.RecordId(), Database::"VAT Product Posting Group");
        until TaxProdPostingGrp.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

        SyncLogEntry.SetSourceRecordID(TaxProdPostingGrp.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::TaxProductPostingGroup, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::TaxProductPostingGroup);
    end;

    procedure TaxPostingSetupPushData(Var TaxPostingSetup: Record "VAT Posting Setup")
    var
        BodyContent: Text;
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::TaxPostingSetup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::TaxPostingSetup, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.TaxPostingSetupPushData(TaxPostingSetup, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::TaxPostingSetup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, TaxPostingSetup."VAT Bus. Posting Group", TaxPostingSetup."VAT Prod. Posting Group", format(TaxPostingSetup."VAT %"), TaxPostingSetup.RecordId(), Database::"VAT Posting Setup");
        until TaxPostingSetup.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

        SyncLogEntry.SetSourceRecordID(TaxPostingSetup.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::TaxPostingSetup, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::TaxPostingSetup);
    end;

    procedure CustomerDiscGroupPushData(var CustDiscGroup: Record "Customer Discount Group")
    var
        BodyContent: Text;
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::CustomerDiscountGroup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::CustomerDiscountGroup, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.CustomerDiscountGroupPushData(CustDiscGroup, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::CustomerDiscountGroup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, CustDiscGroup.Code, CustDiscGroup.Description, '', CustDiscGroup.RecordId(), Database::"Customer Discount Group");
        until CustDiscGroup.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

        SyncLogEntry.SetSourceRecordID(CustDiscGroup.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::CustomerDiscountGroup, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::CustomerDiscountGroup);
    end;

    procedure ItemDiscGroupPushData(var ItemDiscGroup: Record "Item Discount Group")
    var
        BodyContent: Text;
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::ItemDiscountGroup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::ItemDiscountGroup, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.ItemDiscountGroupPushData(ItemDiscGroup, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::ItemDiscountGroup, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, ItemDiscGroup.Code, ItemDiscGroup.Description, '', ItemDiscGroup.RecordId(), Database::"Item Discount Group");
        until ItemDiscGroup.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

        SyncLogEntry.SetSourceRecordID(ItemDiscGroup.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::ItemDiscountGroup, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::ItemDiscountGroup);
    end;

    procedure SalesOrderPushData(var SalesHeader: Record "Sales Header")
    var
        ShippingAgentMapping: Record "i95 Shipping Agent Mapping";
        PaymentMethodsMapping: Record "i95 Payment Methods Mapping";
        BodyContent: Text;
        BodyContentExist: Boolean;
        IsShipAgentErrorExists: Boolean;
        IsPaymentMethodErrorExists: Boolean;
        ShippingAgentCode: Text[30];
        EcommerceShipAgentCode: Code[50];
        EcommerceShipAgentDescription: text[50];
        EcommercePaymentMethodCode: Code[50];
        SyncLogInserted: Boolean;
        ShipAgentErrorTxt: Label 'Shipping Agent Code cannot be blank for Sales Order %1.';
        ShipAgentMappingErrorTxt: Label 'Shipping Agent Mapping does not exist for %1 in Sales Order %2.';
        PaymentMethodErrorTxt: Label 'Payment Method Code cannot be blank for Sales Order %1.';
        PaymentMethodMappingErrorTxt: Label 'Payment Method Mapping does not exist for %1 in Sales Order %2.';
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        Checki95SetupData();

        repeat
            if IsNotSyncedSalesOrder(SalesHeader) then begin
                if not SyncLogInserted then begin
                    SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::SalesOrder, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
                    APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::SalesOrder, SchedulerType::PushData, SyncSource::"Business Central");

                    SyncLogInserted := true;
                    CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
                end;

                Clear(EcommerceShipAgentCode);
                Clear(EcommerceShipAgentDescription);
                Clear(ShippingAgentCode);
                Clear(EcommercePaymentMethodCode);
                clear(IsShipAgentErrorExists);
                clear(IsPaymentMethodErrorExists);

                If SalesHeader."Shipping Agent Code" <> '' then begin
                    ShippingAgentMapping.Reset();
                    ShippingAgentMapping.SetRange(ShippingAgentMapping."BC Shipping Agent Code", SalesHeader."Shipping Agent Code");
                    If SalesHeader."Shipping Agent Service Code" <> '' then
                        ShippingAgentMapping.setrange(ShippingAgentMapping."BC Shipping Agent Service Code", SalesHeader."Shipping Agent Service Code");
                    If ShippingAgentMapping.FindFirst() then begin
                        EcommerceShipAgentCode := ShippingAgentMapping."E-Com Shipping Method Code";
                        EcommerceShipAgentDescription := ShippingAgentMapping."E-Com Shipping Description";
                        If ShippingAgentMapping."BC Shipping Agent Service Code" <> '' then
                            ShippingAgentCode := ShippingAgentMapping."BC Shipping Agent Code" + '-' + ShippingAgentMapping."BC Shipping Agent Service Code"
                        else
                            ShippingAgentCode := ShippingAgentMapping."BC Shipping Agent Code";
                    end else
                        IsShipAgentErrorExists := true;
                end else
                    IsShipAgentErrorExists := true;

                If SalesHeader."Payment Method Code" <> '' then begin
                    PaymentMethodsMapping.Reset();
                    PaymentMethodsMapping.SetRange(PaymentMethodsMapping."BC Payment Method Code", SalesHeader."Payment Method Code");
                    PaymentMethodsMapping.SetRange(PaymentMethodsMapping."BC to Ecommerce Default", true);
                    If PaymentMethodsMapping.FindFirst() then
                        EcommercePaymentMethodCode := PaymentMethodsMapping."E-Commerce Payment Method Code"
                    else begin
                        PaymentMethodsMapping.SetRange(PaymentMethodsMapping."BC to Ecommerce Default");
                        If paymentMethodsMapping.FindFirst() then
                            EcommercePaymentMethodCode := PaymentMethodsMapping."E-Commerce Payment Method Code"
                        else
                            IsPaymentMethodErrorExists := true;
                    end;
                end else
                    IsPaymentMethodErrorExists := true;

                if (IsShipAgentErrorExists or IsPaymentMethodErrorExists) then begin
                    DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::SalesOrder, SyncStatus::"No Response", LogStatus::Cancelled, SyncSource::"Business Central", SyncLogEntryNo, SalesHeader."No.", SalesHeader."Sell-to Customer No.", '', SalesHeader.RecordId(), Database::"Sales Header");
                    if SalesHeader."Shipping Agent Code" = '' then
                        DetailedSyncLogEntry.UpdateErrorMessage(StrSubstNo(ShipAgentErrorTxt, SalesHeader."No."))
                    else
                        if IsShipAgentErrorExists then
                            DetailedSyncLogEntry.UpdateErrorMessage(StrSubstNo(ShipAgentMappingErrorTxt, SalesHeader."Shipping Agent Code", SalesHeader."No."))
                        else
                            if SalesHeader."Payment Method Code" = '' then
                                DetailedSyncLogEntry.UpdateErrorMessage(StrSubstNo(PaymentMethodErrorTxt, SalesHeader."No."))
                            else
                                DetailedSyncLogEntry.UpdateErrorMessage(StrSubstNo(PaymentMethodMappingErrorTxt, SalesHeader."Payment Method Code", SalesHeader."No."));
                end;

                If (not IsShipAgentErrorExists) and (not IsPaymentMethodErrorExists) and (SalesHeader."i95 Sync Message" = '') then begin
                    CreateBodyContent.SalesOrderPushData(salesHeader, BodyContent, EcommerceShipAgentCode, EcommercePaymentMethodCode, EcommerceShipAgentDescription, ShippingAgentCode);
                    DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::SalesOrder, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, SalesHeader."No.", SalesHeader."Sell-to Customer No.", '', SalesHeader.RecordId(), Database::"Sales Header");
                    BodyContentExist := true;
                end;
            end;
        until SalesHeader.Next() = 0;

        if SyncLogInserted then begin
            CreateBodyContent.AddContextFooter(BodyContent);

            SyncLogEntry.reset();
            if SyncLogEntry.get(SyncLogEntryNo) then;

            APILogEntry.reset();
            if APILogEntry.get(APILogEntryNo) then;

            SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
            APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

            SyncLogEntry.SetSourceRecordID(SalesHeader.RecordId());
            commit();

            i95WebserviceExecuteCU.GetAPIUrl(APIType::SalesOrder, SchedulerType::PushData);
            i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

            if TestMode then begin
                i95WebserviceExecuteCU.SetTestMode(true);
                i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
            end;

            if BodyContentExist then
                ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::SalesOrder);
        end;
    end;

    procedure SalesShipmentPushData(var SalesShipmentHeader: Record "Sales Shipment Header")
    var
        ShippingAgentMapping: Record "i95 Shipping Agent Mapping";
        BodyContent: Text;
        BodyContentExist: Boolean;
        IsShipAgentErrorExists: Boolean;
        ShippingAgentCode: Text[30];
        EcommerceShippingCode: Code[50];
        EcommerceShippingDescription: text[50];
        ShipAgentErrorTxt: Label 'Shipping Agent Code cannot be blank for Sales Shipment %1.';
        ShipAgentMappingErrorTxt: Label 'Shipping Agent Mapping does not exist for %1 in Sales Shipment %2.';
    begin
        Clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.Get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::Shipment, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::Shipment, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            Clear(ShippingAgentCode);
            Clear(EcommerceShippingCode);
            Clear(EcommerceShippingDescription);

            If SalesShipmentHeader."Shipping Agent Code" <> '' then begin
                ShippingAgentMapping.Reset();
                ShippingAgentMapping.SetRange(ShippingAgentMapping."BC Shipping Agent Code", SalesShipmentHeader."Shipping Agent Code");
                If SalesShipmentHeader."Shipping Agent Service Code" <> '' then
                    ShippingAgentMapping.setrange(ShippingAgentMapping."BC Shipping Agent Service Code", SalesShipmentHeader."Shipping Agent Service Code");
                If ShippingAgentMapping.FindFirst() then begin
                    EcommerceShippingCode := ShippingAgentMapping."E-Com Shipping Method Code";
                    EcommerceShippingDescription := ShippingAgentMapping."E-Com Shipping Description";
                    If ShippingAgentMapping."BC Shipping Agent Service Code" <> '' then
                        ShippingAgentCode := ShippingAgentMapping."BC Shipping Agent Code" + '-' + ShippingAgentMapping."BC Shipping Agent Service Code"
                    else
                        ShippingAgentCode := ShippingAgentMapping."BC Shipping Agent Code";
                end else
                    IsShipAgentErrorExists := true;
            end else
                IsShipAgentErrorExists := true;

            if IsShipAgentErrorExists then begin
                DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::Shipment, SyncStatus::"No Response", LogStatus::Cancelled, SyncSource::"Business Central", SyncLogEntryNo, SalesShipmentHeader."No.", SalesShipmentHeader."Sell-to Customer No.", '', SalesShipmentHeader.RecordId(), Database::"Sales Shipment Header");
                If SalesShipmentHeader."Shipping Agent Code" = '' then
                    DetailedSyncLogEntry.UpdateErrorMessage(StrSubstNo(ShipAgentErrorTxt, SalesShipmentHeader."No."))
                else
                    DetailedSyncLogEntry.UpdateErrorMessage(StrSubstNo(ShipAgentMappingErrorTxt, SalesShipmentHeader."Shipping Agent Code", SalesShipmentHeader."No."))
            end;

            If (not IsShipAgentErrorExists) and (SalesShipmentHeader."i95 Sync Message" = '') then begin
                CreateBodyContent.SalesShipmentPushData(SalesShipmentHeader, BodyContent, EcommerceShippingCode, EcommerceShippingDescription, ShippingAgentCode);
                DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::Shipment, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, SalesShipmentHeader."No.", SalesShipmentHeader."Sell-to Customer No.", '', SalesShipmentHeader.RecordId(), Database::"Sales Shipment Header");
                BodyContentExist := true;
            end;
        until SalesShipmentHeader.Next() = 0;

        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        SyncLogEntry.SetSourceRecordID(SalesShipmentHeader.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::Shipment, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        if BodyContentExist then
            ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::Shipment);
    end;

    procedure SalesInvoicePushData(var SalesInvoiceHeader: Record "Sales Invoice Header")
    var
        BodyContent: text;
    begin
        Clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.Get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::Invoice, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::Invoice, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.SalesInvoicePushData(SalesInvoiceHeader, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::Invoice, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, SalesInvoiceHeader."No.", SalesInvoiceHeader."Sell-to Customer No.", '', SalesInvoiceHeader.RecordId(), Database::"Sales Invoice Header");
        until SalesInvoiceHeader.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        SyncLogEntry.SetSourceRecordID(SalesInvoiceHeader.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::Invoice, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::Invoice);
    end;

    procedure CancelSalesOrderPushData(var i95SyncLogEntry: Record "i95 Sync Log Entry")
    var
        DetSyncLogEntry: Record "i95 Detailed Sync Log Entry";
        BodyContent: Text;
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := i95SyncLogEntry."Entry No";
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::CancelOrder, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            DetSyncLogEntry.Reset();
            DetSyncLogEntry.SetCurrentKey("API Type", "Sync Log Entry No", "Sync Status");
            DetailedSyncLogEntry.SetRange("API Type", APIType::CancelOrder);
            DetSyncLogEntry.SetRange(DetSyncLogEntry."Sync Log Entry No", i95SyncLogEntry."Entry No");
            DetailedSyncLogEntry.SetRange("Sync Status", DetailedSyncLogEntry."Sync Status"::"Waiting for Sync");
            If DetSyncLogEntry.FindFirst() then
                CreateBodyContent.CancelOrderPushData(DetSyncLogEntry, BodyContent);
        until i95SyncLogEntry.Next() = 0;

        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::CancelOrder, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::CancelOrder);
    end;

    procedure DiscountPricePushData(var Item: Record Item)
    var
        BodyContent: Text;
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::DiscountPrice, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::DiscountPrice, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.DiscountPricePushData(Item, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::DiscountPrice, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, Item."No.", Item.Description, '', Item.RecordId(), Database::item);
        until Item.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

        SyncLogEntry.SetSourceRecordID(Item.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::DiscountPrice, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::DiscountPrice);
    end;

    procedure DiscountPriceByItemDiscGrpPushData(var ItemDiscGrp: Record "Item Discount Group")
    var
        BodyContent: Text;
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        clear(CalledByItemDiscountUpdate);
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::DiscountPrice, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::DiscountPrice, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.DiscountPriceItemDiscGrpPushData(ItemDiscGrp, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::DiscountPrice, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, ItemDiscGrp.Code, ItemDiscGrp.Description, '', ItemDiscGrp.RecordId(), database::"Item Discount Group");
        until ItemDiscGrp.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

        SyncLogEntry.SetSourceRecordID(ItemDiscGrp.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::DiscountPrice, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        CalledByItemDiscountUpdate := true;
        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::DiscountPrice);
    end;

    procedure ConfigurableProductPushData(Var Item: Record Item)
    var
        BodyContent: text;
    begin
        Clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);
        i95Setup.Get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::ConfigurableProduct, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::ConfigurableProduct, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            CreateBodyContent.ConfigurableProductPushData(Item, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::ConfigurableProduct, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, Item."No.", Item.Description, '', item.RecordId(), Database::Item);
        until Item.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        SyncLogEntry.SetSourceRecordID(Item.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::ConfigurableProduct, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::ConfigurableProduct);
    end;

    procedure ChildProductPushData(Var Item: Record Item)
    var
        ItemVariant: Record "Item Variant";
        BodyContent: text;
    begin
        Clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.Get();
        Checki95SetupData();

        CalledByItemVariant := true;

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::Product, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::Product, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            ItemVariant.Reset();
            ItemVariant.SetRange("Item No.", Item."No.");
            if ItemVariant.FindSet() then
                repeat
                    CreateBodyContent.ChildProductPushData(ItemVariant, BodyContent);
                    DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::Product, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central",
                                                            SyncLogEntryNo, ItemVariant."Item No.", ItemVariant.Code, ItemVariant.Description, ItemVariant.RecordId(), Database::"Item Variant");
                until ItemVariant.Next() = 0;
        until Item.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        SyncLogEntry.SetSourceRecordID(Item.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::Product, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::Product);
    end;

    procedure EditSalesOrderPushData(var SalesHeader: Record "Sales Header")
    var
        ShippingAgentMapping: Record "i95 Shipping Agent Mapping";
        PaymentMethodsMapping: Record "i95 Payment Methods Mapping";
        BodyContent: Text;
        BodyContentExist: Boolean;
        IsShipAgentErrorExists: Boolean;
        IsPaymentMethodErrorExists: Boolean;
        ShippingAgentCode: Text[30];
        EcommerceShipAgentCode: Code[50];
        EcommerceShipAgentDescription: text[50];
        EcommercePaymentMethodCode: Code[50];
        SyncLogInserted: Boolean;
        ShipAgentErrorTxt: Label 'Shipping Agent Code cannot be blank for Sales Order %1.';
        ShipAgentMappingErrorTxt: Label 'Shipping Agent Mapping does not exist for %1 in Sales Order %2.';
        PaymentMethodErrorTxt: Label 'Payment Method Code cannot be blank for Sales Order %1.';
        PaymentMethodMappingErrorTxt: Label 'Payment Method Mapping does not exist for %1 in Sales Order %2.';
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);

        i95Setup.get();
        Checki95SetupData();

        repeat
            if SalesHeader."i95 Reference ID" <> '' then begin
                if not SyncLogInserted then begin
                    SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::EditOrder, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
                    APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::EditOrder, SchedulerType::PushData, SyncSource::"Business Central");

                    SyncLogInserted := true;
                    CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
                end;

                Clear(EcommerceShipAgentCode);
                Clear(EcommerceShipAgentDescription);
                Clear(ShippingAgentCode);
                Clear(EcommercePaymentMethodCode);
                clear(IsShipAgentErrorExists);
                clear(IsPaymentMethodErrorExists);

                If SalesHeader."Shipping Agent Code" <> '' then begin
                    ShippingAgentMapping.Reset();
                    ShippingAgentMapping.SetRange(ShippingAgentMapping."BC Shipping Agent Code", SalesHeader."Shipping Agent Code");
                    If SalesHeader."Shipping Agent Service Code" <> '' then
                        ShippingAgentMapping.setrange(ShippingAgentMapping."BC Shipping Agent Service Code", SalesHeader."Shipping Agent Service Code");
                    If ShippingAgentMapping.FindFirst() then begin
                        EcommerceShipAgentCode := ShippingAgentMapping."E-Com Shipping Method Code";
                        EcommerceShipAgentDescription := ShippingAgentMapping."E-Com Shipping Description";
                        If ShippingAgentMapping."BC Shipping Agent Service Code" <> '' then
                            ShippingAgentCode := ShippingAgentMapping."BC Shipping Agent Code" + '-' + ShippingAgentMapping."BC Shipping Agent Service Code"
                        else
                            ShippingAgentCode := ShippingAgentMapping."BC Shipping Agent Code";
                    end else
                        IsShipAgentErrorExists := true;
                end else
                    IsShipAgentErrorExists := true;

                If SalesHeader."Payment Method Code" <> '' then begin
                    PaymentMethodsMapping.Reset();
                    PaymentMethodsMapping.SetRange(PaymentMethodsMapping."BC Payment Method Code", SalesHeader."Payment Method Code");
                    PaymentMethodsMapping.SetRange(PaymentMethodsMapping."BC to Ecommerce Default", true);
                    If PaymentMethodsMapping.FindFirst() then
                        EcommercePaymentMethodCode := PaymentMethodsMapping."E-Commerce Payment Method Code"
                    else begin
                        PaymentMethodsMapping.SetRange(PaymentMethodsMapping."BC to Ecommerce Default");
                        If paymentMethodsMapping.FindFirst() then
                            EcommercePaymentMethodCode := PaymentMethodsMapping."E-Commerce Payment Method Code"
                        else
                            IsPaymentMethodErrorExists := true;
                    end;
                end else
                    IsPaymentMethodErrorExists := true;

                if (IsShipAgentErrorExists or IsPaymentMethodErrorExists) then begin
                    DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::EditOrder, SyncStatus::"No Response", LogStatus::Cancelled, SyncSource::"Business Central", SyncLogEntryNo, SalesHeader."No.", SalesHeader."Sell-to Customer No.", '', SalesHeader.RecordId(), Database::"Sales Header");
                    if SalesHeader."Shipping Agent Code" = '' then
                        DetailedSyncLogEntry.UpdateErrorMessage(StrSubstNo(ShipAgentErrorTxt, SalesHeader."No."))
                    else
                        if IsShipAgentErrorExists then
                            DetailedSyncLogEntry.UpdateErrorMessage(StrSubstNo(ShipAgentMappingErrorTxt, SalesHeader."Shipping Agent Code", SalesHeader."No."))
                        else
                            if SalesHeader."Payment Method Code" = '' then
                                DetailedSyncLogEntry.UpdateErrorMessage(StrSubstNo(PaymentMethodErrorTxt, SalesHeader."No."))
                            else
                                DetailedSyncLogEntry.UpdateErrorMessage(StrSubstNo(PaymentMethodMappingErrorTxt, SalesHeader."Payment Method Code", SalesHeader."No."));
                end;

                If (not IsShipAgentErrorExists) and (not IsPaymentMethodErrorExists) and (SalesHeader."i95 Sync Message" = '') then begin
                    CreateBodyContent.EditSalesOrderPushData(salesHeader, BodyContent, EcommerceShipAgentCode, EcommercePaymentMethodCode, EcommerceShipAgentDescription, ShippingAgentCode);
                    DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::EditOrder, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, SalesHeader."No.", SalesHeader."Sell-to Customer No.", '', SalesHeader.RecordId(), Database::"Sales Header");
                    BodyContentExist := true;
                end;
            end;
        until SalesHeader.Next() = 0;

        if SyncLogInserted then begin
            CreateBodyContent.AddContextFooter(BodyContent);

            SyncLogEntry.reset();
            if SyncLogEntry.get(SyncLogEntryNo) then;

            APILogEntry.reset();
            if APILogEntry.get(APILogEntryNo) then;

            SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
            APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");

            SyncLogEntry.SetSourceRecordID(SalesHeader.RecordId());
            commit();

            i95WebserviceExecuteCU.GetAPIUrl(APIType::EditOrder, SchedulerType::PushData);
            i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

            if TestMode then begin
                i95WebserviceExecuteCU.SetTestMode(true);
                i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
            end;

            if BodyContentExist then
                ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::EditOrder);
        end;
    end;

    procedure ProcessPullResponse(CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice)
    var
        ItemVariant: Record "Item Variant";
        ItemDiscGrp: Record "Item Discount Group";
        BodyContent: Text;
    begin
        Clear(ResultDataBlank);
        clear(CalledByItemDiscountUpdate);
        clear(CalledByItemVariant);
        i95Setup.get();
        Checki95SetupData();

        i95WebserviceExecuteCU.GetAPIUrl(CurrentAPIType, SchedulerType::PullResponse);
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, CurrentAPIType, SchedulerType::PullResponse, SyncSource::"Business Central");

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        SetPullResponseDefaults(CurrentAPIType);

        CreateBodyContent.AddContextHeader(BodyContent, 'PullResponse');
        DetailedSyncLogEntry.Reset();
        DetailedSyncLogEntry.SetCurrentKey("API Type", "Sync Log Entry No", "Sync Status");
        DetailedSyncLogEntry.SetRange("API Type", CurrentAPIType);
        DetailedSyncLogEntry.SetRange("Sync Log Entry No", SyncLogEntry."Entry No");
        DetailedSyncLogEntry.SetRange("Sync Status", DetailedSyncLogEntry."Sync Status"::"Waiting for Response");
        if DetailedSyncLogEntry.FindSet() then
            repeat
                CreateBodyContent.PullResponse(DetailedSyncLogEntry."Message ID", BodyContent);
            Until DetailedSyncLogEntry.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Response Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        commit();
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        if (CurrentAPIType = CurrentAPIType::Product) or (CurrentAPIType = CurrentAPIType::Inventory) then
            If (Database::"Item Variant" = DetailedSyncLogEntry."Table ID") and (ItemVariant.get(DetailedSyncLogEntry."Source Record ID")) then
                CalledByItemVariant := true;

        if CurrentAPIType = CurrentAPIType::DiscountPrice then
            If (Database::"Item Discount Group" = DetailedSyncLogEntry."Table ID") and (ItemDiscGrp.get(DetailedSyncLogEntry."Source Record ID")) then
                CalledByItemDiscountUpdate := true;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Acknowledgement", LogStatus::"In Progress", CurrentAPIType);
    end;

    procedure ProcessPullResponseAcknowledgment(CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice)
    var
        ItemVariant: Record "Item Variant";
        ItemDiscGrp: Record "Item Discount Group";
        BodyContent: Text;
    begin
        Clear(ResultDataBlank);
        clear(CalledByItemDiscountUpdate);
        Clear(CalledByItemVariant);

        i95Setup.get();
        Checki95SetupData();

        i95WebserviceExecuteCU.GetAPIUrl(CurrentAPIType, SchedulerType::PullResponseAck);
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, CurrentAPIType, SchedulerType::PullResponseAck, SyncSource::"Business Central");

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        SetPullResponseAcknowldgmentDefaults(CurrentAPIType);

        CreateBodyContent.AddContextHeader(BodyContent, 'PullResponseAck');
        DetailedSyncLogEntry.Reset();
        DetailedSyncLogEntry.SetCurrentKey("API Type", "Sync Log Entry No", "Sync Status");
        DetailedSyncLogEntry.SetRange("API Type", CurrentAPIType);
        DetailedSyncLogEntry.SetRange("Sync Log Entry No", SyncLogEntry."Entry No");
        DetailedSyncLogEntry.SetRange("Sync Status", DetailedSyncLogEntry."Sync Status"::"Waiting for Acknowledgement");
        if DetailedSyncLogEntry.FindSet() then
            repeat
                CreateBodyContent.PullResponseAcknowledge(DetailedSyncLogEntry, BodyContent);
            Until DetailedSyncLogEntry.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Acknowledgement Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        commit();
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        if (CurrentAPIType = CurrentAPIType::Product) or (CurrentAPIType = CurrentAPIType::Inventory) then
            If (Database::"Item Variant" = DetailedSyncLogEntry."Table ID") and (ItemVariant.get(DetailedSyncLogEntry."Source Record ID")) then
                CalledByItemVariant := true;

        if CurrentAPIType = CurrentAPIType::DiscountPrice then
            If (Database::"Item Discount Group" = DetailedSyncLogEntry."Table ID") and (ItemDiscGrp.get(DetailedSyncLogEntry."Source Record ID")) then
                CalledByItemDiscountUpdate := true;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Sync Complete", LogStatus::Completed, CurrentAPIType);
    end;


    procedure SetPullResponseDefaults(CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice)
    var
        APILogEntryL: Record "i95 API Call Log Entry";
        SchedulerID: Integer;
        SyncCounterL: Integer;
    begin
        i95Setup.Get();
        if i95Setup."Schedular ID" = '' then
            SchedulerID := 0
        else
            Evaluate(SchedulerID, i95Setup."Schedular ID");
        SyncCounterL := 1;
        APILogEntryL.Reset();
        APILogEntryL.SetRange("Sync Log Entry No", SyncLogEntryNo);
        APILogEntryL.SetRange("Scheduler Type", APILogEntryL."Scheduler Type"::PullResponse);
        if APILogEntryL.FindFirst() then
            SyncCounterL := APILogEntryL.Count();

        case CurrentAPIType of
            CurrentAPIType::Product:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 1, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::Inventory:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 2, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::TierPrices:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 5, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::SalesOrder:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 7, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::Customer:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 6, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::CustomerGroup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 4, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::Shipment:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 8, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::Invoice:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 9, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::CancelOrder:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 24, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::TaxBusPostingGroup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 40, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::TaxProductPostingGroup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 41, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::TaxPostingSetup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 42, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::CustomerDiscountGroup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 16, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::ItemDiscountGroup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 17, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::DiscountPrice:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 0, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::ConfigurableProduct:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 0, 2, SyncCounterL, SchedulerID);
            CurrentAPIType::EditOrder:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 0, 2, SyncCounterL, SchedulerID);
        end;
    end;

    procedure SetPullResponseAcknowldgmentDefaults(CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice)
    var
        APILogEntryL: Record "i95 API Call Log Entry";
        SchedulerID: Integer;
        SyncCounterL: Integer;
    begin
        i95Setup.Get();
        if i95Setup."Schedular ID" = '' then
            SchedulerID := 0
        else
            Evaluate(SchedulerID, i95Setup."Schedular ID");
        SyncCounterL := 1;
        APILogEntryL.Reset();
        APILogEntryL.SetRange("Sync Log Entry No", SyncLogEntryNo);
        APILogEntryL.SetRange("Scheduler Type", APILogEntryL."Scheduler Type"::PullResponseACK);
        if APILogEntryL.FindFirst() then
            SyncCounterL := APILogEntryL.Count();

        case CurrentAPIType of
            CurrentAPIType::Product:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 1, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::Inventory:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 2, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::TierPrices:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 5, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::SalesOrder:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 7, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::Customer:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 6, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::CustomerGroup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 4, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::Shipment:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 8, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::Invoice:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 9, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::CancelOrder:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 24, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::TaxBusPostingGroup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 40, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::TaxProductPostingGroup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 41, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::TaxPostingSetup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 42, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::CustomerDiscountGroup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 16, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::ItemDiscountGroup:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 17, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::DiscountPrice:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 0, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::ConfigurableProduct:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 0, 6, SyncCounterL, SchedulerID);
            CurrentAPIType::EditOrder:
                CreateBodyContent.SetDefaultValues(SyncLogEntryNo, 0, 6, SyncCounterL, SchedulerID);
        end;
    end;

    Procedure ExecuteAPIAndUpdateLogs(CurrentSyncStatus: Integer;
                                    CurrentLogStatus: Option " ",New,"In Progress",Completed,Error;
                                    CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID)
    var
        SourceRecordID: RecordId;
        ErrorFound: Boolean;
        ErrorText: Text[300];
        SyncLogStatus: Integer;
    begin
        if i95WebserviceExecuteCU.run() then begin
            Clear(ErrorFound);
            Clear(ErrorText);
            HttpReasonCode := i95WebserviceExecuteCU.GetResponseStatusCode();
            SyncLogEntry."Error Message" := Copystr(Text70328075Txt, 1, 300);
            SyncLogStatus := SyncLogEntry."Sync Status";

            If SyncLogEntry."Log Status" <> SyncLogEntry."Log Status"::Error then
                SyncLogEntry.UpdateSyncLogEntry(SyncLogEntry."Sync Status", LogStatus::"In Progress", HttpReasonCode, i95WebserviceExecuteCU.GetAPIResponseResultText(),
                                   i95WebserviceExecuteCU.GetAPIResponseMessageText(), SyncLogEntry."Message ID", SyncLogEntry."i95 Source ID",
                                   SyncLogEntry."Status ID"::"Request Inprocess", SyncSource::"Business Central");
            APILogEntry.UpdateAPILogEntry(HttpReasonCode, copystr(Text70328075Txt, 1, 300));

            TargetId := i95WebserviceExecuteCU.GetTargetId();
            MessageID := i95WebserviceExecuteCU.GetResultMessageID();
            SourceID := i95WebserviceExecuteCU.GetSourceID();
            StatusID := i95WebserviceExecuteCU.GetStatusID();
            ResponseResultText := i95WebserviceExecuteCU.GetAPIResponseResultText();
            ResponseMessageText := i95WebserviceExecuteCU.GetAPIResponseMessageText();

            case SyncLogEntry."Sync Status" of
                SyncLogEntry."Sync Status"::"Waiting for Sync":
                    SyncLogEntry.WriteToBlobField(i95WebserviceExecuteCU.GetWebServiceResponseJson(), CalledByAPI::"i95 Sync Result");
                SyncLogEntry."Sync Status"::"Waiting for Response":
                    SyncLogEntry.WriteToBlobField(i95WebserviceExecuteCU.GetWebServiceResponseJson(), CalledByAPI::"i95 Response Result");
                SyncLogEntry."Sync Status"::"Waiting for Acknowledgement":
                    SyncLogEntry.WriteToBlobField(i95WebserviceExecuteCU.GetWebServiceResponseJson(), CalledByAPI::"i95 Acknowledgement Result");
            end;

            APILogEntry.WriteToBlobField(i95WebserviceExecuteCU.GetWebServiceResponseJson(), BlobToUpdate::"i95 API Result");

            If (SourceId <> '') or (StatusId <> 0) or (MessageId <> 0) or (TargetId <> '') then begin
                If (ResponseResultText <> 'false') then begin
                    If SyncLogEntry."Log Status" <> SyncLogEntry."Log Status"::Error then begin
                        SyncLogEntry."Error Message" := '';
                        SyncLogEntry.UpdateSyncLogEntry(CurrentSyncStatus, CurrentLogStatus, HttpReasonCode,
                                                        ResponseResultText, ResponseMessageText,
                                                        MessageID, SourceID, StatusID, SyncSource::"Business Central");
                    end;
                    ResultDataJsonArray := i95WebserviceExecuteCU.GetResultDataJsonArray();

                    foreach ResultDataJsonToken in ResultDataJsonArray do begin
                        ResultDataJsonObject := ResultDataJsonToken.AsObject();
                        If (CurrentAPIType <> CurrentAPIType::CancelOrder) then
                            GetSourceRecordID(SourceRecordID, i95WebserviceExecuteCU.ProcessJsonTokenasCode('TargetId', ResultDataJsonObject), CurrentAPIType)
                        else
                            SourceRecordID := SyncLogEntry."Source Record ID";

                        If (i95WebserviceExecuteCU.ProcessJsonTokenasText('Result', ResultDataJsonObject) = 'false') then begin
                            DetailedSyncLogEntry.Reset();
                            DetailedSyncLogEntry.SetCurrentKey("Sync Log Entry No");
                            DetailedSyncLogEntry.SetRange("Sync Log Entry No", SyncLogEntryNo);
                            DetailedSyncLogEntry.SetRange("Source Record ID", SourceRecordID);
                            if DetailedSyncLogEntry.FindSet() then begin
                                DetailedSyncLogEntry.UpdateSyncLogEntry(DetailedSyncLogEntry."Sync Status", DetailedSyncLogEntry."Log Status"::Error, HttpReasonCode,
                                i95WebserviceExecuteCU.ProcessJsonTokenasText('Result', ResultDataJsonObject), i95WebserviceExecuteCU.ProcessJsonTokenasText('Message', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsonTokenasCode('SourceId', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsontokenasInteger('MessageId', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsonTokenasText('Message', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsonTokenasInteger('StatusId', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsonTokenasCode('TargetId', ResultDataJsonObject), SyncSource::"Business Central");
                                DetailedSyncLogEntry.UpdateErrorMessage(copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('Message', ResultDataJsonObject), 1, 300));
                                ErrorFound := true;
                                ErrorText := copystr(i95WebserviceExecuteCU.ProcessJsonTokenasText('Message', ResultDataJsonObject), 1, 300);
                            end;
                        end else begin
                            DetailedSyncLogEntry.Reset();
                            DetailedSyncLogEntry.SetCurrentKey("Sync Log Entry No");
                            DetailedSyncLogEntry.SetRange("Sync Log Entry No", SyncLogEntryNo);
                            DetailedSyncLogEntry.SetRange("Source Record ID", SourceRecordID);
                            if DetailedSyncLogEntry.FindSet() then begin
                                DetailedSyncLogEntry.UpdateSyncLogEntry(CurrentSyncStatus, CurrentLogStatus, HttpReasonCode,
                                i95WebserviceExecuteCU.ProcessJsonTokenasText('Result', ResultDataJsonObject), i95WebserviceExecuteCU.ProcessJsonTokenasText('Message', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsonTokenasCode('SourceId', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsontokenasInteger('MessageId', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsonTokenasText('Message', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsonTokenasInteger('StatusId', ResultDataJsonObject),
                                i95WebserviceExecuteCU.ProcessJsonTokenasCode('TargetId', ResultDataJsonObject), SyncSource::"Business Central");

                                If CurrentAPIType <> CurrentAPIType::CancelOrder then
                                    UpdateSourceRecord(SourceRecordID, i95WebserviceExecuteCU.ProcessJsonTokenasCode('SourceId', ResultDataJsonObject), CurrentAPIType, CurrentSyncStatus);
                            end;
                        end;
                        APILogEntry.UpdateAPILogEntry(HttpReasonCode, '');
                    end;
                    If ErrorFound then begin
                        SyncLogEntry."Sync Status" := SyncLogStatus;
                        SyncLogEntry."Log Status" := SyncLogEntry."Log Status"::Error;
                        SyncLogEntry."Error Message" := ErrorText;
                        SyncLogEntry.modify();
                    end else begin
                        SyncLogEntry.CalcFields("No. Of Errors");
                        if SyncLogEntry."No. Of Errors" = 0 then begin
                            SyncLogEntry."Sync Status" := CurrentSyncStatus;
                            SyncLogEntry."Log Status" := CurrentLogStatus;
                            SyncLogEntry."Error Message" := '';
                            SyncLogEntry.modify();
                        end;
                    end;
                end else
                    If (ResponseResultText = 'false') then begin
                        SyncLogEntry."Sync Status" := SyncLogEntry."Sync Status";
                        SyncLogEntry."Log Status" := SyncLogEntry."Log Status"::Error;
                        SyncLogEntry."Error Message" := copystr(ResponseMessageText, 1, 300);
                        SyncLogEntry.modify();

                        APILogEntry.UpdateAPILogEntry(HttpReasonCode, i95WebserviceExecuteCU.GetAPIResponseMessageText());
                    end;
            end else
                If (ResponseResultText = 'false') then begin
                    SyncLogEntry."Sync Status" := SyncLogEntry."Sync Status"::"No Response";
                    SyncLogEntry."Log Status" := SyncLogEntry."Log Status"::Cancelled;
                    SyncLogEntry."Error Message" := '';
                    SyncLogEntry.modify();

                    APILogEntry.UpdateAPILogEntry(HttpReasonCode, i95WebserviceExecuteCU.GetAPIResponseMessageText());
                end;
        end else begin
            HttpReasonCode := i95WebserviceExecuteCU.GetResponseStatusCode();
            SyncLogEntry.UpdateSyncLogEntry(SyncLogEntry."Sync Status", LogStatus::Error, HttpReasonCode, i95WebserviceExecuteCU.GetAPIResponseResultText(), i95WebserviceExecuteCU.GetAPIResponseMessageText(), MessageID, SourceID, SyncLogEntry."Status ID"::Error, SyncSource::"Business Central");
            SyncLogEntry."Error Message" := copystr(GetLastErrorText(), 1, 300);
            SyncLogEntry.Modify();

            APILogEntry.UpdateAPILogEntry(HttpReasonCode, copystr(GetLastErrorText(), 1, 300));
        end;
    end;

    procedure GetSourceRecordID(Var SourceRecordID: RecordId; CurrentTargetID: Code[20]; CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID)
    var
        Item: Record Item;
        ItemVariant: Record "Item Variant";
        SalesHeader: Record "Sales Header";
        Customer: Record Customer;
        CustomerGroup: Record "Customer Price Group";
        SalesShipment: Record "Sales Shipment Header";
        SalesInvoice: Record "Sales Invoice Header";
        TaxBusPostingGrp: Record "VAT Business Posting Group";
        TaxProdPostingGrp: Record "VAT Product Posting Group";
        TaxPostingSetup: Record "VAT Posting Setup";
        CustDiscountGroup: Record "Customer Discount Group";
        ItemDiscountGroup: Record "Item Discount Group";
        ItemCode: Code[20];
        VariantCode: Code[10];
    begin
        case CurrentAPIType of
            CurrentAPIType::Product:
                if CalledByItemVariant then begin
                    i95Setup.get();
                    ItemCode := copystr(CopyStr(CurrentTargetID, 1, StrPos(CurrentTargetID, i95Setup."i95 Item Variant Seperator") - 1), 1, 20);
                    VariantCode := copystr(CopyStr(CurrentTargetID, StrPos(CurrentTargetID, i95Setup."i95 Item Variant Seperator") + 1), 1, 10);
                    ItemVariant.get(ItemCode, VariantCode);
                    SourceRecordID := ItemVariant.RecordId();
                end else begin
                    item.Get(CurrentTargetID);
                    SourceRecordID := Item.RecordId();
                end;
            CurrentAPIType::Inventory:
                if CalledByItemVariant then begin
                    i95Setup.get();
                    ItemCode := copystr(CopyStr(CurrentTargetID, 1, StrPos(CurrentTargetID, i95Setup."i95 Item Variant Seperator") - 1), 1, 20);
                    VariantCode := copystr(CopyStr(CurrentTargetID, StrPos(CurrentTargetID, i95Setup."i95 Item Variant Seperator") + 1), 1, 10);
                    ItemVariant.get(ItemCode, VariantCode);
                    SourceRecordID := ItemVariant.RecordId();
                end else begin
                    item.Get(CurrentTargetID);
                    SourceRecordID := Item.RecordId();
                end;

            CurrentAPIType::TierPrices,
            CurrentaPIType::ConfigurableProduct:
                begin
                    item.Get(CurrentTargetID);
                    SourceRecordID := Item.RecordId();
                end;
            CurrentAPIType::SalesOrder,
            currentAPIType::EditOrder:
                begin
                    SalesHeader.get(SalesHeader."Document Type"::Order, CurrentTargetID);
                    SourceRecordID := SalesHeader.RecordId();
                end;
            CurrentAPIType::Customer:
                begin
                    Customer.get(CurrentTargetID);
                    SourceRecordID := Customer.RecordId();
                end;
            CurrentAPIType::CustomerGroup:
                begin
                    CustomerGroup.Get(CurrentTargetID);
                    SourceRecordID := CustomerGroup.RecordId();
                end;
            CurrentAPIType::Shipment:
                begin
                    SalesShipment.Get(CurrentTargetID);
                    SourceRecordID := SalesShipment.RecordId();
                end;
            CurrentAPIType::Invoice:
                begin
                    SalesInvoice.get(CurrentTargetID);
                    SourceRecordID := SalesInvoice.RecordId();
                end;
            CurrentAPIType::TaxBusPostingGroup:
                begin
                    TaxBusPostingGrp.get(CurrentTargetID);
                    SourceRecordID := TaxBusPostingGrp.RecordId();
                end;
            CurrentAPIType::TaxProductPostingGroup:
                begin
                    TaxProdPostingGrp.get(CurrentTargetID);
                    SourceRecordID := TaxProdPostingGrp.RecordId();
                end;
            CurrentAPIType::TaxPostingSetup:
                begin
                    TaxPostingSetup.Get(CurrentTargetID, i95WebserviceExecuteCU.ProcessJsonTokenasCode('Reference', ResultDataJsonObject));
                    SourceRecordID := TaxPostingSetup.RecordId();
                end;
            CurrentAPIType::CustomerDiscountGroup:
                begin
                    CustDiscountGroup.get(CurrentTargetID);
                    SourceRecordID := CustDiscountGroup.RecordId();
                end;
            CurrentAPIType::ItemDiscountGroup:
                begin
                    ItemDiscountGroup.Get(CurrentTargetID);
                    SourceRecordID := ItemDiscountGroup.RecordId();
                end;
            CurrentAPIType::DiscountPrice:
                if (not CalledByItemDiscountUpdate) and (item.Get(CurrentTargetID)) then
                    SourceRecordID := Item.RecordId()
                else
                    if CalledByItemDiscountUpdate and (ItemDiscountGroup.Get(CurrentTargetID)) then
                        SourceRecordID := ItemDiscountGroup.RecordId();
        end;
    end;

    procedure UpdateSourceRecord(SourceRecordID: RecordId; i95SourceCode: Code[20]; CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID;
                                  CurrentSyncStatus: Integer)
    var
        Item: Record Item;
        SalesHeader: Record "Sales Header";
        Customer: Record Customer;
        CustomerPriceGroup: Record "Customer Price Group";
        ShipToAddress: Record "Ship-to Address";
        SalesShipmentHeader: Record "Sales Shipment Header";
        SalesInvoiceHeader: Record "Sales Invoice Header";
        TaxBusPostingGrp: Record "VAT Business Posting Group";
        TaxProdPostingGrp: Record "VAT Product Posting Group";
        TaxPostingSetup: Record "VAT Posting Setup";
        CustDiscountGroup: Record "Customer Discount Group";
        ItemDiscountGroup: Record "Item Discount Group";
        ItemVariant: Record "Item Variant";
    begin
        case CurrentAPIType of
            CurrentAPIType::Product:
                if CalledByItemVariant then begin
                    ItemVariant.Get(SourceRecordID);
                    ItemVariant.UpdateReferenceId(i95SourceCode);

                    Item.get(ItemVariant."Item No.");
                    item.Updatei95ChildItemVariantSyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1);
                end else begin
                    item.Get(SourceRecordID);
                    Item.Seti95APIUpdateCall(true);
                    Item.Updatei95SyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::Inventory:
                if CalledByItemVariant then begin
                    ItemVariant.Get(SourceRecordID);
                    ItemVariant.Updatei95InventorySyncStatus(CurrentSyncStatus + 1, i95SourceCode);
                end else begin
                    item.Get(SourceRecordID);
                    Item.Seti95APIUpdateCall(true);
                    Item.Updatei95InventorySyncStatus(CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::Customer:
                begin
                    Customer.Get(SourceRecordID);
                    Customer.Seti95APIUpdateCall(true);
                    Customer.Updatei95SyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1, i95SourceCode);

                    ShipToAddress.Updatei95SyncStatus(SyncSource::"Business Central", Customer."No.");
                end;
            CurrentAPIType::CustomerGroup:
                begin
                    CustomerPriceGroup.Get(SourceRecordID);
                    CustomerPriceGroup.Seti95APIUpdateCall(true);
                    CustomerPriceGroup.Updatei95SyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::TierPrices:
                begin
                    item.Get(SourceRecordID);
                    Item.Seti95APIUpdateCall(true);
                    Item.Updatei95SalesPriceSyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1);
                end;
            CurrentAPIType::SalesOrder:
                begin
                    SalesHeader.Get(SourceRecordID);
                    SalesHeader.Seti95PullRequestAPICall(true);
                    SalesHeader.Updatei95SyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::Shipment:
                begin
                    SalesShipmentHeader.Get(SourceRecordID);
                    SalesShipmentHeader.Updatei95SyncStatus(CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::Invoice:
                begin
                    SalesInvoiceHeader.get(SourceRecordID);
                    SalesInvoiceHeader.Updatei95SyncStatus(CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::TaxBusPostingGroup:
                begin
                    TaxBusPostingGrp.get(SourceRecordID);
                    TaxBusPostingGrp.Updatei95SyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::TaxProductPostingGroup:
                begin
                    TaxProdPostingGrp.get(SourceRecordID);
                    TaxProdPostingGrp.Updatei95SyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::TaxPostingSetup:
                begin
                    TaxPostingSetup.get(SourceRecordID);
                    TaxPostingSetup.Updatei95SyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::CustomerDiscountGroup:
                begin
                    CustDiscountGroup.get(SourceRecordID);
                    CustDiscountGroup.Updatei95SyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::ItemDiscountGroup:
                begin
                    ItemDiscountGroup.Get(SourceRecordID);
                    ItemDiscountGroup.Updatei95SyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::DiscountPrice:
                If not CalledByItemDiscountUpdate then begin
                    item.Get(SourceRecordID);
                    Item.Seti95APIUpdateCall(true);
                    Item.Updatei95DiscountPriceSyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1);
                end else begin
                    ItemDiscountGroup.Get(SourceRecordID);
                    ItemDiscountGroup.Updatei95DiscountPriceSyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1);
                end;
            CurrentAPIType::EditOrder:
                begin
                    SalesHeader.Get(SourceRecordID);
                    SalesHeader.Seti95PullRequestAPICall(true);
                    SalesHeader.Updatei95SyncStatusforEditOrder(SyncSource::"Business Central", CurrentSyncStatus + 1, i95SourceCode);
                end;
            CurrentAPIType::ConfigurableProduct:
                begin
                    item.Get(SourceRecordID);
                    Item.Seti95APIUpdateCall(true);
                    Item.Updatei95ItemVariantSyncStatus(SyncSource::"Business Central", CurrentSyncStatus + 1, i95SourceCode);
                end;
        end;
    end;

    procedure SetSynclogEntryNo(EntryNo: Integer);
    begin
        SyncLogEntryNo := EntryNo;
    end;

    procedure Checki95SetupData()
    begin
        i95Setup.TestField("Base Url");
        i95Setup.TestField("Client ID");
        i95Setup.testfield("Subscription Key");
        i95Setup.TestField("Endpoint Code");
        i95Setup.TestField(Authorization);
    end;

    procedure SetTestMode(TestModeFlag: Boolean)
    begin
        TestMode := TestModeFlag;
    end;

    procedure SetMockResponseText(MockResponseText: Text)
    begin
        TestModeResponseTxt := MockResponseText;
    end;

    procedure IsNotSyncedSalesOrder(SalesHeader: Record "Sales Header"): Boolean
    var
        DetSyncLogEntry: Record "i95 Detailed Sync Log Entry";
    begin
        DetSyncLogEntry.Reset();
        DetSyncLogEntry.SetCurrentKey(DetSyncLogEntry."Source Record ID");
        DetSyncLogEntry.SetRange(DetSyncLogEntry."Source Record ID", SalesHeader.RecordId());
        exit(DetSyncLogEntry.IsEmpty());
    end;

    procedure CreateSyncLogforCancelSalesOrder(SalesHeader: Record "Sales Header")
    var
        i95SyncLogEntry: Record "i95 Sync Log Entry";
        SyncLogEntNo: Integer;
    begin
        i95SyncLogEntry.Reset();
        If i95SyncLogEntry.FindLast() then
            SyncLogEntNo := i95SyncLogEntry."Entry No"
        else
            SyncLogEntNo := 0;

        i95SyncLogEntry.init();
        i95SyncLogEntry."Entry No" := SyncLogEntNo + 1;
        i95SyncLogEntry."Sync DateTime" := CurrentDateTime();
        i95SyncLogEntry."API Type" := i95SyncLogEntry."API Type"::CancelOrder;
        i95SyncLogEntry."Sync Status" := i95SyncLogEntry."Sync Status"::"Waiting for Sync";
        i95SyncLogEntry."Log Status" := i95SyncLogEntry."Log Status"::New;
        i95SyncLogEntry."Sync Source" := i95SyncLogEntry."Sync Source"::"Business Central";
        i95SyncLogEntry."Source Record ID" := SalesHeader.RecordId();
        i95SyncLogEntry.Insert();

        DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::CancelOrder, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", i95SyncLogEntry."Entry No", SalesHeader."No.", SalesHeader."Sell-to Customer No.", format(SalesHeader."i95 Last Modification DateTime"), SalesHeader.RecordId(), Database::"Sales Header");
    end;

    procedure VariantInventoryPushData(var ItemVariant: Record "Item Variant")
    var
        Item: Record item;
        BodyContent: text;
        InventoryString: Text[30];
    begin
        clear(i95WebserviceExecuteCU);
        clear(SyncLogEntryNo);
        Clear(ResultDataBlank);
        Clear(InventoryString);
        Clear(CalledByItemVariant);

        i95Setup.get();
        Checki95SetupData();

        SyncLogEntryNo := SyncLogEntry.InsertSyncLogEntry(APIType::Inventory, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central");
        APILogEntryNo := APILogEntry.InsertApiCallLogEntry(SyncLogEntryNo, APIType::Inventory, SchedulerType::PushData, SyncSource::"Business Central");

        CreateBodyContent.AddContextHeader(BodyContent, 'PushData');
        repeat
            Item.Get(ItemVariant."Item No.");
            if i95Setup."i95 Default Warehouse" <> '' then
                Item.SetFilter(item."Location Filter", '%1', i95Setup."i95 Default Warehouse");
            Item.SetFilter(Item."Variant Filter", '%1', ItemVariant.Code);

            Item.CalcFields(Item.Inventory);

            InventoryString := format(Item.Inventory);
            InventoryString := DelChr(InventoryString, '=', ',');

            CreateBodyContent.VariantInventoryPushData(ItemVariant, InventoryString, BodyContent);
            DetailedSyncLogEntry.InsertDetailSyncLogEntry(APIType::Inventory, SyncStatus::"Waiting for Sync", LogStatus::New, SyncSource::"Business Central", SyncLogEntryNo, ItemVariant.Code, ItemVariant."Item No.", '', ItemVariant.RecordId(), Database::"Item Variant");
        until ItemVariant.Next() = 0;
        CreateBodyContent.AddContextFooter(BodyContent);

        SyncLogEntry.reset();
        if SyncLogEntry.get(SyncLogEntryNo) then;

        APILogEntry.reset();
        if APILogEntry.get(APILogEntryNo) then;

        SyncLogEntry.WriteToBlobField(BodyContent, CalledByAPI::"i95 Sync Request");
        APILogEntry.WriteToBlobField(BodyContent, BlobToUpdate::"i95 API Request");
        SyncLogEntry.SetSourceRecordID(ItemVariant.RecordId());
        commit();

        i95WebserviceExecuteCU.GetAPIUrl(APIType::Inventory, SchedulerType::PushData);
        i95WebserviceExecuteCU.SetWebRequestData(BodyContent);

        if TestMode then begin
            i95WebserviceExecuteCU.SetTestMode(true);
            i95WebserviceExecuteCU.SetMockResponseText(TestModeResponseTxt);
        end;

        CalledByItemVariant := true;

        ExecuteAPIAndUpdateLogs(SyncStatus::"Waiting for Response", LogStatus::"In Progress", APIType::Inventory);
    end;

    var
        i95Setup: Record "i95 Setup";
        SyncLogEntry: Record "i95 Sync Log Entry";
        APILogEntry: Record "i95 API Call Log Entry";
        DetailedSyncLogEntry: Record "i95 Detailed Sync Log Entry";
        i95WebserviceExecuteCU: Codeunit "i95 Webservice Execute";
        CreateBodyContent: Codeunit "i95 Create Body Content";
        MessageID: Integer;
        SourceID: Code[20];
        StatusID: Integer;
        SyncLogEntryNo: Integer;
        TestModeResponseTxt: Text;
        HttpReasonCode: Text;
        APILogEntryNo: Integer;
        ResultDataJsonArray: JsonArray;
        ResultDataJsonObject: JsonObject;
        ResultDataJsonToken: JsonToken;
        APIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID;
        SyncStatus: Option "Waiting for Sync","Waiting for Response","Waiting for Acknowledgement","Sync Complete","No Response";
        LogStatus: Option " ",New,"In Progress",Completed,Error,Cancelled;
        CalledByAPI: Option "i95 Sync Request","i95 Sync Result","i95 Response Request","i95 Response Result","i95 Acknowledgement Request","i95 Acknowledgement Result";
        BlobToUpdate: Option "i95 API Request","i95 API Result";
        SyncSource: Option "","Business Central",i95;
        SchedulerType: Option " ",PushData,PullResponse,PullResponseAck,PullData,PushResponse;
        TargetId: code[20];
        ResultDataBlank: Boolean;
        TestMode: Boolean;
        CalledByItemDiscountUpdate: Boolean;
        CalledByItemVariant: Boolean;
        Text70328075Txt: Label 'Result Data Blank.';
        ResponseResultText: Text;
        ResponseMessageText: Text;
}