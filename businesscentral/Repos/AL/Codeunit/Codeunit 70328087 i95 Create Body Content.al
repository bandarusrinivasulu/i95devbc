codeunit 70328087 "i95 Create Body Content"
{
    Permissions = tabledata "Sales Shipment Header" = rm, tabledata "Sales Shipment Line" = rm, tabledata "Sales Invoice Header" = rm, tabledata "Sales Invoice Line" = rm;

    var
        BodyContentjsonObj: JsonObject;
        FirstRecord: Boolean;
        LocalMessageID: Integer;
        EntityID: Integer;
        SyncCounter: Integer;
        MessageStatus: Integer;
        SchedulerID: Integer;

    procedure AddContextHeader(var BodyContent: Text; SchedulerType: Text)
    var
        i95Setup: Record "i95 Setup";
        SchedulerIDL: Integer;
        ContextHdrjsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
    begin
        clear(BodyContentjsonObj);
        i95Setup.Get();
        FirstRecord := true;
        if i95Setup."Schedular ID" = '' then
            SchedulerIDL := 0
        else
            Evaluate(SchedulerIDL, i95Setup."Schedular ID");

        ContextHdrjsonObj.Add('clientId', i95Setup."Client ID");
        ContextHdrjsonObj.Add('subscriptionKey', i95Setup."Subscription Key");
        ContextHdrjsonObj.Add('instanceType', format(i95Setup."Instance Type"));
        ContextHdrjsonObj.Add('requestType', 'Target');
        If SchedulerType = 'PushResponse' then
            ContextHdrjsonObj.Add('schedulerType', 'PullData')
        else
            ContextHdrjsonObj.Add('schedulerType', SchedulerType);
        ContextHdrjsonObj.Add('schedulerId', SchedulerIDL);
        ContextHdrjsonObj.Add('IsNotEncrypted', true);
        ContextHdrjsonObj.Add('endpointCode', i95Setup."Endpoint Code");

        BodyContentjsonObj.Add('context', ContextHdrjsonObj);

        if UpperCase(SchedulerType) = 'PULLDATA' then begin
            BodyContentjsonObj.Add('packetSize', i95Setup."Pull Data Packet Size");
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);
        end;
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure AddContextFooter(var BodyContent: Text)
    begin

    end;

    procedure ProductPushData(Item: Record Item; var BodyContent: text)
    var
        i95Setup: Record "i95 Setup";
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
    begin
        i95Setup.get();

        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', Item.Description);

        InputDataJsonObj.Add('sku', Item."No.");
        InputDataJsonObj.Add('name', Item.Description);
        InputDataJsonObj.Add('description', Item.Description);
        InputDataJsonObj.Add('shortDescription', Item."Search Description");
        InputDataJsonObj.Add('cost', format(Item."Unit Cost"));
        InputDataJsonObj.Add('targetId', Item."No.");
        InputDataJsonObj.Add('reference', Item.Description);
        InputDataJsonObj.Add('weight', format(Item."Gross Weight"));
        InputDataJsonObj.Add('price', format(Item."Unit Price"));
        If Item."Base Unit of Measure" <> '' then
            InputDataJsonObj.Add('unitOfMeasure', Item."Base Unit of Measure")
        else
            InputDataJsonObj.Add('unitOfMeasure', i95Setup."Default UOM");
        InputDataJsonObj.Add('status', 1);
        InputDataJsonObj.Add('Taxable Goods', 0);
        InputDataJsonObj.Add('qty', 0);
        InputDataJsonObj.Add('taxProductPostingGroupCode', Item."VAT Prod. Posting Group");
        InputDataJsonObj.Add('itemDiscountGroupId', Item."Item Disc. Group");
        InputDataJsonObj.WriteTo(BodyContent);
        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', Item."No.");
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure InventoryPushData(Item: Record Item; InventoryString: Text; var BodyContent: text)
    var
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', Item.Description);

        InputDataJsonObj.Add('sku', format(Item."No."));
        InputDataJsonObj.Add('targetId', format(Item."No."));
        InputDataJsonObj.Add('reference', Item.Description);
        InputDataJsonObj.Add('qty', InventoryString);
        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', format(Item."No."));
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure VariantInventoryPushData(ItemVariant: Record "Item Variant"; InventoryString: Text; var BodyContent: text)
    var
        i95Setup: Record "i95 Setup";
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
    begin
        i95Setup.Get();
        i95Setup.TestField("i95 Item Variant Seperator");
        i95Setup.TestField("i95 Item Variant Pattern 1");
        i95Setup.TestField("i95 Item Variant Pattern 2");
        i95Setup.TestField("i95 Item Variant Pattern 3");

        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', ItemVariant.Description);

        InputDataJsonObj.Add('sku', ItemVariant."Item No." + i95Setup."i95 Item Variant Seperator" + ItemVariant.Code);
        InputDataJsonObj.Add('targetId', ItemVariant."Item No." + i95Setup."i95 Item Variant Seperator" + ItemVariant.Code);
        InputDataJsonObj.Add('reference', ItemVariant.Description);
        InputDataJsonObj.Add('qty', InventoryString);
        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', ItemVariant."Item No." + i95Setup."i95 Item Variant Seperator" + ItemVariant.Code);
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure ConfigurableProductPushData(Item: Record Item; var BodyContent: text)
    var
        ItemVariant: record "Item Variant";
        i95Setup: Record "i95 Setup";
        FirstItemVariantCode: code[50];
        AttributeValue: Text[30];
        ChildSku: Text[250];
        i: Integer;
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        configurableEntityJsonObj: JsonObject;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        i95Setup.get();

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', Item.Description);

        InputDataJsonObj.Add('sku', format(Item."No."));
        InputDataJsonObj.Add('name', Item.Description);
        InputDataJsonObj.Add('description', Item.Description);
        InputDataJsonObj.Add('shortDescription', Item.Description);
        InputDataJsonObj.Add('typeId', 'Configurable');

        Clear(FirstItemVariantCode);
        Clear(configurableEntityJsonObj);
        ItemVariant.Reset();
        ItemVariant.SetRange(ItemVariant."Item No.", item."No.");
        If ItemVariant.Findfirst() then
            FirstItemVariantCode := ItemVariant.Code;

        Clear(i);
        Clear(AttributeValue);
        if strpos(FirstItemVariantCode, i95Setup."i95 Item Variant Seperator") <> 0 then
            repeat
                AttributeValue := copystr(copystr(FirstItemVariantCode, 1, strpos(FirstItemVariantCode, i95Setup."i95 Item Variant Seperator") - 1), 1, 30);
                FirstItemVariantCode := copystr(CopyStr(FirstItemVariantCode, strpos(FirstItemVariantCode, i95Setup."i95 Item Variant Seperator") + 1, MaxStrLen(firstItemVariantCode)), 1, 50);
                i += 1;
            until strpos(FirstItemVariantCode, i95Setup."i95 Item Variant Seperator") = 0;

        //The Pattern should be seperated by comma --> Ex Colour,Style,Size.   It should not be Colour-style-size 
        case i of
            0:
                configurableEntityJsonObj.Add('attributes', convertstr(i95Setup."i95 Item Variant Pattern 1", i95Setup."i95 Item Variant Seperator", ','));
            1:
                configurableEntityJsonObj.Add('attributes', convertstr(i95Setup."i95 Item Variant Pattern 2", i95Setup."i95 Item Variant Seperator", ','));
            2:
                configurableEntityJsonObj.Add('attributes', convertstr(i95Setup."i95 Item Variant Pattern 3", i95Setup."i95 Item Variant Seperator", ','));
        end;

        ItemVariant.Reset();
        ItemVariant.SetRange(ItemVariant."Item No.", item."No.");
        If ItemVariant.FindSet() then
            repeat
                if ChildSku = '' then
                    ChildSku := ItemVariant."Item No." + i95Setup."i95 Item Variant Seperator" + ItemVariant.Code
                else
                    ChildSku += ',' + ItemVariant."Item No." + i95Setup."i95 Item Variant Seperator" + ItemVariant.Code;
            until ItemVariant.Next() = 0;

        configurableEntityJsonObj.Add('childSkus', ChildSku);
        InputDataJsonObj.Add('configurableEntity', configurableEntityJsonObj);
        InputDataJsonObj.Add('targetId', format(Item."No."));
        InputDataJsonObj.Add('reference', Item.Description);
        InputDataJsonObj.Add('weight', format(Item."Gross Weight"));
        InputDataJsonObj.Add('price', format(Item."Unit Price"));
        InputDataJsonObj.Add('unitOfMeasure', Item."Base Unit of Measure");
        InputDataJsonObj.WriteTo(BodyContent);
        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', format(Item."No."));
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure CustomerPushData(Customer: Record Customer; var BodyContent: Text)
    var
        ShipToAddress: Record "Ship-to Address";
        FirstAddress: Boolean;
        FirstName: Text[100];
        LastName: Text[100];
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        AddressesJsonArr: JsonArray;
        AddressesJsonObj: JsonObject;
        AddressesJsonToken: JsonToken;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        FirstAddress := true;

        If StrPos(Customer.Name, ' ') <> 0 then begin
            FirstName := CopyStr(Customer.Name, 1, StrPos(Customer.Name, ' '));
            LastName := copystr(Customer.Name, strpos(Customer.Name, ' ') + 1, StrLen(Customer.Name));
        end else begin
            FirstName := Customer.Name;
            LastName := Customer.Name;
        end;

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', Format(Customer."No."));

        InputDataJsonObj.Add('targetId', Customer."No.");
        InputDataJsonObj.Add('reference', Customer."No.");
        InputDataJsonObj.Add('firstName', FirstName);
        InputDataJsonObj.Add('lastName', LastName);
        InputDataJsonObj.Add('customerGroup', Customer."Customer Price Group");
        InputDataJsonObj.Add('taxBusPostingGroupCode', Customer."VAT Bus. Posting Group");
        InputDataJsonObj.Add('addresses', AddressesJsonArr);
        InputDataJsonObj.Get('addresses', AddressesJsonToken);
        AddressesJsonArr := AddressesJsonToken.AsArray();
        ShipToAddress.Reset();
        ShipToAddress.SetRange(ShipToAddress."Customer No.", Customer."No.");
        If ShipToAddress.FindSet() then
            repeat
                clear(AddressesJsonObj);
                if (ShipToAddress.Name <> '') and (ShipToAddress.Address <> '') then begin

                    If StrPos(ShipToAddress.Name, ' ') <> 0 then begin
                        FirstName := CopyStr(ShipToAddress.Name, 1, StrPos(ShipToAddress.Name, ' '));
                        LastName := copystr(ShipToAddress.Name, strpos(ShipToAddress.Name, ' ') + 1, StrLen(ShipToAddress.Name));
                    end else begin
                        FirstName := ShipToAddress.Name;
                        LastName := ShipToAddress.Name;
                    end;

                    AddressesJsonObj.Add('targetId', ShipToAddress.Code);
                    AddressesJsonObj.Add('reference', ShipToAddress."Customer No.");
                    AddressesJsonObj.Add('targetCustomerId', ShipToAddress."Customer No.");
                    if ShipToAddress.Code = 'I95DEFAULT' then
                        AddressesJsonObj.Add('isDefaultBilling', 'true')
                    else
                        AddressesJsonObj.Add('isDefaultBilling', 'false');
                    if not ShipToAddress."i95 Is Default Shipping" then
                        AddressesJsonObj.Add('isDefaultShipping', 'false')
                    else
                        AddressesJsonObj.Add('isDefaultShipping', 'true');
                    AddressesJsonObj.Add('firstName', FirstName);
                    AddressesJsonObj.Add('lastName', LastName);
                    AddressesJsonObj.Add('street', ShipToAddress.Address);
                    AddressesJsonObj.Add('street2', ShipToAddress."Address 2");
                    AddressesJsonObj.Add('city', ShipToAddress.City);
                    AddressesJsonObj.Add('postcode', ShipToAddress."Post Code");
                    AddressesJsonObj.Add('countryId', ShipToAddress."Country/Region Code");
                    AddressesJsonObj.Add('regionId', ShipToAddress.County);
                    AddressesJsonObj.Add('telephone', ShipToAddress."Phone No.");
                    AddressesJsonObj.Add('createdTime', format(ShipToAddress."i95 Created DateTime"));
                    AddressesJsonArr.Add(AddressesJsonObj);
                end;
            until ShipToAddress.Next() = 0;

        ShipToAddress.Get(Customer."No.", 'I95DEFAULT');

        InputDataJsonObj.Add('email', ShipToAddress."E-Mail");
        InputDataJsonObj.Add('customerDiscountGroupId', Customer."Customer Disc. Group");
        InputDataJsonObj.Add('createdDate', format(Customer."i95 Created DateTime"));
        InputDataJsonObj.Add('street', ShipToAddress.Address);
        InputDataJsonObj.Add('street2', ShipToAddress."Address 2");
        InputDataJsonObj.Add('city', ShipToAddress.City);
        InputDataJsonObj.Add('regionId', ShipToAddress.County);
        InputDataJsonObj.Add('countryId', ShipToAddress."Country/Region Code");
        InputDataJsonObj.Add('postcode', ShipToAddress."Post Code");
        InputDataJsonObj.Add('telephone', ShipToAddress."Phone No.");
        InputDataJsonObj.Add('isDefaultBilling', 'true');
        InputDataJsonObj.Add('targetAddressId', ShipToAddress.Code);
        if ShipToAddress."i95 Is Default Shipping" then
            InputDataJsonObj.Add('isDefaultShipping', 'true')
        else
            InputDataJsonObj.Add('isDefaultShipping', 'false');

        InputDataJsonObj.WriteTo(BodyContent);
        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', format(Customer."No."));
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure CustomerPriceGroupPushData(CustPriceGroup: Record "Customer Price Group"; var BodyContent: Text)
    var
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', Format(CustPriceGroup.Code));

        InputDataJsonObj.Add('targetId', CustPriceGroup.Code);
        InputDataJsonObj.Add('reference', CustPriceGroup.Code);

        InputDataJsonObj.WriteTo(BodyContent);
        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', format(CustPriceGroup.Code));
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure SalesPricePushData(Item: Record Item; var BodyContent: Text)
    var
        SalesPrice: Record "Sales Price";
        FirstSalesPrice: Boolean;
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        TierPricesJsonArr: JsonArray;
        TierPricessonObj: JsonObject;
        TierPricesJsonToken: JsonToken;
    begin

        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        FirstSalesPrice := true;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', Format(Item.Description));

        InputDataJsonObj.Add('targetId', Item."No.");
        InputDataJsonObj.Add('reference', Item.Description);
        InputDataJsonObj.Add('tierPrices', TierPricesJsonArr);
        InputDataJsonObj.Get('tierPrices', TierPricesJsonToken);
        TierPricesJsonArr := TierPricesJsonToken.AsArray();
        SalesPrice.Reset();
        SalesPrice.SetRange(SalesPrice."Item No.", Item."No.");
        SalesPrice.SetRange(SalesPrice."Sales Type", SalesPrice."Sales Type"::"Customer Price Group");
        If SalesPrice.FindSet() then
            repeat
                Clear(TierPricessonObj);
                TierPricessonObj.Add('priceLevelKey', SalesPrice."Sales Code");
                TierPricessonObj.Add('minQty', SalesPrice."Minimum Quantity");
                TierPricessonObj.Add('price', SalesPrice."Unit Price");
                TierPricesJsonArr.Add(TierPricessonObj);
            until SalesPrice.Next() = 0;

        InputDataJsonObj.WriteTo(BodyContent);
        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', format(Item."No."));
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure SalesOrderPushData(SalesHeader: record "Sales Header"; var BodyContent: Text; EcommerceShippingCode: code[50]; EcommercePaymentMethodCode: Code[50]; EcommerceShippingTitle: text[50]; ShippingAgentCode: text[30])
    var
        SalesLine: Record "Sales Line";
        i95Setup: Record "i95 Setup";
        ItemVariant: Record "Item Variant";
        SalesLine2: Record "Sales Line";
        FirstSalesLine: Boolean;
        DiscountAmount: Decimal;
        AttributeCode: text[30];
        AttributeValue: Text[30];
        PatternType: Integer;
        Pattern: text[50];
        VariantCode: text[50];
        i: Integer;
        VariantSepratorCount: Integer;
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        DiscountAmountJsonArr: JsonArray;
        DiscountAmountJsonObj: JsonObject;
        DiscountAmountJsonToken: JsonToken;
        PaymentJsonArr: JsonArray;
        PaymentJsonObj: JsonObject;
        PaymentJsonToken: JsonToken;
        OrderItemsJsonArr: JsonArray;
        OrderItemsJsonObj: JsonObject;
        OrderItemsJsonToken: JsonToken;
        OrderItemsdiscountAmountJsonArr: JsonArray;
        OrderItemsdiscountAmountJsonObj: JsonObject;
        OrderItemsdiscountAmountJsonToken: JsonToken;
        OrderItemVariantJsonArr: JsonArray;
        OrderItemVariantJsonObj: JsonObject;
        OrderItemVariantJsonToken: JsonToken;
        AttributewithKeyJsonArr: JsonArray;
        AttributewithKeyJsonObj: JsonObject;
        AttributewithKeyJsonToken: JsonToken;
        BillingAddressJsonObj: JsonObject;
        ShippingAddressJsonObj: JsonObject;
    begin
        i95Setup.get();
        i95Setup.TestField("i95 Item Variant Seperator");
        i95Setup.TestField("i95 Item Variant Pattern 1");
        i95Setup.TestField("i95 Item Variant Pattern 2");
        i95Setup.TestField("i95 Item Variant Pattern 3");

        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        FirstSalesLine := true;

        SalesHeader.CalcFields(Amount, "Amount Including VAT");

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', Format(SalesHeader."Sell-to Customer No."));

        InputDataJsonObj.Add('targetId', SalesHeader."No.");
        InputDataJsonObj.Add('reference', SalesHeader."Sell-to Customer No.");
        InputDataJsonObj.Add('targetCustomerId', SalesHeader."Sell-to Customer No.");
        InputDataJsonObj.Add('carrierName', lowercase(ShippingAgentCode));
        InputDataJsonObj.Add('targetShippingAddressId', SalesHeader."Sell-to Customer No.");
        InputDataJsonObj.Add('targetBillingAddressId', SalesHeader."Bill-to Customer No.");
        InputDataJsonObj.Add('orderCreatedDate', format(SalesHeader."Order Date"));
        InputDataJsonObj.Add('shippingMethod', lowercase(EcommerceShippingCode));
        InputDataJsonObj.Add('shippingTitle', lowercase(EcommerceShippingTitle));
        InputDataJsonObj.Add('payment', PaymentJsonArr);
        InputDataJsonObj.Get('payment', PaymentJsonToken);
        PaymentJsonArr := PaymentJsonToken.AsArray();
        clear(PaymentJsonObj);
        PaymentJsonObj.Add('paymentMethod', LowerCase(EcommercePaymentMethodCode));
        PaymentJsonArr.Add(PaymentJsonObj);

        SalesLine.Reset();
        SalesLine.SetRange(SalesLine."Document Type", SalesLine."Document Type"::Order);
        SalesLine.SetRange(SalesLine."Document No.", SalesHeader."No.");
        SalesLine.SetRange(SalesLine.Type, SalesLine.Type::"G/L Account");
        SalesLine.SetRange(SalesLine."No.", i95Setup."i95 Shipping Charge G/L Acc");
        If SalesLine.FindFirst() then
            InputDataJsonObj.Add('shippingAmount', format(SalesLine."Unit Price"))
        else
            InputDataJsonObj.Add('shippingAmount', 0);

        Clear(DiscountAmount);
        SalesLine.Reset();
        SalesLine.SetRange(SalesLine."Document Type", SalesLine."Document Type"::Order);
        SalesLine.SetRange(SalesLine."Document No.", SalesHeader."No.");
        If SalesLine.Findset() then
            repeat
                DiscountAmount += SalesLine."Line Discount Amount";
            until SalesLine.Next() = 0;

        InputDataJsonObj.Add('subTotal', delchr(format(SalesHeader.Amount), '=', ','));
        InputDataJsonObj.Add('orderDocumentAmount', delchr(format(SalesHeader."Amount Including VAT"), '=', ','));
        InputDataJsonObj.Add('taxAmount', delchr(format(SalesHeader."Amount Including VAT" - SalesHeader.Amount), '=', ','));
        InputDataJsonObj.Add('discount', DiscountAmountJsonArr);
        InputDataJsonObj.Get('discount', DiscountAmountJsonToken);
        DiscountAmountJsonArr := DiscountAmountJsonToken.AsArray();
        clear(DiscountAmountJsonObj);
        DiscountAmountJsonObj.Add('discountType', 'discount');
        DiscountAmountJsonObj.Add('discountAmount', format(DiscountAmount));
        DiscountAmountJsonArr.Add(DiscountAmountJsonObj);

        InputDataJsonObj.Add('orderItems', OrderItemsJsonArr);
        InputDataJsonObj.Get('orderItems', OrderItemsJsonToken);
        OrderItemsJsonArr := OrderItemsJsonToken.AsArray();

        SalesLine.Reset();
        SalesLine.SetRange(SalesLine."Document Type", SalesLine."Document Type"::Order);
        SalesLine.SetRange(SalesLine."Document No.", SalesHeader."No.");
        SalesLine.SetRange(SalesLine.Type, SalesLine.Type::Item);
        if SalesLine.FindSet() then
            repeat
                clear(OrderItemsJsonObj);

                if SalesLine."Variant Code" = '' then begin

                    OrderItemsJsonObj.Add('sku', SalesLine."No.");
                    OrderItemsJsonObj.Add('qty', delchr(format(SalesLine.Quantity), '=', ','));
                    OrderItemsJsonObj.Add('price', delchr(format(SalesLine."Unit Price"), '=', ','));

                    Clear(OrderItemsdiscountAmountJsonArr);
                    clear(OrderItemsdiscountAmountJsonObj);
                    OrderItemsJsonObj.Add('discount', OrderItemsdiscountAmountJsonArr);
                    OrderItemsJsonObj.Get('discount', OrderItemsdiscountAmountJsonToken);
                    OrderItemsdiscountAmountJsonArr := OrderItemsdiscountAmountJsonToken.AsArray();

                    SalesLine2.Reset();
                    SalesLine2.SetRange(SalesLine2."Document Type", SalesLine2."Document Type"::Order);
                    SalesLine2.SetRange(SalesLine2."Document No.", SalesHeader."No.");
                    SalesLine2.SetRange(SalesLine2."Line No.", SalesLine."Line No.");
                    If SalesLine2.FindFirst() then begin
                        OrderItemsdiscountAmountJsonObj.Add('discountType', 'discount');
                        OrderItemsdiscountAmountJsonObj.Add('discountAmount', format(SalesLine2."Line Discount Amount"));
                    end;
                    OrderItemsdiscountAmountJsonArr.Add(OrderItemsdiscountAmountJsonObj);

                    OrderItemsJsonObj.Add('lineNo', delchr(format(SalesLine."Line No."), '=', ','));
                    OrderItemsJsonArr.Add(OrderItemsJsonObj);
                end else begin
                    Clear(VariantCode);
                    Clear(VariantSepratorCount);
                    Clear(Pattern);
                    Clear(PatternType);
                    ItemVariant.get(SalesLine."No.", SalesLine."Variant Code");

                    OrderItemsJsonObj.Add('sku', ItemVariant."Item No." + i95Setup."i95 Item Variant Seperator" + ItemVariant.Code);
                    OrderItemsJsonObj.Add('qty', delchr(format(SalesLine.Quantity), '=', ','));
                    OrderItemsJsonObj.Add('price', delchr(format(SalesLine."Unit Price"), '=', ','));
                    OrderItemsJsonObj.Add('markdownPrice', 0);
                    OrderItemsJsonObj.Add('parentSku', ItemVariant."Item No.");
                    OrderItemsJsonObj.Add('retailVariantId', ItemVariant.Code);

                    //Item Variant
                    Clear(OrderItemVariantJsonObj);
                    clear(OrderItemVariantJsonArr);
                    Clear(AttributewithKeyJsonArr);
                    OrderItemsJsonObj.Add('itemVariants', OrderItemVariantJsonArr);
                    OrderItemsJsonObj.Get('itemVariants', OrderItemVariantJsonToken);
                    OrderItemVariantJsonArr := OrderItemVariantJsonToken.AsArray();
                    clear(OrderItemVariantJsonObj);

                    OrderItemVariantJsonObj.Add('attributeWithKey', AttributewithKeyJsonArr);
                    OrderItemVariantJsonObj.get('attributeWithKey', AttributewithKeyJsonToken);
                    AttributewithKeyJsonArr := AttributewithKeyJsonToken.AsArray();
                    clear(AttributewithKeyJsonObj);

                    VariantCode := ItemVariant.Code;
                    VariantSepratorCount := STRLEN(DELCHR(VariantCode, '=', DELCHR(VariantCode, '=', i95Setup."i95 Item Variant Seperator")));

                    case VariantSepratorCount of
                        0:
                            begin
                                Pattern := i95Setup."i95 Item Variant Pattern 1";
                                PatternType := 1;
                            end;
                        1:
                            begin
                                Pattern := i95Setup."i95 Item Variant Pattern 2";
                                PatternType := 2;
                            end;
                        2:
                            begin
                                Pattern := i95Setup."i95 Item Variant Pattern 3";
                                PatternType := 3;
                            end;
                    end;

                    ItemVariant.get(SalesLine."No.", SalesLine."Variant Code");
                    VariantCode := ItemVariant.Code;
                    for i := 1 to PatternType do begin
                        Clear(AttributewithKeyJsonObj);
                        Clear(AttributeCode);
                        Clear(AttributeValue);
                        if StrPos(Pattern, i95Setup."i95 Item Variant Seperator") <> 0 then begin
                            AttributeCode := copystr(CopyStr(Pattern, 1, StrPos(Pattern, i95Setup."i95 Item Variant Seperator") - 1), 1, 30);
                            AttributeValue := copystr(copystr(VariantCode, 1, strpos(VariantCode, i95Setup."i95 Item Variant Seperator") - 1), 1, 30);

                            Pattern := copystr(CopyStr(Pattern, StrPos(Pattern, i95Setup."i95 Item Variant Seperator") + 1), 1, 30);
                            VariantCode := copystr(CopyStr(VariantCode, strpos(VariantCode, i95Setup."i95 Item Variant Seperator") + 1), 1, 50);
                        end else begin
                            AttributeCode := CopyStr(Pattern, 1, 30);
                            AttributeValue := CopyStr(VariantCode, 1, 30);
                        end;

                        AttributewithKeyJsonObj.Add('attributeCode', AttributeCode);
                        AttributewithKeyJsonObj.Add('attributeValue', AttributeValue);
                        AttributewithKeyJsonObj.Add('attributeType', 'select');

                        AttributewithKeyJsonArr.Add(AttributewithKeyJsonObj);
                    end;

                    OrderItemVariantJsonArr.Add(OrderItemVariantJsonObj);

                    Clear(OrderItemsdiscountAmountJsonArr);
                    Clear(OrderItemsdiscountAmountJsonObj);

                    OrderItemsJsonObj.Add('discount', OrderItemsdiscountAmountJsonArr);
                    OrderItemsJsonObj.Get('discount', OrderItemsdiscountAmountJsonToken);
                    OrderItemsdiscountAmountJsonArr := OrderItemsdiscountAmountJsonToken.AsArray();

                    SalesLine2.Reset();
                    SalesLine2.SetRange(SalesLine2."Document Type", SalesLine2."Document Type"::Order);
                    SalesLine2.SetRange(SalesLine2."Document No.", SalesHeader."No.");
                    SalesLine2.SetRange(SalesLine2."Line No.", SalesLine."Line No.");
                    If SalesLine2.FindFirst() then begin
                        OrderItemsdiscountAmountJsonObj.Add('discountType', 'discount');
                        OrderItemsdiscountAmountJsonObj.Add('discountAmount', format(SalesLine2."Line Discount Amount"));
                    end;
                    OrderItemsdiscountAmountJsonArr.Add(OrderItemsdiscountAmountJsonObj);

                    OrderItemsJsonObj.Add('lineNo', delchr(format(SalesLine."Line No."), '=', ','));
                    OrderItemsJsonArr.Add(OrderItemsJsonObj);
                end;

            until SalesLine.Next() = 0;

        Clear(BillingAddressJsonObj);
        BillingAddressJsonObj.Add('targetAddressId', 'I95DEFAULT');
        BillingAddressJsonObj.Add('isDefaultBilling', true);
        if strpos(SalesHeader."Bill-to Name", ' ') <> 0 then begin
            BillingAddressJsonObj.Add('firstName', copystr(SalesHeader."Bill-to Name", 1, strpos(SalesHeader."Bill-to Name", ' ') - 1));
            BillingAddressJsonObj.Add('lastName', copystr(SalesHeader."Bill-to Name", strpos(SalesHeader."Bill-to Name", ' ') + 1));
        end else begin
            BillingAddressJsonObj.Add('firstName', SalesHeader."Bill-to Name");
            BillingAddressJsonObj.Add('lastName', SalesHeader."Bill-to Name");
        end;
        BillingAddressJsonObj.Add('street', SalesHeader."Bill-to Address");
        BillingAddressJsonObj.Add('street2', SalesHeader."Bill-to Address 2");
        BillingAddressJsonObj.Add('city', SalesHeader."Bill-to City");
        BillingAddressJsonObj.Add('postcode', SalesHeader."Bill-to Post Code");
        BillingAddressJsonObj.Add('countryId', SalesHeader."Bill-to Country/Region Code");
        BillingAddressJsonObj.Add('regionId', SalesHeader."Bill-to County");
        BillingAddressJsonObj.Add('telephone', SalesHeader."Sell-to Phone No.");
        InputDataJsonObj.Add('billingAddress', BillingAddressJsonObj);

        Clear(ShippingAddressJsonObj);
        If SalesHeader."Ship-to Code" <> '' then
            ShippingAddressJsonObj.Add('targetAddressId', SalesHeader."Ship-to Code")
        else
            ShippingAddressJsonObj.Add('targetAddressId', SalesHeader."Bill-to Customer No.");
        if strpos(SalesHeader."Ship-to Name", ' ') <> 0 then begin
            ShippingAddressJsonObj.Add('firstName', copystr(SalesHeader."Ship-to Name", 1, strpos(SalesHeader."Ship-to Name", ' ') - 1));
            ShippingAddressJsonObj.Add('lastName', copystr(SalesHeader."Ship-to Name", strpos(SalesHeader."Ship-to Name", ' ') + 1))
        end else begin
            ShippingAddressJsonObj.Add('firstName', SalesHeader."Ship-to Name");
            ShippingAddressJsonObj.Add('lastName', SalesHeader."Ship-to Name");
        end;

        ShippingAddressJsonObj.Add('street', SalesHeader."Ship-to Address");
        ShippingAddressJsonObj.Add('street2', SalesHeader."Ship-to Address 2");
        ShippingAddressJsonObj.Add('city', SalesHeader."Ship-to City");
        ShippingAddressJsonObj.Add('postcode', SalesHeader."Ship-to Post Code");
        ShippingAddressJsonObj.Add('countryId', SalesHeader."Ship-to Country/Region Code");
        ShippingAddressJsonObj.Add('regionId', SalesHeader."Ship-to County");
        ShippingAddressJsonObj.Add('telephone', SalesHeader."Sell-to Phone No.");
        InputDataJsonObj.Add('shippingAddress', ShippingAddressJsonObj);

        InputDataJsonObj.WriteTo(BodyContent);
        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', format(SalesHeader."No."));
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure SalesShipmentPushData(SalesShipHeader: Record "Sales Shipment Header"; Var
                                                                                         BodyContent: Text;
                                                                                         EcommerceShippingCode: code[50];
                                                                                         EcommerceShippingDescription: text[50];
                                                                                         ShippingAgentCode: text[30])
    var
        SalesShipmentLine: Record "Sales Shipment Line";
        i95Setup: Record "i95 Setup";
        ItemVariant: Record "Item Variant";
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        ShipmentItemEntityJsonArr: JsonArray;
        ShipmentItemEntityJsonObj: JsonObject;
        ShipmentItemEntityJsonToken: JsonToken;
        TrackingJsonArr: JsonArray;
        TrackingJsonObj: JsonObject;
        TrackingJsonToken: JsonToken;
    begin
        i95Setup.get();
        i95Setup.TestField("i95 Item Variant Seperator");

        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', Format(SalesShipHeader."Order No."));

        InputDataJsonObj.Add('targetOrderId', FORMAT(SalesShipHeader."Order No."));
        InputDataJsonObj.Add('reference', FORMAT(SalesShipHeader."Order No."));
        InputDataJsonObj.Add('targetId', FORMAT(SalesShipHeader."No."));
        InputDataJsonObj.Add('targetOrderStatus', 'Shipped');

        InputDataJsonObj.Add('shipmentItemEntity', ShipmentItemEntityJsonArr);
        InputDataJsonObj.Get('shipmentItemEntity', ShipmentItemEntityJsonToken);
        ShipmentItemEntityJsonArr := ShipmentItemEntityJsonToken.AsArray();
        clear(ShipmentItemEntityJsonObj);

        SalesShipmentLine.Reset();
        SalesShipmentLine.SetRange(SalesShipmentLine."Document No.", SalesShipHeader."No.");
        SalesShipmentLine.SetRange(SalesShipmentLine.Type, SalesShipmentLine.Type::Item);
        If SalesShipmentLine.FindSet() then
            repeat
                clear(ShipmentItemEntityJsonObj);

                if SalesShipmentLine."Variant Code" = '' then
                    ShipmentItemEntityJsonObj.Add('orderItemId', FORMAT(SalesShipmentLine."No."))
                else begin
                    ItemVariant.get(SalesShipmentLine."No.", SalesShipmentLine."Variant Code");
                    ShipmentItemEntityJsonObj.Add('orderItemId', SalesShipmentLine."No." + i95Setup."i95 Item Variant Seperator" + SalesShipmentLine."Variant Code");
                end;
                ShipmentItemEntityJsonObj.Add('qty', delchr(FORMAT(SalesShipmentLine.Quantity), '=', ','));
                ShipmentItemEntityJsonObj.Add('lineNo', delchr(FORMAT(SalesShipmentLine."Line No."), '=', ','));

                ShipmentItemEntityJsonArr.Add(ShipmentItemEntityJsonObj);

            until SalesShipmentLine.next() = 0;

        If SalesShipHeader."Package Tracking No." <> '' then begin
            InputDataJsonObj.Add('tracking', TrackingJsonArr);
            InputDataJsonObj.Get('tracking', TrackingJsonToken);
            TrackingJsonArr := TrackingJsonToken.AsArray();
            clear(TrackingJsonObj);
            TrackingJsonObj.Add('trackNumber', FORMAT(SalesShipHeader."Package Tracking No."));
            TrackingJsonObj.Add('title', lowercase(EcommerceShippingDescription));
            TrackingJsonObj.Add('carrier', LowerCase(EcommerceShippingCode));
            TrackingJsonArr.Add(TrackingJsonObj);
        end;

        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', SalesShipHeader."No.");
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    END;

    procedure SalesInvoicePushData(SalesInvoiceHdr: Record "Sales Invoice Header"; var BodyContent: Text)
    var
        SalesInvoiceLines: Record "Sales Invoice Line";
        i95Setup: Record "i95 Setup";
        ItemVariant: Record "Item Variant";
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        invoiceItemEntityJsonArr: JsonArray;
        invoiceItemEntityJsonObj: JsonObject;
        invoiceItemEntityJsonToken: JsonToken;
    begin
        i95Setup.get();
        i95Setup.TestField("i95 Item Variant Seperator");

        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', Format(SalesInvoiceHdr."Order No."));

        InputDataJsonObj.Add('targetOrderId', Format(SalesInvoiceHdr."Order No."));
        InputDataJsonObj.Add('reference', SalesInvoiceHdr."Order No.");
        InputDataJsonObj.Add('Completed', 'Completed');
        InputDataJsonObj.Add('targetId', SalesInvoiceHdr."No.");
        InputDataJsonObj.Add('LastUpdated', Format(SalesInvoiceHdr."i95 Last Sync DateTime"));
        InputDataJsonObj.Add('CreatedDate', Format(SalesInvoiceHdr."i95 Created Date Time"));

        InputDataJsonObj.Add('invoiceItemEntity', invoiceItemEntityJsonArr);
        InputDataJsonObj.Get('invoiceItemEntity', invoiceItemEntityJsonToken);
        invoiceItemEntityJsonArr := invoiceItemEntityJsonToken.AsArray();

        SalesInvoiceLines.Reset();
        SalesInvoiceLines.SetRange(SalesInvoiceLines."Document No.", SalesInvoiceHdr."No.");
        SalesInvoiceLines.SetRange(SalesInvoiceLines.Type, SalesInvoiceLines.Type::Item);
        If SalesInvoiceLines.FindSet() then
            repeat
                clear(invoiceItemEntityJsonObj);
                if SalesInvoiceLines."Variant Code" = '' then
                    invoiceItemEntityJsonObj.Add('orderItemId', Format(SalesInvoiceLines."No."))
                else begin
                    ItemVariant.get(SalesInvoiceLines."No.", SalesInvoiceLines."Variant Code");
                    invoiceItemEntityJsonObj.Add('orderItemId', SalesInvoiceLines."No." + i95Setup."i95 Item Variant Seperator" + SalesInvoiceLines."Variant Code");
                end;
                invoiceItemEntityJsonObj.Add('qty', delchr(format(SalesInvoiceLines.Quantity), '=', ','));
                invoiceItemEntityJsonObj.Add('lineNo', delchr(format(SalesInvoiceLines."Line No."), '=', ','));

                invoiceItemEntityJsonArr.Add(invoiceItemEntityJsonObj);

            until SalesInvoiceLines.Next() = 0;
        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', SalesInvoiceHdr."No.");
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure CancelOrderPushData(DetSyncLogEntry: Record "i95 Detailed Sync Log Entry"; var BodyContent: Text)
    var
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        customerJsonObj: JsonObject;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', DetSyncLogEntry."Field 2");

        InputDataJsonObj.Add('reference', DetSyncLogEntry."Field 2");
        InputDataJsonObj.Add('targetId', DetSyncLogEntry."Field 1");
        InputDataJsonObj.Add('isCancelled', 1);

        Clear(customerJsonObj);
        customerJsonObj.Add('targetId', DetSyncLogEntry."Field 2");
        customerJsonObj.Add('creditLimitType', 'NoCredit');
        InputDataJsonObj.Add('customer', customerJsonObj);
        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', DetSyncLogEntry."Field 1");
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure TaxBusPostingGroupPushData(TaxBusPostingGrp: Record "VAT Business Posting Group"; var BodyContent: Text)
    var
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', TaxBusPostingGrp.Description);

        InputDataJsonObj.Add('code', TaxBusPostingGrp.Code);
        InputDataJsonObj.Add('description', TaxBusPostingGrp.Description);
        InputDataJsonObj.Add('targetId', TaxBusPostingGrp.Code);
        InputDataJsonObj.Add('reference', TaxBusPostingGrp.Description);

        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', TaxBusPostingGrp.Code);
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure TaxProdPostingGroupPushData(TaxProdPostingGrp: Record "VAT Product Posting Group"; var BodyContent: Text)
    var
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', TaxProdPostingGrp.Description);

        InputDataJsonObj.Add('Code', TaxProdPostingGrp.Code);
        InputDataJsonObj.Add('description', TaxProdPostingGrp.Description);
        InputDataJsonObj.Add('targetId', TaxProdPostingGrp.Code);
        InputDataJsonObj.Add('reference', TaxProdPostingGrp.Description);
        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', TaxProdPostingGrp.Code);
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure TaxPostingSetupPushData(TaxPostingSetup: Record "VAT Posting Setup"; var BodyContent: Text)
    var
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', TaxPostingSetup."VAT Prod. Posting Group");

        InputDataJsonObj.Add('taxBusPostingGroupCode', TaxPostingSetup."VAT Bus. Posting Group");
        InputDataJsonObj.Add('taxProductPostingGroupCode', TaxPostingSetup."VAT Prod. Posting Group");
        InputDataJsonObj.Add('taxPercentage', format(TaxPostingSetup."VAT %"));
        InputDataJsonObj.Add('targetId', TaxPostingSetup."VAT Bus. Posting Group");
        InputDataJsonObj.Add('reference', TaxPostingSetup."VAT Prod. Posting Group");

        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', TaxPostingSetup."VAT Bus. Posting Group");
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure CustomerDiscountGroupPushData(CustDiscountGroup: Record "Customer Discount Group"; var BodyContent: Text)
    var
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', CustDiscountGroup.Code);

        InputDataJsonObj.Add('targetId', CustDiscountGroup.Code);
        InputDataJsonObj.Add('reference', CustDiscountGroup.Code);
        InputDataJsonObj.Add('description', CustDiscountGroup.Description);

        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', CustDiscountGroup.Code);
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure ItemDiscountGroupPushData(ItemDiscountGroup: Record "Item Discount Group"; var BodyContent: Text)
    var
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', ItemDiscountGroup.Code);

        InputDataJsonObj.Add('targetId', ItemDiscountGroup.Code);
        InputDataJsonObj.Add('reference', ItemDiscountGroup.Code);
        InputDataJsonObj.Add('description', ItemDiscountGroup.Description);
        InputDataJsonObj.Add('itemDiscountGroupId', ItemDiscountGroup.Code);
        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', ItemDiscountGroup.Code);
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure PullResponse(MessageID: Integer; var BodyContent: Text)
    var
        PushDatajsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        ResponseDataJsonObj: JsonObject;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('localMessageId', format(LocalMessageID));
        PushDatajsonObj.Add('entityId', EntityID);
        PushDatajsonObj.Add('messageStatus', MessageStatus);
        PushDatajsonObj.Add('syncCounter', SyncCounter);
        PushDatajsonObj.Add('schedulerId', SchedulerID);
        PushDatajsonObj.Add('messageId', MessageID);
        PushDatajsonObj.Add('responseData', ResponseDataJsonObj);

        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure PullResponseAcknowledge(DetailedSyncLogEntry: Record "i95 Detailed Sync Log Entry"; var BodyContent: Text)
    var
        PushDatajsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        ResponseDataJsonObj: JsonObject;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('localMessageId', format(LocalMessageID));
        PushDatajsonObj.Add('entityId', EntityID);
        PushDatajsonObj.Add('messageStatus', MessageStatus);
        PushDatajsonObj.Add('syncCounter', SyncCounter);
        PushDatajsonObj.Add('schedulerId', SchedulerID);
        PushDatajsonObj.Add('reference', DetailedSyncLogEntry."Field 2");
        PushDatajsonObj.Add('messageId', DetailedSyncLogEntry."Message ID");
        PushDatajsonObj.Add('responseData', ResponseDataJsonObj);
        PushDatajsonObj.Add('targetId', DetailedSyncLogEntry."Target ID");
        PushDatajsonObj.Add('sourceId', format(DetailedSyncLogEntry."i95 Source ID"));

        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure SetDefaultValues(i95LocalMessageID: Integer; i95EntityID: Integer; i95MessageStatus: Integer; i95SyncCounter: Integer; i95SchedulerID: Integer)
    begin
        LocalMessageID := i95LocalMessageID;
        EntityID := i95EntityID;
        MessageStatus := i95MessageStatus;
        SyncCounter := i95SyncCounter;
        SchedulerID := i95SchedulerID;
    end;

    procedure CustomerPushResponseData(Customer: Record Customer; var BodyContent: Text; DetailedSyncLogEntry: Record "i95 Detailed Sync Log Entry")
    var
        ShipToAddress: Record "Ship-to Address";
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        AddressesJsonArr: JsonArray;
        AddressesJsonObj: JsonObject;
        AddressesJsonToken: JsonToken;
        ResponseDataJsonObj: JsonObject;
        KeysJsonObj: JsonObject;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', Customer."E-Mail");
        InputDataJsonObj.Add('addresses', AddressesJsonArr);
        InputDataJsonObj.Get('addresses', AddressesJsonToken);
        AddressesJsonArr := AddressesJsonToken.AsArray();

        ShipToAddress.Reset();
        ShipToAddress.SetRange(ShipToAddress."Customer No.", Customer."No.");
        if ShipToAddress.FindSet() then
            repeat
                clear(AddressesJsonObj);
                AddressesJsonObj.Add('targetId', ShipToAddress.Code);
                AddressesJsonObj.Add('sourceId', ShipToAddress.Code);
                AddressesJsonObj.Add('targetCustomerId', Customer."No.");
                AddressesJsonArr.Add(AddressesJsonObj);
            until ShipToAddress.Next() = 0;

        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('MessageId', DetailedSyncLogEntry."Message ID");
        PushDatajsonObj.Add('result', true);
        PushDatajsonObj.Add('statusId', 0);
        PushDatajsonObj.Add('ResponseData', ResponseDataJsonObj);
        PushDatajsonObj.Add('TargetId', Customer."No.");
        PushDatajsonObj.Add('sourceId', Format(DetailedSyncLogEntry."i95 Source ID"));
        PushDatajsonObj.Add('Keys', KeysJsonObj);

        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure CustomerPrieGroupPushResponseData(CustomerPriceGroup: Record "Customer Price Group"; var BodyContent: Text; DetailedSyncLogEntry: Record "i95 Detailed Sync Log Entry")
    var
        PushDatajsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        ResponseDataJsonObj: JsonObject;
        KeysJsonObj: JsonObject;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('reference', CustomerPriceGroup.Code);
        PushDatajsonObj.Add('messageId', DetailedSyncLogEntry."Message ID");
        PushDatajsonObj.Add('result', true);
        PushDatajsonObj.Add('statusId', 0);
        PushDatajsonObj.Add('responseData', ResponseDataJsonObj);
        PushDatajsonObj.Add('targetId', CustomerPriceGroup.Code);
        PushDatajsonObj.Add('sourceId', format(DetailedSyncLogEntry."i95 Source ID"));
        PushDatajsonObj.Add('keys', KeysJsonObj);
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure SalesOrderPushResponseData(SalesHeader: Record "Sales Header"; var BodyContent: Text; DetailedSyncLogEntry: Record "i95 Detailed Sync Log Entry")
    var
        Customer: Record Customer;
        PushDatajsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        ResponseDataJsonObj: JsonObject;
        KeysJsonObj: JsonObject;
    begin
        If Customer.get(SalesHeader."Sell-to Customer No.") then;
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('reference', Customer."E-Mail");
        PushDatajsonObj.Add('messageId', DetailedSyncLogEntry."Message ID");
        PushDatajsonObj.Add('result', true);
        PushDatajsonObj.Add('statusId', 0);
        PushDatajsonObj.Add('responseData', ResponseDataJsonObj);
        PushDatajsonObj.Add('targetId', SalesHeader."No.");
        PushDatajsonObj.Add('sourceId', Format(DetailedSyncLogEntry."i95 Source ID"));
        PushDatajsonObj.Add('keys', KeysJsonObj);

        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure ProductPushResponseData(Item: Record Item; var BodyContent: text; DetailedSyncLogEntry: Record "i95 Detailed Sync Log Entry")
    var
        PushDatajsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        ResponseDataJsonObj: JsonObject;
        KeysJsonObj: JsonObject;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('reference', Item.Description);
        PushDatajsonObj.Add('messageId', DetailedSyncLogEntry."Message ID");
        PushDatajsonObj.Add('result', true);
        PushDatajsonObj.Add('statusId', 0);
        PushDatajsonObj.Add('responseData', ResponseDataJsonObj);
        PushDatajsonObj.Add('targetId', Item."No.");
        PushDatajsonObj.Add('sourceId', Format(DetailedSyncLogEntry."i95 Source ID"));
        PushDatajsonObj.Add('keys', KeysJsonObj);

        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure DiscountPricePushData(Item: Record item; var BodyContent: text)
    var
        SalesLineDiscount: Record "Sales Line Discount";
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        DiscountPricesJsonArr: JsonArray;
        DiscountPricesJsonToken: JsonToken;
        DiscountPricesJsonObj: JsonObject;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushdatajsonObj.Add('Reference', Item."No.");

        InputDataJsonObj.Add('targetId', Item."No.");
        InputDataJsonObj.Add('reference', Item."No.");

        InputDataJsonObj.Add('discountPrices', DiscountPricesJsonArr);
        InputDataJsonObj.get('discountPrices', DiscountPricesJsonToken);
        DiscountPricesJsonArr := DiscountPricesJsonToken.AsArray();

        SalesLineDiscount.Reset();
        SalesLineDiscount.SetRange(SalesLineDiscount.Type, SalesLineDiscount.Type::Item);
        SalesLineDiscount.SetRange(SalesLineDiscount.Code, Item."No.");
        if SalesLineDiscount.FindSet() then
            repeat
                Clear(DiscountPricesJsonObj);
                DiscountPricesJsonObj.Add('targetId', Item."No.");
                DiscountPricesJsonObj.Add('reference', Item."No.");
                DiscountPricesJsonObj.Add('code', Item."No.");
                DiscountPricesJsonObj.Add('salesCode', SalesLineDiscount."Sales Code");

                case SalesLineDiscount."Sales Type" of
                    SalesLineDiscount."Sales Type"::"Customer Disc. Group":
                        DiscountPricesJsonObj.Add('salesType', 'CustomerDiscountGroup');
                    SalesLineDiscount."Sales Type"::"All Customers":
                        DiscountPricesJsonObj.Add('salesType', 'AllCustomers');

                    else
                        DiscountPricesJsonObj.Add('salesType', format(SalesLineDiscount."Sales Type"));
                end;

                DiscountPricesJsonObj.Add('type', format(SalesLineDiscount.Type));
                DiscountPricesJsonObj.Add('price', format(SalesLineDiscount."Line Discount %"));
                DiscountPricesJsonObj.Add('qty', format(SalesLineDiscount."Minimum Quantity"));
                DiscountPricesJsonObj.Add('startDate', format(SalesLineDiscount."Starting Date"));
                DiscountPricesJsonObj.Add('endDate', format(SalesLineDiscount."Ending Date"));

            until SalesLineDiscount.Next() = 0;
        DiscountPricesJsonArr.Add(DiscountPricesJsonObj);

        InputDataJsonObj.WriteTo(BodyContent);
        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('MessageId', 0);

        PushDatajsonObj.Add('TargetId', Item."No.");

        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure DiscountPriceItemDiscGrpPushData(ItemDiscGrp: Record "Item Discount Group"; var BodyContent: text)
    var
        SalesLineDiscount: Record "Sales Line Discount";
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        DiscountPricesJsonArr: JsonArray;
        DiscountPricesJsonToken: JsonToken;
        DiscountPricesJsonObj: JsonObject;
    begin
        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', ItemDiscGrp.Code);

        InputDataJsonObj.Add('targetId', ItemDiscGrp.Code);
        InputDataJsonObj.Add('reference', ItemDiscGrp.Code);

        InputDataJsonObj.Add('discountPrices', DiscountPricesJsonArr);
        InputDataJsonObj.get('discountPrices', DiscountPricesJsonToken);
        DiscountPricesJsonArr := DiscountPricesJsonToken.AsArray();

        SalesLineDiscount.Reset();
        SalesLineDiscount.SetRange(SalesLineDiscount.Type, SalesLineDiscount.Type::"Item Disc. Group");
        SalesLineDiscount.SetRange(SalesLineDiscount.Code, ItemDiscGrp.Code);
        if SalesLineDiscount.FindSet() then
            repeat
                Clear(DiscountPricesJsonObj);
                DiscountPricesJsonObj.Add('targetId', ItemDiscGrp.Code);
                DiscountPricesJsonObj.Add('reference', ItemDiscGrp.Code);
                DiscountPricesJsonObj.Add('code', ItemDiscGrp.Code);
                DiscountPricesJsonObj.Add('salesCode', SalesLineDiscount."Sales Code");

                case SalesLineDiscount."Sales Type" of
                    SalesLineDiscount."Sales Type"::"Customer Disc. Group":
                        DiscountPricesJsonObj.Add('salesType', 'CustomerDiscountGroup');
                    SalesLineDiscount."Sales Type"::"All Customers":
                        DiscountPricesJsonObj.Add('salesType', 'AllCustomers');
                    else
                        DiscountPricesJsonObj.Add('salesType', format(SalesLineDiscount."Sales Type"));
                end;

                If SalesLineDiscount.Type = SalesLineDiscount.Type::"Item Disc. Group" then
                    DiscountPricesJsonObj.Add('type', 'ItemDiscountGroup')
                else
                    DiscountPricesJsonObj.Add('type', format(SalesLineDiscount.Type));

                DiscountPricesJsonObj.Add('price', format(SalesLineDiscount."Line Discount %"));
                DiscountPricesJsonObj.Add('qty', format(SalesLineDiscount."Minimum Quantity"));
                DiscountPricesJsonObj.Add('startDate', format(SalesLineDiscount."Starting Date"));
                DiscountPricesJsonObj.Add('endDate', format(SalesLineDiscount."Ending Date"));

            until SalesLineDiscount.Next() = 0;
        DiscountPricesJsonArr.Add(DiscountPricesJsonObj);
        InputDataJsonObj.WriteTo(BodyContent);
        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('MessageId', 0);
        PushDatajsonObj.Add('TargetId', ItemDiscGrp.Code);
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure EditSalesOrderPushData(SalesHeader: record "Sales Header"; var BodyContent: text; EcommerceShippingCode: code[50]; EcommercePaymentMethodCode: Code[50]; EcommerceShippingTitle: text[50]; ShippingAgentCode: text[30])
    var
        SalesLine: Record "Sales Line";
        i95Setup: Record "i95 Setup";
        ItemVariant: Record "Item Variant";
        SalesLine2: Record "Sales Line";
        FirstSalesLine: Boolean;
        DiscountAmount: Decimal;
        AttributeCode: text[30];
        AttributeValue: Text[30];
        PatternType: Integer;
        Pattern: text[50];
        VariantCode: text[50];
        i: Integer;
        VariantSepratorCount: Integer;
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        DiscountAmountJsonArr: JsonArray;
        DiscountAmountJsonObj: JsonObject;
        DiscountAmountJsonToken: JsonToken;
        PaymentJsonArr: JsonArray;
        PaymentJsonObj: JsonObject;
        PaymentJsonToken: JsonToken;
        OrderItemsJsonArr: JsonArray;
        OrderItemsJsonObj: JsonObject;
        OrderItemsJsonToken: JsonToken;
        OrderItemsdiscountAmountJsonArr: JsonArray;
        OrderItemsdiscountAmountJsonObj: JsonObject;
        OrderItemsdiscountAmountJsonToken: JsonToken;
        OrderItemVariantJsonArr: JsonArray;
        OrderItemVariantJsonObj: JsonObject;
        OrderItemVariantJsonToken: JsonToken;
        AttributewithKeyJsonArr: JsonArray;
        AttributewithKeyJsonObj: JsonObject;
        AttributewithKeyJsonToken: JsonToken;
        BillingAddressJsonObj: JsonObject;
        ShippingAddressJsonObj: JsonObject;
    begin
        i95Setup.get();
        i95Setup.TestField("i95 Item Variant Seperator");
        i95Setup.TestField("i95 Item Variant Pattern 1");
        i95Setup.TestField("i95 Item Variant Pattern 2");
        i95Setup.TestField("i95 Item Variant Pattern 3");

        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        FirstSalesLine := true;

        SalesHeader.CalcFields(Amount, "Amount Including VAT");

        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', Format(SalesHeader."Sell-to Customer No."));

        InputDataJsonObj.Add('targetId', SalesHeader."No.");
        InputDataJsonObj.Add('reference', SalesHeader."Sell-to Customer No.");
        InputDataJsonObj.Add('targetOrderId', SalesHeader."No.");

        If SalesHeader."i95 Order Status" = SalesHeader."i95 Order Status"::Edited then
            InputDataJsonObj.Add('targetOrderEditStatus', 'edited')
        else
            if SalesHeader."i95 Order Status" = SalesHeader."i95 Order Status"::Updated then
                InputDataJsonObj.Add('targetOrderEditStatus', 'updated');

        InputDataJsonObj.Add('sourceOrderId', SalesHeader."i95 Reference ID");
        InputDataJsonObj.Add('targetCustomerId', SalesHeader."Sell-to Customer No.");
        InputDataJsonObj.Add('carrierName', lowercase(ShippingAgentCode));
        InputDataJsonObj.Add('targetShippingAddressId', SalesHeader."Sell-to Customer No.");
        InputDataJsonObj.Add('targetBillingAddressId', SalesHeader."Bill-to Customer No.");
        InputDataJsonObj.Add('orderCreatedDate', format(SalesHeader."Order Date"));
        InputDataJsonObj.Add('shippingMethod', lowercase(EcommerceShippingCode));
        InputDataJsonObj.Add('shippingTitle', lowercase(EcommerceShippingTitle));
        InputDataJsonObj.Add('payment', PaymentJsonArr);
        InputDataJsonObj.Get('payment', PaymentJsonToken);
        PaymentJsonArr := PaymentJsonToken.AsArray();
        clear(PaymentJsonObj);
        PaymentJsonObj.Add('paymentMethod', LowerCase(EcommercePaymentMethodCode));
        PaymentJsonArr.Add(PaymentJsonObj);

        SalesLine.Reset();
        SalesLine.SetRange(SalesLine."Document Type", SalesLine."Document Type"::Order);
        SalesLine.SetRange(SalesLine."Document No.", SalesHeader."No.");
        SalesLine.SetRange(SalesLine.Type, SalesLine.Type::"G/L Account");
        SalesLine.SetRange(SalesLine."No.", i95Setup."i95 Shipping Charge G/L Acc");
        If SalesLine.FindFirst() then
            InputDataJsonObj.Add('shippingAmount', format(SalesLine."Unit Price"))
        else
            InputDataJsonObj.Add('shippingAmount', 0);

        Clear(DiscountAmount);
        SalesLine.Reset();
        SalesLine.SetRange(SalesLine."Document Type", SalesLine."Document Type"::Order);
        SalesLine.SetRange(SalesLine."Document No.", SalesHeader."No.");
        If SalesLine.Findset() then
            repeat
                DiscountAmount += SalesLine."Line Discount Amount";
            until SalesLine.Next() = 0;

        InputDataJsonObj.Add('subTotal', delchr(format(SalesHeader.Amount), '=', ','));
        InputDataJsonObj.Add('orderDocumentAmount', delchr(format(SalesHeader."Amount Including VAT"), '=', ','));
        InputDataJsonObj.Add('taxAmount', delchr(format(SalesHeader."Amount Including VAT" - SalesHeader.Amount), '=', ','));

        InputDataJsonObj.Add('discount', DiscountAmountJsonArr);
        InputDataJsonObj.Get('discount', DiscountAmountJsonToken);
        DiscountAmountJsonArr := DiscountAmountJsonToken.AsArray();
        clear(DiscountAmountJsonObj);

        DiscountAmountJsonObj.Add('discountType', 'discount');
        DiscountAmountJsonObj.Add('discountAmount', format(DiscountAmount));
        DiscountAmountJsonArr.Add(DiscountAmountJsonObj);

        InputDataJsonObj.Add('orderItems', OrderItemsJsonArr);
        InputDataJsonObj.Get('orderItems', OrderItemsJsonToken);
        OrderItemsJsonArr := OrderItemsJsonToken.AsArray();

        SalesLine.Reset();
        SalesLine.SetRange(SalesLine."Document Type", SalesLine."Document Type"::Order);
        SalesLine.SetRange(SalesLine."Document No.", SalesHeader."No.");
        SalesLine.SetRange(SalesLine.Type, SalesLine.Type::Item);
        if SalesLine.FindSet() then
            repeat
                clear(OrderItemsJsonObj);

                if SalesLine."Variant Code" = '' then begin

                    OrderItemsJsonObj.Add('sku', SalesLine."No.");
                    OrderItemsJsonObj.Add('qty', delchr(format(SalesLine.Quantity), '=', ','));
                    OrderItemsJsonObj.Add('price', delchr(format(SalesLine."Unit Price"), '=', ','));

                    Clear(OrderItemsdiscountAmountJsonArr);
                    clear(OrderItemsdiscountAmountJsonObj);
                    OrderItemsJsonObj.Add('discount', OrderItemsdiscountAmountJsonArr);
                    OrderItemsJsonObj.Get('discount', OrderItemsdiscountAmountJsonToken);
                    OrderItemsdiscountAmountJsonArr := OrderItemsdiscountAmountJsonToken.AsArray();

                    SalesLine2.Reset();
                    SalesLine2.SetRange(SalesLine2."Document Type", SalesLine2."Document Type"::Order);
                    SalesLine2.SetRange(SalesLine2."Document No.", SalesHeader."No.");
                    SalesLine2.SetRange(SalesLine2."Line No.", SalesLine."Line No.");
                    If SalesLine2.FindFirst() then begin
                        OrderItemsdiscountAmountJsonObj.Add('discountType', 'discount');
                        OrderItemsdiscountAmountJsonObj.Add('discountAmount', format(SalesLine2."Line Discount Amount"));
                    end;
                    OrderItemsdiscountAmountJsonArr.Add(OrderItemsdiscountAmountJsonObj);

                    OrderItemsJsonObj.Add('lineNo', delchr(format(SalesLine."Line No."), '=', ','));
                    OrderItemsJsonArr.Add(OrderItemsJsonObj);
                end else begin
                    Clear(VariantCode);
                    Clear(VariantSepratorCount);
                    Clear(Pattern);
                    Clear(PatternType);
                    ItemVariant.get(SalesLine."No.", SalesLine."Variant Code");

                    OrderItemsJsonObj.Add('sku', ItemVariant."Item No." + i95Setup."i95 Item Variant Seperator" + ItemVariant.Code);
                    OrderItemsJsonObj.Add('qty', delchr(format(SalesLine.Quantity), '=', ','));
                    OrderItemsJsonObj.Add('price', delchr(format(SalesLine."Unit Price"), '=', ','));
                    OrderItemsJsonObj.Add('markdownPrice', 0);
                    OrderItemsJsonObj.Add('parentSku', ItemVariant."Item No.");
                    OrderItemsJsonObj.Add('retailVariantId', ItemVariant.Code);

                    //Item Variant
                    Clear(OrderItemVariantJsonObj);
                    clear(OrderItemVariantJsonArr);
                    Clear(AttributewithKeyJsonArr);
                    OrderItemsJsonObj.Add('itemVariants', OrderItemVariantJsonArr);
                    OrderItemsJsonObj.Get('itemVariants', OrderItemVariantJsonToken);
                    OrderItemVariantJsonArr := OrderItemVariantJsonToken.AsArray();
                    clear(OrderItemVariantJsonObj);

                    OrderItemVariantJsonObj.Add('attributeWithKey', AttributewithKeyJsonArr);
                    OrderItemVariantJsonObj.get('attributeWithKey', AttributewithKeyJsonToken);
                    AttributewithKeyJsonArr := AttributewithKeyJsonToken.AsArray();
                    clear(AttributewithKeyJsonObj);

                    VariantCode := ItemVariant.Code;
                    VariantSepratorCount := STRLEN(DELCHR(VariantCode, '=', DELCHR(VariantCode, '=', i95Setup."i95 Item Variant Seperator")));

                    case VariantSepratorCount of
                        0:
                            begin
                                Pattern := i95Setup."i95 Item Variant Pattern 1";
                                PatternType := 1;
                            end;
                        1:
                            begin
                                Pattern := i95Setup."i95 Item Variant Pattern 2";
                                PatternType := 2;
                            end;
                        2:
                            begin
                                Pattern := i95Setup."i95 Item Variant Pattern 3";
                                PatternType := 3;
                            end;
                    end;

                    ItemVariant.get(SalesLine."No.", SalesLine."Variant Code");
                    VariantCode := ItemVariant.Code;
                    for i := 1 to PatternType do begin
                        Clear(AttributewithKeyJsonObj);
                        Clear(AttributeCode);
                        Clear(AttributeValue);
                        if StrPos(Pattern, i95Setup."i95 Item Variant Seperator") <> 0 then begin
                            AttributeCode := copystr(CopyStr(Pattern, 1, StrPos(Pattern, i95Setup."i95 Item Variant Seperator") - 1), 1, 30);
                            AttributeValue := copystr(copystr(VariantCode, 1, strpos(VariantCode, i95Setup."i95 Item Variant Seperator") - 1), 1, 30);

                            Pattern := copystr(CopyStr(Pattern, StrPos(Pattern, i95Setup."i95 Item Variant Seperator") + 1), 1, 30);
                            VariantCode := copystr(CopyStr(VariantCode, strpos(VariantCode, i95Setup."i95 Item Variant Seperator") + 1), 1, 50);
                        end else begin
                            AttributeCode := CopyStr(Pattern, 1, 30);
                            AttributeValue := CopyStr(VariantCode, 1, 30);
                        end;

                        AttributewithKeyJsonObj.Add('attributeCode', AttributeCode);
                        AttributewithKeyJsonObj.Add('attributeValue', AttributeValue);
                        AttributewithKeyJsonObj.Add('attributeType', 'select');

                        AttributewithKeyJsonArr.Add(AttributewithKeyJsonObj);
                    end;
                    OrderItemVariantJsonArr.Add(OrderItemVariantJsonObj);

                    Clear(OrderItemsdiscountAmountJsonArr);
                    Clear(OrderItemsdiscountAmountJsonObj);
                    OrderItemsJsonObj.Add('discount', OrderItemsdiscountAmountJsonArr);
                    OrderItemsJsonObj.Get('discount', OrderItemsdiscountAmountJsonToken);
                    OrderItemsdiscountAmountJsonArr := OrderItemsdiscountAmountJsonToken.AsArray();

                    SalesLine2.Reset();
                    SalesLine2.SetRange(SalesLine2."Document Type", SalesLine2."Document Type"::Order);
                    SalesLine2.SetRange(SalesLine2."Document No.", SalesHeader."No.");
                    SalesLine2.SetRange(SalesLine2."Line No.", SalesLine."Line No.");
                    If SalesLine2.FindFirst() then begin
                        OrderItemsdiscountAmountJsonObj.Add('discountType', 'discount');
                        OrderItemsdiscountAmountJsonObj.Add('discountAmount', format(SalesLine2."Line Discount Amount"));
                    end;
                    OrderItemsdiscountAmountJsonArr.Add(OrderItemsdiscountAmountJsonObj);

                    OrderItemsJsonObj.Add('lineNo', delchr(format(SalesLine."Line No."), '=', ','));
                    OrderItemsJsonArr.Add(OrderItemsJsonObj);
                end;

            until SalesLine.Next() = 0;

        Clear(BillingAddressJsonObj);
        BillingAddressJsonObj.Add('targetAddressId', 'I95DEFAULT');
        BillingAddressJsonObj.Add('isDefaultBilling', true);
        if strpos(SalesHeader."Bill-to Name", ' ') <> 0 then begin
            BillingAddressJsonObj.Add('firstName', copystr(SalesHeader."Bill-to Name", 1, strpos(SalesHeader."Bill-to Name", ' ') - 1));
            BillingAddressJsonObj.Add('lastName', copystr(SalesHeader."Bill-to Name", strpos(SalesHeader."Bill-to Name", ' ') + 1));
        end else begin
            BillingAddressJsonObj.Add('firstName', SalesHeader."Bill-to Name");
            BillingAddressJsonObj.Add('lastName', SalesHeader."Bill-to Name");
        end;
        BillingAddressJsonObj.Add('street', SalesHeader."Bill-to Address");
        BillingAddressJsonObj.Add('street2', SalesHeader."Bill-to Address 2");
        BillingAddressJsonObj.Add('city', SalesHeader."Bill-to City");
        BillingAddressJsonObj.Add('postcode', SalesHeader."Bill-to Post Code");
        BillingAddressJsonObj.Add('countryId', SalesHeader."Bill-to Country/Region Code");
        BillingAddressJsonObj.Add('regionId', SalesHeader."Bill-to County");
        BillingAddressJsonObj.Add('telephone', SalesHeader."Sell-to Phone No.");
        InputDataJsonObj.Add('billingAddress', BillingAddressJsonObj);

        Clear(ShippingAddressJsonObj);
        If SalesHeader."Ship-to Code" <> '' then
            ShippingAddressJsonObj.Add('targetAddressId', SalesHeader."Ship-to Code")
        else
            ShippingAddressJsonObj.Add('targetAddressId', SalesHeader."Bill-to Customer No.");
        if strpos(SalesHeader."Ship-to Name", ' ') <> 0 then begin
            ShippingAddressJsonObj.Add('firstName', copystr(SalesHeader."Ship-to Name", 1, strpos(SalesHeader."Ship-to Name", ' ') - 1));
            ShippingAddressJsonObj.Add('lastName', copystr(SalesHeader."Ship-to Name", strpos(SalesHeader."Ship-to Name", ' ') + 1))
        end else begin
            ShippingAddressJsonObj.Add('firstName', SalesHeader."Ship-to Name");
            ShippingAddressJsonObj.Add('lastName', SalesHeader."Ship-to Name");
        end;

        ShippingAddressJsonObj.Add('street', SalesHeader."Ship-to Address");
        ShippingAddressJsonObj.Add('street2', SalesHeader."Ship-to Address 2");
        ShippingAddressJsonObj.Add('city', SalesHeader."Ship-to City");
        ShippingAddressJsonObj.Add('postcode', SalesHeader."Ship-to Post Code");
        ShippingAddressJsonObj.Add('countryId', SalesHeader."Ship-to Country/Region Code");
        ShippingAddressJsonObj.Add('regionId', SalesHeader."Ship-to County");
        ShippingAddressJsonObj.Add('telephone', SalesHeader."Sell-to Phone No.");
        InputDataJsonObj.Add('shippingAddress', ShippingAddressJsonObj);

        InputDataJsonObj.WriteTo(BodyContent);
        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('TargetId', format(SalesHeader."No."));
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;

    procedure ChildProductPushData(ItemVariant: Record "Item Variant"; var BodyContent: Text)
    var
        i95Setup: Record "i95 Setup";
        Item: Record Item;
        PushDatajsonObj: JsonObject;
        InputDataJsonObj: JsonObject;
        InputDataJsonArr: JsonArray;
        RequestDataJsonArr: JsonArray;
        RequestDataJsonToken: JsonToken;
        AttributewithKeyJsonArr: JsonArray;
        AttributewithKeyJsonObj: JsonObject;
        AttributewithKeyJsonToken: JsonToken;
        AttributeCode: text[30];
        AttributeValue: Text[30];
        PatternType: Integer;
        Pattern: text[50];
        VariantCode: text[50];
        i: Integer;
        VariantSepratorCount: Integer;
    begin
        i95Setup.get();
        i95Setup.TestField("i95 Item Variant Seperator");
        i95Setup.TestField("i95 Item Variant Pattern 1");
        i95Setup.TestField("i95 Item Variant Pattern 2");
        i95Setup.TestField("i95 Item Variant Pattern 3");

        Item.get(ItemVariant."Item No.");

        if FirstRecord then
            BodyContentjsonObj.Add('requestData', RequestDataJsonArr);

        FirstRecord := false;
        BodyContentjsonObj.Get('requestData', RequestDataJsonToken);
        RequestDataJsonArr := RequestDataJsonToken.AsArray();

        PushDatajsonObj.Add('Reference', ItemVariant.Description);

        InputDataJsonObj.Add('targetId', ItemVariant."Item No." + i95Setup."i95 Item Variant Seperator" + ItemVariant.Code);
        InputDataJsonObj.Add('reference', ItemVariant.Description);
        InputDataJsonObj.Add('sku', ItemVariant."Item No." + i95Setup."i95 Item Variant Seperator" + ItemVariant.Code);
        InputDataJsonObj.Add('parentSku', ItemVariant."Item No.");
        InputDataJsonObj.Add('retailVariantId', ItemVariant.Code);
        InputDataJsonObj.Add('dateCreated', format(ItemVariant."i95 Created DateTime"));
        InputDataJsonObj.Add('name', ItemVariant.Description);
        InputDataJsonObj.Add('description', ItemVariant.Description);
        InputDataJsonObj.Add('shortDescription', ItemVariant."Description 2");

        InputDataJsonObj.Add('attributeWithKey', AttributewithKeyJsonArr);
        InputDataJsonObj.get('attributeWithKey', AttributewithKeyJsonToken);
        AttributewithKeyJsonArr := AttributewithKeyJsonToken.AsArray();
        clear(AttributewithKeyJsonObj);

        VariantCode := ItemVariant.Code;

        VariantSepratorCount := STRLEN(DELCHR(VariantCode, '=', DELCHR(VariantCode, '=', i95Setup."i95 Item Variant Seperator")));

        clear(Pattern);
        clear(PatternType);
        case VariantSepratorCount of
            0:
                begin
                    Pattern := i95Setup."i95 Item Variant Pattern 1";
                    PatternType := 1;
                end;
            1:
                begin
                    Pattern := i95Setup."i95 Item Variant Pattern 2";
                    PatternType := 2;
                end;
            2:
                begin
                    Pattern := i95Setup."i95 Item Variant Pattern 3";
                    PatternType := 3;
                end;
        end;

        for i := 1 to PatternType do begin
            Clear(AttributewithKeyJsonObj);
            if StrPos(Pattern, i95Setup."i95 Item Variant Seperator") <> 0 then begin
                AttributeCode := copystr(CopyStr(Pattern, 1, StrPos(Pattern, i95Setup."i95 Item Variant Seperator") - 1), 1, 30);
                AttributeValue := copystr(copystr(VariantCode, 1, strpos(VariantCode, i95Setup."i95 Item Variant Seperator") - 1), 1, 30);

                Pattern := copystr(CopyStr(Pattern, StrPos(Pattern, i95Setup."i95 Item Variant Seperator") + 1), 1, 30);
                VariantCode := copystr(CopyStr(VariantCode, strpos(VariantCode, i95Setup."i95 Item Variant Seperator") + 1), 1, 50);
            end else begin
                AttributeCode := CopyStr(Pattern, 1, 30);
                AttributeValue := CopyStr(VariantCode, 1, 30);
            end;

            AttributewithKeyJsonObj.Add('attributeCode', AttributeCode);
            AttributewithKeyJsonObj.Add('attributeValue', AttributeValue);
            AttributewithKeyJsonObj.Add('attributeType', 'select');

            AttributewithKeyJsonArr.Add(AttributewithKeyJsonObj);
        end;

        InputDataJsonArr.Add(AttributewithKeyJsonObj);

        InputDataJsonObj.Add('weight', format(Item."Gross Weight"));
        InputDataJsonObj.Add('price', format(Item."Unit Price"));
        If Item."Base Unit of Measure" <> '' then
            InputDataJsonObj.Add('unitOfMeasure', Item."Base Unit of Measure")
        else
            InputDataJsonObj.Add('unitOfMeasure', i95Setup."Default UOM");

        InputDataJsonObj.Add('isVisible', 1);
        InputDataJsonObj.Add('status', 1);
        InputDataJsonObj.Add('qty', 0);
        InputDataJsonObj.Add('updatedTime', format(ItemVariant."i95 Last Modification DateTime"));

        InputDataJsonObj.WriteTo(BodyContent);

        PushDatajsonObj.Add('InputData', BodyContent);
        PushDatajsonObj.Add('MessageId', 0);
        PushDatajsonObj.Add('TargetId', ItemVariant."Item No." + i95Setup."i95 Item Variant Seperator" + ItemVariant.Code);
        RequestDataJsonArr.Add(PushDatajsonObj);
        BodyContentjsonObj.WriteTo(BodyContent);
    end;
}