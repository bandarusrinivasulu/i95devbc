Codeunit 70328079 "i95 Process Response Api"
{
    Permissions = tabledata "Sales Shipment Header" = rm, tabledata "Sales Shipment Line" = rm, tabledata "Sales Invoice Header" = rm, tabledata "Sales Invoice Line" = rm;

    var
        i95SyncLogEntry: Record "i95 Sync Log Entry";
        i95PullWebservice: Codeunit "i95 Pull Webservice";
        i95PushWebService: Codeunit "i95 Push Webservice";

    trigger OnRun()
    begin
        with i95SyncLogEntry do begin

            //Sync Source = i95
            Reset();
            SetCurrentKey("Sync Source", "API Type", "Sync Status");
            SetRange("Sync Source", "Sync Source"::i95);
            SetFilter("Waiting For Response", '<>%1', 0);
            If FindSet() then
                repeat
                    i95PullWebservice.SetSynclogEntryNo("Entry No");
                    i95PullWebservice.ProcessPushResponse("API Type");
                until i95SyncLogEntry.next() = 0;

            //Sync Source = Business Central
            Reset();
            SetCurrentKey("Sync Source", "API Type", "Sync Status");
            SetRange("Sync Source", "Sync Source"::"Business Central");
            SetFilter("Waiting For Response", '<>%1', 0);
            If FindSet() then
                repeat
                    i95PushWebService.SetSynclogEntryNo("Entry No");
                    i95PushWebService.ProcessPullResponse("API Type");
                until i95SyncLogEntry.next() = 0;
        end;
    end;

}