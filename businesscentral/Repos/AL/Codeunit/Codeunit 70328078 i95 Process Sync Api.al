Codeunit 70328078 "i95 Process Sync Api"
{
    Permissions = tabledata "Sales Shipment Header" = rm, tabledata "Sales Shipment Line" = rm, tabledata "Sales Invoice Header" = rm, tabledata "Sales Invoice Line" = rm;

    var
        i95PushWebService: Codeunit "i95 Push Webservice";
        i95PullWebservice: Codeunit "i95 Pull Webservice";
        CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID;

    trigger OnRun()
    begin
        i95PullWebservice.ProcessPullData(CurrentAPIType::SchedulerID);

        //PushData
        ProductPushData();
        InventoryPushData();
        SalesPricePushData();
        SalesShipmentPushData();
        SalesInvoicePushData();
        CustomerPushData();
        CustomerGroupPushData();
        SalesOrderPushData();
        CancelSalesOrderPushData();
        TaxBusPostingGroupPushData();
        TaxProdPostingGroupPushData();
        TaxPostingSetupPushData();
        CustDiscountGroupPushData();
        ItemDiscountGroupPushData();
        DiscountPricePushData();
        ItemVariantPushData();
        EditSalesOrderPushData();

        //PullData
        i95PullWebservice.ProcessPullData(CurrentAPIType::Customer);
        i95PullWebservice.ProcessPullData(CurrentAPIType::CustomerGroup);
        i95PullWebservice.ProcessPullData(CurrentAPIType::SalesOrder);
        i95PullWebservice.ProcessPullData(CurrentAPIType::Product);
    end;

    procedure ProductPushData()
    var
        Item: Record Item;
        ItemVariant: Record "Item Variant";
    begin
        Item.SetCurrentKey("i95 Sync Status");
        item.SetRange(item."i95 Sync Status", item."i95 Sync Status"::"Waiting for Sync");
        If Item.findset() then begin
            ItemVariant.Reset();
            ItemVariant.SetRange(ItemVariant."Item No.", Item."No.");
            If ItemVariant.IsEmpty() then
                i95PushWebService.ProductPushData(Item)
            else begin
                i95PushWebService.ConfigurableProductPushData(Item);
                i95PushWebService.ChildProductPushData(Item);
            end;
        end;
    end;

    procedure InventoryPushData()
    var
        Item: Record Item;
        ItemVariant: Record "Item Variant";
    begin
        Item.SetCurrentKey("i95 Inventory Sync Status");
        item.SetRange(item."i95 Inventory Sync Status", item."i95 Inventory Sync Status"::"Waiting for Sync");
        If Item.findset() then
            i95PushWebService.InventoryPushData(Item);

        ItemVariant.SetCurrentKey("i95 Inventory Sync Status");
        ItemVariant.SetRange("i95 Inventory Sync Status", ItemVariant."i95 Inventory Sync Status"::"Waiting for Sync");
        if ItemVariant.FindSet() then
            i95PushWebService.VariantInventoryPushData(ItemVariant);
    end;

    procedure CustomerPushData()
    var
        Customer: Record customer;
    begin
        Customer.SetCurrentKey("i95 Sync Status");
        Customer.SetRange(Customer."i95 Sync Status", Customer."i95 Sync Status"::"Waiting for Sync");
        If Customer.FindSet() then
            i95PushWebService.CustomerPushData(Customer);
    end;

    procedure CustomerGroupPushData()
    var
        CustPriceGroup: Record "Customer Price Group";
    begin
        CustPriceGroup.SetCurrentKey("i95 Sync Status");
        CustPriceGroup.SetRange(CustPriceGroup."i95 Sync Status", CustPriceGroup."i95 Sync Status"::"Waiting for Sync");
        if CustPriceGroup.FindSet() then
            i95PushWebService.CustomerPriceGroupPushData(CustPriceGroup);
    end;

    procedure SalesPricePushData()
    var
        Item: Record Item;
    begin
        Item.SetCurrentKey("i95 SalesPrice Sync Status");
        Item.SetRange(Item."i95 SalesPrice Sync Status", Item."i95 SalesPrice Sync Status"::"Waiting for Sync");
        If Item.FindSet() then
            i95PushWebService.SalesPricePushData(Item);
    end;

    procedure SalesOrderPushData()
    var
        SalesHeader: Record "Sales Header";
    begin
        SalesHeader.SetCurrentKey("i95 Sync Status");
        SalesHeader.SetRange(SalesHeader."Document Type", SalesHeader."Document Type"::Order);
        SalesHeader.SetRange(SalesHeader."i95 Sync Status", SalesHeader."i95 Sync Status"::"Waiting for Sync");
        If SalesHeader.FindSet() then
            i95PushWebService.SalesOrderPushData(SalesHeader);
    end;

    procedure SalesShipmentPushData()
    var
        SalesShipmentHeader: Record "Sales Shipment Header";
    begin
        SalesShipmentHeader.SetCurrentKey("i95 Sync Status");
        SalesShipmentHeader.SetRange(SalesShipmentHeader."i95 Sync Status", SalesShipmentHeader."i95 Sync Status"::"Waiting for Sync");
        If SalesShipmentHeader.FindSet() then
            i95PushWebService.SalesShipmentPushData(SalesShipmentHeader);
    end;

    procedure SalesInvoicePushData()
    var
        SalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        SalesInvoiceHeader.SetCurrentKey("i95 Sync Status");
        SalesInvoiceHeader.SetRange(SalesInvoiceHeader."i95 Sync Status", SalesInvoiceHeader."i95 Sync Status"::"Waiting for Sync");
        If SalesInvoiceHeader.FindSet() then
            i95PushWebService.SalesInvoicePushData(SalesInvoiceHeader);
    end;

    procedure CancelSalesOrderPushData()
    var
        i95SyncLogEntry: Record "i95 Sync Log Entry";
    begin
        i95SyncLogEntry.Reset();
        i95SyncLogEntry.SetCurrentKey(i95SyncLogEntry."Sync Source", i95SyncLogEntry."API Type", i95SyncLogEntry."Sync Status");
        i95SyncLogEntry.SetRange(i95SyncLogEntry."Sync Source", i95SyncLogEntry."Sync Source"::"Business Central");
        i95SyncLogEntry.SetRange(i95SyncLogEntry."API Type", i95SyncLogEntry."API Type"::CancelOrder);
        i95SyncLogEntry.SetRange(i95SyncLogEntry."Sync Status", i95SyncLogEntry."Sync Status"::"Waiting for Sync");
        if i95SyncLogEntry.FindSet() then
            i95PushWebService.CancelSalesOrderPushData(i95SyncLogEntry);
    end;

    procedure TaxBusPostingGroupPushData()
    var
        TaxBusPostingGrp: Record "VAT Business Posting Group";
    begin
        TaxBusPostingGrp.Reset();
        TaxBusPostingGrp.SetCurrentKey("i95 Sync Status");
        TaxBusPostingGrp.SetRange(TaxBusPostingGrp."i95 Sync Status", TaxBusPostingGrp."i95 Sync Status"::"Waiting for Sync");
        If TaxBusPostingGrp.FindSet() then
            i95PushWebService.TaxBusPostingGrpPushData(TaxBusPostingGrp);
    end;

    procedure TaxProdPostingGroupPushData()
    var
        TaxProdPostingGrp: Record "VAT Product Posting Group";
    begin
        TaxProdPostingGrp.Reset();
        TaxProdPostingGrp.SetCurrentKey("i95 Sync Status");
        TaxProdPostingGrp.SetRange(TaxProdPostingGrp."i95 Sync Status", TaxProdPostingGrp."i95 Sync Status"::"Waiting for Sync");
        if TaxProdPostingGrp.FindSet() then
            i95PushWebService.TaxProdPostingGrpPushData(TaxProdPostingGrp);
    end;

    procedure TaxPostingSetupPushData()
    var
        TaxPostingSetup: Record "VAT Posting Setup";
    begin
        TaxPostingSetup.Reset();
        TaxPostingSetup.SetCurrentKey(TaxPostingSetup."i95 Sync Status");
        TaxPostingSetup.SetRange(TaxPostingSetup."i95 Sync Status", TaxPostingSetup."i95 Sync Status"::"Waiting for Sync");
        if TaxPostingSetup.FindSet() then
            i95PushWebService.TaxPostingSetupPushData(TaxPostingSetup);
    end;

    procedure CustDiscountGroupPushData()
    var
        CustDiscountGroup: Record "Customer Discount Group";
    begin
        CustDiscountGroup.Reset();
        CustDiscountGroup.SetCurrentKey(CustDiscountGroup."i95 Sync Status");
        CustDiscountGroup.SetRange(CustDiscountGroup."i95 Sync Status", CustDiscountGroup."i95 Sync Status"::"Waiting for Sync");
        if CustDiscountGroup.FindSet() then
            i95PushWebService.CustomerDiscGroupPushData(CustDiscountGroup);
    end;

    procedure ItemDiscountGroupPushData()
    var
        ItemDiscountGroup: Record "Item Discount Group";
    begin
        ItemDiscountGroup.Reset();
        ItemDiscountGroup.SetCurrentKey(ItemDiscountGroup."i95 Sync Status");
        ItemDiscountGroup.SetRange(ItemDiscountGroup."i95 Sync Status", ItemDiscountGroup."i95 Sync Status"::"Waiting for Sync");
        if ItemDiscountGroup.FindSet() then
            i95PushWebService.ItemDiscGroupPushData(ItemDiscountGroup);
    end;

    procedure DiscountPricePushData()
    var
        Item: Record item;
        ItemDiscGroup: Record "Item Discount Group";
    begin
        Item.SetCurrentKey("i95 DiscountPrice Sync Status");
        Item.SetRange(Item."i95 DiscountPrice Sync Status", Item."i95 DiscountPrice Sync Status"::"Waiting for Sync");
        If Item.FindSet() then
            i95PushWebService.DiscountPricePushData(Item);

        ItemDiscGroup.SetCurrentKey("i95 DiscountPrice Sync Status");
        ItemDiscGroup.SetRange(ItemDiscGroup."i95 DiscountPrice Sync Status", ItemDiscGroup."i95 DiscountPrice Sync Status"::"Waiting for Sync");
        if ItemDiscGroup.FindSet() then
            i95PushWebService.DiscountPriceByItemDiscGrpPushData(ItemDiscGroup);
    end;

    procedure ItemVariantPushData()
    var
        Item: Record item;
    begin
        Item.SetCurrentKey(Item."i95 ItemVariant Sync Status");
        Item.SetRange(item."i95 ItemVariant Sync Status", item."i95 ItemVariant Sync Status"::"Waiting for Sync");
        If Item.Findset() then
            i95PushWebService.ConfigurableProductPushData(Item);

        item.Reset();
        Item.SetCurrentKey(Item."i95 Child Variant Sync Status");
        Item.SetRange(item."i95 Child Variant Sync Status", Item."i95 Child Variant Sync Status"::"Waiting for Sync");
        If Item.Findset() then
            i95PushWebService.ChildProductPushData(Item);
    end;

    procedure EditSalesOrderPushData()
    var
        SalesHeader: Record "Sales Header";
    begin
        SalesHeader.SetCurrentKey("i95 EditOrder Sync Status");
        SalesHeader.SetRange(SalesHeader."Document Type", SalesHeader."Document Type"::Order);
        SalesHeader.SetRange(SalesHeader."i95 EditOrder Sync Status", SalesHeader."i95 EditOrder Sync Status"::"Waiting for Sync");
        If SalesHeader.FindSet() then
            i95PushWebService.EditSalesOrderPushData(SalesHeader);
    end;
}