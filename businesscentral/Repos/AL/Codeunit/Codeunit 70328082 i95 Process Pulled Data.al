codeunit 70328082 "i95 Process Pulled Data"
{
    trigger OnRun()
    var
        i95Setup: Record "i95 Setup";
    begin
        i95Setup.get();
        i95Setup.TestField(i95Setup."i95 Customer Posting Group");
        i95Setup.TestField(i95Setup."i95 Gen. Bus. Posting Group");
        i95Setup.TestField(i95Setup."Default Guest Customer No.");

        with i95SyncLogEntry do begin
            Reset();
            SetRange("Sync Source", "Sync Source"::i95);
            SetRange("Sync Status", "Sync Status"::"Waiting for Sync");  //Need to check this field usage
            SetRange("PullData Status", "PullData Status"::"Data Received");
            if i95SyncLogEntryNo <> 0 then
                SetRange("Entry No", i95SyncLogEntryNo);
            If FindSet() then
                repeat
                    Clear(i95UpdateDataCU);
                    Commit();
                    i95UpdateDataCU.SetParamaters(i95SyncLogEntry."Entry No");
                    if not i95UpdateDataCU.Run() then begin
                        i95SyncLogEntry."Error Message" := copystr(GetLastErrorText(), 1, 300);
                        i95SyncLogEntry."Log Status" := i95SyncLogEntry."Log Status"::Error;
                        i95SyncLogEntry."Status ID" := i95SyncLogEntry."Status ID"::Error;
                        i95SyncLogEntry.modify();
                    end;
                until i95SyncLogEntry.next() = 0;
        end;
    end;

    procedure SetParamaters(EntryNo: Integer)
    begin
        i95SyncLogEntryNo := EntryNo;
    end;

    var
        i95SyncLogEntry: Record "i95 Sync Log Entry";
        i95UpdateDataCU: Codeunit "i95 Update Data to BC";
        i95SyncLogEntryNo: Integer;
}