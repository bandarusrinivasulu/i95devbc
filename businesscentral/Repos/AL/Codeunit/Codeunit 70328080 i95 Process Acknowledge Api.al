Codeunit 70328080 "i95 Process Acknowledge Api"
{
    Permissions = tabledata "Sales Shipment Header" = rm, tabledata "Sales Shipment Line" = rm, tabledata "Sales Invoice Header" = rm, tabledata "Sales Invoice Line" = rm;

    var
        i95SyncLogEntry: Record "i95 Sync Log Entry";
        i95PushWebService: Codeunit "i95 Push Webservice";

    trigger OnRun()
    begin
        with i95SyncLogEntry do begin
            //Sync Source = Business Central
            Reset();
            SetCurrentKey("Sync Source", "API Type", "Sync Status");
            SetRange("Sync Source", "Sync Source"::"Business Central");
            Setfilter("Waiting For Acknowledgement", '<>%1', 0);
            If FindSet() then
                repeat
                    i95PushWebService.SetSynclogEntryNo("Entry No");
                    i95PushWebService.ProcessPullResponseAcknowledgment("API Type");
                until i95SyncLogEntry.next() = 0;
        end;
    end;

}