Tableextension 70328076 "i95 Sales Price Ext" extends "Sales Price"
{
    fields
    {
        field(70328075; "i95 Created By"; Code[80])
        {
            DataClassification = CustomerContent;
            Caption = 'Created By';
            Editable = false;
        }
        field(70328076; "i95 Created DateTime"; DateTime)
        {
            DataClassification = CustomerContent;
            Caption = 'Created DateTime';
            Editable = false;
        }

        field(70328077; "i95 Creation Source"; Option)
        {
            DataClassification = CustomerContent;
            Caption = 'Creation Source';
            OptionMembers = " ","Business Central","i95";
            OptionCaption = ' ,"Business Central","i95Dev"';
            Editable = false;
        }
        field(70328078; "i95 Last Modified By"; Code[80])
        {
            DataClassification = CustomerContent;
            Caption = 'Last Modified By';
            Editable = false;
        }
        field(70328079; "i95 Last Modification DateTime"; DateTime)
        {
            DataClassification = CustomerContent;
            Caption = 'Last Modification DateTime';
            Editable = false;
        }
        field(70328080; "i95 Last Modification Source"; Option)
        {
            DataClassification = CustomerContent;
            OptionMembers = " ","Business Central","i95";
            OptionCaption = ' ,"Business Central","i95Dev"';
            Caption = 'Last Modification Source';
            Editable = false;
        }
    }

    trigger OnBeforeInsert()
    var
        Item: Record Item;
    begin
        If "Sales Type" = "Sales Type"::"Customer Price Group" then begin
            "i95 Created By" := copystr(UserId(), 1, 80);
            "i95 Created DateTime" := CurrentDateTime();
            "i95 Creation Source" := "i95 Creation Source"::"Business Central";
            If i95MandatoryFieldsUpdated() then
                If item.get(Rec."Item No.") then begin
                    Item."i95 SalesPrice Sync Status" := Item."i95 SalesPrice Sync Status"::"Waiting for Sync";
                    Item."i95 SP Last Updated DateTime" := CurrentDateTime();
                    Item.Modify(false);
                end;
        end;
    end;

    trigger OnBeforeModify()
    var
        Item: Record Item;
    Begin
        If ("Sales Type" = "Sales Type"::"Customer Price Group") and (not UpdatedFromi95) and i95MandatoryFieldsUpdated() then begin
            "i95 Last Modified By" := copystr(UserId(), 1, 80);
            "i95 Last Modification DateTime" := CurrentDateTime();
            "i95 Last Modification Source" := "i95 Last Modification Source"::"Business Central";
            if Item.Get(Rec."Item No.") then begin
                Item."i95 SP Last Updated DateTime" := CurrentDateTime();
                item."i95 SalesPrice Sync Status" := Item."i95 SalesPrice Sync Status"::"Waiting for Sync";
                Item."i95 SP Last Updated DateTime" := CurrentDateTime();
                Item.Modify(false);
            end;
        end;
    End;

    trigger OnBeforeDelete()
    var
        Item: Record Item;
    Begin
        If "Sales Type" = "Sales Type"::"Customer Price Group" then
            if Item.Get(Rec."Item No.") then begin
                Item."i95 SP Last Updated DateTime" := CurrentDateTime();
                item."i95 SalesPrice Sync Status" := Item."i95 SalesPrice Sync Status"::"Waiting for Sync";
                Item."i95 SP Last Updated DateTime" := CurrentDateTime();
                Item.Modify(false);
            end;
    End;

    trigger OnBeforeRename()
    var
        Item: Record Item;
    Begin
        If ("Sales Type" = "Sales Type"::"Customer Price Group") and (not UpdatedFromi95) and i95MandatoryFieldsUpdated() then begin
            "i95 Last Modified By" := copystr(UserId(), 1, 80);
            "i95 Last Modification DateTime" := CurrentDateTime();
            "i95 Last Modification Source" := "i95 Last Modification Source"::"Business Central";

            if xRec."Item No." <> Rec."Item No." then
                if Item.Get(xRec."Item No.") then begin
                    Item."i95 SP Last Updated DateTime" := CurrentDateTime();
                    item."i95 SalesPrice Sync Status" := Item."i95 SalesPrice Sync Status"::"Waiting for Sync";
                    Item."i95 SP Last Updated DateTime" := CurrentDateTime();
                    Item.Modify(false);
                end;

            if Item.Get(Rec."Item No.") then begin
                Item."i95 SP Last Updated DateTime" := CurrentDateTime();
                item."i95 SalesPrice Sync Status" := Item."i95 SalesPrice Sync Status"::"Waiting for Sync";
                Item."i95 SP Last Updated DateTime" := CurrentDateTime();
                Item.Modify(false);
            end;
        end;
    End;

    procedure i95MandatoryFieldsUpdated(): Boolean
    begin
        If (Rec."Sales Code" = '') or (Rec."Item No." = '') or (rec."Unit Price" = 0) then
            exit(false)
        else
            exit(true);
    end;

    procedure i95SetAPIUpdateCall(APICall: Boolean)
    begin
        UpdatedFromi95 := APICall;
    end;

    var
        UpdatedFromi95: Boolean;
}