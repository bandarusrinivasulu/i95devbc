page 70328079 "i95 Test API Call"
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    Caption = 'i95Dev Test API Call';
    layout
    {
        area(Content)
        {

        }
        area(Factboxes)
        {

        }
    }

    actions
    {
        area(Processing)
        {
            group("PushData")
            {
                action("Process SalesInvoice PushData API")
                {
                    Image = SalesInvoice;
                    Caption = 'Sales Invoice';
                    ToolTip = 'Process SalesInvoice PushData API';
                    ApplicationArea = All;

                    trigger OnAction();
                    var
                        SalesInvoiceHeader: Record "Sales Invoice Header";
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        SalesInvoiceHeader.reset();
                        SalesInvoiceHeader.SetCurrentKey(SalesInvoiceHeader."i95 Sync Status");
                        SalesInvoiceHeader.SetRange("i95 Sync Status", SalesInvoiceHeader."i95 Sync Status"::"Waiting for Sync");
                        If SalesInvoiceHeader.FindSet() then
                            i95PushWebservice.SalesInvoicePushData(SalesInvoiceHeader);
                    end;
                }
                action("Process SalesShipment PushData API")
                {
                    Image = SalesShipment;
                    Caption = 'Sales Shipment';
                    ToolTip = 'Process SalesShipment PushData API';
                    ApplicationArea = all;
                    trigger OnAction();
                    var
                        SalesShipmentHeader: Record "Sales Shipment Header";
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        SalesShipmentHeader.Reset();
                        SalesShipmentHeader.SetCurrentKey(SalesShipmentHeader."i95 Sync Status");
                        SalesShipmentHeader.SetRange("i95 Sync Status", SalesShipmentHeader."i95 Sync Status"::"Waiting for Sync");
                        if SalesShipmentHeader.Findset() then
                            i95PushWebservice.SalesShipmentPushData(SalesShipmentHeader);
                    end;
                }
                action("Process Inventory PushData API")
                {
                    Image = Inventory;
                    Caption = 'Inventory';
                    ToolTip = 'Process Inventory PushData API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        Item: Record Item;
                        ItemVariant: Record "Item Variant";
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        Item.SetCurrentKey(Item."i95 Inventory Sync Status");
                        Item.SetRange(Item."i95 Inventory Sync Status", item."i95 Inventory Sync Status"::"Waiting for Sync");
                        If Item.Findset() then
                            i95PushWebservice.InventoryPushData(Item);

                        ItemVariant.SetCurrentKey("i95 Inventory Sync Status");
                        ItemVariant.SetRange("i95 Inventory Sync Status", ItemVariant."i95 Inventory Sync Status"::"Waiting for Sync");
                        if ItemVariant.FindSet() then
                            i95PushWebService.VariantInventoryPushData(ItemVariant);
                    end;
                }
                action("Process Sales Price PushData API")
                {
                    Image = SalesPrices;
                    Caption = 'Sales Price';
                    ToolTip = 'Process Sales Price PushData API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        Item: Record Item;
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        Item.Reset();
                        Item.SetCurrentKey(Item."i95 SalesPrice Sync Status");
                        Item.SetRange(Item."i95 SalesPrice Sync Status", Item."i95 SalesPrice Sync Status"::"Waiting for Sync");
                        if Item.FindSet() then
                            repeat
                                i95PushWebservice.SalesPricePushData(Item);
                            until Item.Next() = 0;
                    end;
                }
                action("Process Product PushData API")
                {
                    Image = Item;
                    Caption = 'Product';
                    ToolTip = 'Process Product PushData API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        Item: Record Item;
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        Item.SetCurrentKey(Item."i95 Sync Status");
                        Item.SetRange("i95 Sync Status", item."i95 Sync Status"::"Waiting for Sync");
                        If Item.Findset() then
                            i95PushWebservice.ProductPushData(Item);
                    end;
                }
                action(CustomerPushDataTest)
                {
                    Image = Customer;
                    Caption = 'Customer';
                    ToolTip = 'Process Customer PushData API';
                    ApplicationArea = all;
                    trigger OnAction();
                    var
                        Customer: Record customer;
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        Customer.SetCurrentKey(Customer."i95 Sync Status");
                        Customer.SetRange("i95 Sync Status", Customer."i95 Sync Status"::"Waiting for Sync");
                        If Customer.FindSet() then
                            i95PushWebservice.CustomerPushData(customer);
                    end;
                }

                action(CustomerGrpPushDataTest)
                {
                    Image = CustomerGroup;
                    Caption = 'Customer Price Group';
                    ToolTip = 'Process Customer Price Group PushData API';
                    ApplicationArea = all;
                    trigger OnAction();
                    var
                        CustPriceGrp: Record "Customer Price Group";
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        CustPriceGrp.SetCurrentKey(CustPriceGrp."i95 Sync Status");
                        CustPriceGrp.SetRange("i95 Sync Status", CustPriceGrp."i95 Sync Status"::"Waiting for Sync");
                        If CustPriceGrp.FindSet() then
                            i95PushWebservice.CustomerPriceGroupPushData(CustPriceGrp);
                    end;
                }
                action(SalesOrderPushDataTest)
                {
                    Image = Order;
                    Caption = 'Sales Order';
                    ToolTip = 'Process Sales Order PushData API';
                    ApplicationArea = all;
                    trigger OnAction();
                    var
                        SalHdr: Record "Sales Header";
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        SalHdr.SetCurrentKey(SalHdr."i95 Sync Status");
                        SalHdr.SetRange("Document Type", SalHdr."Document Type"::Order);
                        SalHdr.SetRange("i95 Sync Status", SalHdr."i95 Sync Status"::"Waiting for Sync");
                        If SalHdr.FindSet() then
                            i95PushWebservice.SalesOrderPushData(SalHdr);
                    end;
                }
                action(CancelSalesOrderPushDataTest)
                {
                    Image = Cancel;
                    Caption = 'Cancel Sales Order';
                    ToolTip = 'Process Cancelled Sales Order PushData API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        i95SyncLogEntry: Record "i95 Sync Log Entry";
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        i95SyncLogEntry.Reset();
                        i95SyncLogEntry.SetCurrentKey("Sync Source", "API Type", "Sync Status");
                        i95SyncLogEntry.SetRange(i95SyncLogEntry."Sync Source", i95SyncLogEntry."Sync Source"::"Business Central");
                        i95SyncLogEntry.SetRange(i95SyncLogEntry."API Type", i95SyncLogEntry."API Type"::CancelOrder);
                        i95SyncLogEntry.SetRange(i95SyncLogEntry."Sync Status", i95SyncLogEntry."Sync Status"::"Waiting for Sync");
                        if i95SyncLogEntry.FindSet() then
                            i95PushWebService.CancelSalesOrderPushData(i95SyncLogEntry);
                    end;
                }
                action(TaxBusPostingGrpPushDataTest)
                {
                    Image = TaxDetail;
                    Caption = 'Tax Business Posting Group';
                    ToolTip = 'Process Tax Business Posting Group PushData API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        TaxBusPostingGrp: Record "VAT Business Posting Group";
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        TaxBusPostingGrp.Reset();
                        TaxBusPostingGrp.SetCurrentKey("i95 Sync Status");
                        TaxBusPostingGrp.SetRange(TaxBusPostingGrp."i95 Sync Status", TaxBusPostingGrp."i95 Sync Status"::"Waiting for Sync");
                        If TaxBusPostingGrp.FindSet() then
                            i95PushWebService.TaxBusPostingGrpPushData(TaxBusPostingGrp);
                    end;
                }
                action(TaxProdPostingGrpPushDataTest)
                {
                    Image = TaxPayment;
                    Caption = 'Tax Product Posting Group';
                    ToolTip = 'Process Tax Product Posting Group PushData API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        TaxProdPostingGrp: Record "VAT Product Posting Group";
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        TaxProdPostingGrp.Reset();
                        TaxProdPostingGrp.SetCurrentKey("i95 Sync Status");
                        TaxProdPostingGrp.SetRange(TaxProdPostingGrp."i95 Sync Status", TaxProdPostingGrp."i95 Sync Status"::"Waiting for Sync");
                        If TaxProdPostingGrp.FindSet() then
                            i95PushWebService.TaxProdPostingGrpPushData(TaxProdPostingGrp);
                    end;
                }
                action(TaxPostingSetupPushDataTest)
                {
                    Image = TaxSetup;
                    Caption = 'Tax Posting Setup';
                    ToolTip = ' Process Tax Posting Setup PushData API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        TaxPostingSetup: Record "VAT Posting Setup";
                        i95PushWebservice: Codeunit "i95 Push Webservice";
                    begin
                        TaxPostingSetup.Reset();
                        TaxPostingSetup.SetCurrentKey(TaxPostingSetup."i95 Sync Status");
                        TaxPostingSetup.SetRange(TaxPostingSetup."i95 Sync Status", TaxPostingSetup."i95 Sync Status"::"Waiting for Sync");
                        if TaxPostingSetup.FindSet() then
                            i95PushWebService.TaxPostingSetupPushData(TaxPostingSetup);
                    end;
                }
                action(CustDiscountGroupPushDataTest)
                {
                    Image = Discount;
                    Caption = 'Customer Discount Group';
                    ToolTip = 'Process Customer Discount Group PushData API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        CustDiscountGroup: Record "Customer Discount Group";
                        i95PushWebService: Codeunit "i95 Push Webservice";
                    begin
                        CustDiscountGroup.Reset();
                        CustDiscountGroup.SetCurrentKey(CustDiscountGroup."i95 Sync Status");
                        CustDiscountGroup.SetRange(CustDiscountGroup."i95 Sync Status", CustDiscountGroup."i95 Sync Status"::"Waiting for Sync");
                        if CustDiscountGroup.FindSet() then
                            i95PushWebService.CustomerDiscGroupPushData(CustDiscountGroup);
                    end;
                }
                action(ItemDiscountGroupPushDataTest)
                {
                    Image = ItemCosts;
                    Caption = 'Item Discount Group';
                    ToolTip = 'Process Item Discount Group PushData API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        ItemDiscountGroup: Record "Item Discount Group";
                        i95PushWebService: Codeunit "i95 Push Webservice";
                    begin
                        ItemDiscountGroup.Reset();
                        ItemDiscountGroup.SetCurrentKey(ItemDiscountGroup."i95 Sync Status");
                        ItemDiscountGroup.SetRange(ItemDiscountGroup."i95 Sync Status", ItemDiscountGroup."i95 Sync Status"::"Waiting for Sync");
                        if ItemDiscountGroup.FindSet() then
                            i95PushWebService.ItemDiscGroupPushData(ItemDiscountGroup);
                    end;
                }
                action(DiscountPricePushDataTest)
                {
                    Image = SalesLineDisc;
                    Caption = 'Discount Price';
                    ToolTip = 'Process Discount Price PushData API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        Item: Record item;
                        ItemDiscGroup: Record "Item Discount Group";
                        i95PushWebService: Codeunit "i95 Push Webservice";
                    begin
                        Item.SetCurrentKey("i95 DiscountPrice Sync Status");
                        Item.SetRange(Item."i95 DiscountPrice Sync Status", Item."i95 DiscountPrice Sync Status"::"Waiting for Sync");
                        If Item.FindSet() then
                            i95PushWebService.DiscountPricePushData(Item);

                        ItemDiscGroup.SetCurrentKey("i95 DiscountPrice Sync Status");
                        ItemDiscGroup.SetRange(ItemDiscGroup."i95 DiscountPrice Sync Status", ItemDiscGroup."i95 DiscountPrice Sync Status"::"Waiting for Sync");
                        if ItemDiscGroup.FindSet() then
                            i95PushWebService.DiscountPriceByItemDiscGrpPushData(ItemDiscGroup);
                    end;
                }
                action(ItemVariantPushDataTest)
                {
                    Image = ItemVariant;
                    Caption = 'Item Variant';
                    ToolTip = 'Process Item Variant PushData API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        Item: Record item;
                        i95PushWebService: Codeunit "i95 Push Webservice";
                    begin
                        Item.SetCurrentKey(Item."i95 ItemVariant Sync Status");
                        Item.SetRange(item."i95 ItemVariant Sync Status", item."i95 ItemVariant Sync Status"::"Waiting for Sync");
                        If Item.Findset() then
                            i95PushWebService.ConfigurableProductPushData(Item);

                        item.Reset();
                        Item.SetCurrentKey(Item."i95 Child Variant Sync Status");
                        Item.SetRange(item."i95 Child Variant Sync Status", item."i95 Child Variant Sync Status"::"Waiting for Sync");
                        If Item.Findset() then
                            i95PushWebService.ChildProductPushData(Item);
                    end;
                }
                action(EditSalesOrderPushDataTest)
                {
                    Image = EditJournal;
                    Caption = 'Edit Sales Order';
                    ToolTip = 'Process Edit Sales Order Pushdata API';
                    ApplicationArea = All;
                    trigger OnAction();
                    var
                        SalesHeader: Record "Sales Header";
                        i95PushWebService: Codeunit "i95 Push Webservice";
                    begin
                        SalesHeader.SetCurrentKey("i95 EditOrder Sync Status");
                        SalesHeader.SetRange(SalesHeader."Document Type", SalesHeader."Document Type"::Order);
                        SalesHeader.SetRange(SalesHeader."i95 EditOrder Sync Status", SalesHeader."i95 EditOrder Sync Status"::"Waiting for Sync");
                        If SalesHeader.FindSet() then
                            i95PushWebService.EditSalesOrderPushData(SalesHeader);
                    end;
                }
            }
            group("PullData")
            {
                action(CustomerPullDataTest)
                {
                    Image = Customer;
                    Caption = 'Customer';
                    ToolTip = 'Process Customer PullData API';
                    ApplicationArea = all;
                    trigger OnAction();
                    var
                        i95PullWebservice: Codeunit "i95 Pull Webservice";
                    begin
                        i95PullWebservice.ProcessPullData(CurrentAPIType::Customer);
                    end;
                }
                action(CustomerGroupPullDataTest)
                {
                    Image = CustomerGroup;
                    Caption = 'Customer Price Group';
                    ToolTip = 'Process Customer Price Group PullData API';
                    ApplicationArea = all;
                    trigger OnAction();
                    var
                        i95PullWebservice: Codeunit "i95 Pull Webservice";
                    begin
                        i95PullWebservice.ProcessPullData(CurrentAPIType::CustomerGroup);
                    end;
                }
                action(SalesOrderPullDataTest)
                {
                    Image = Order;
                    Caption = 'Sales Order';
                    ToolTip = 'Process SalesOrder PullData API';
                    ApplicationArea = all;
                    trigger OnAction();
                    var
                        i95PullWebservice: Codeunit "i95 Pull Webservice";
                    begin
                        i95PullWebservice.ProcessPullData(CurrentAPIType::SalesOrder);
                    end;
                }
                action(ProductPullDataTest)
                {
                    Image = Item;
                    Caption = 'Product';
                    ToolTip = 'Process Product PullData API';
                    ApplicationArea = all;
                    trigger OnAction();
                    var
                        i95PullWebservice: Codeunit "i95 Pull Webservice";
                    begin
                        i95PullWebservice.ProcessPullData(CurrentAPIType::Product);
                    end;
                }
                action(SchedulerIDPullDataTest)
                {
                    Image = Order;
                    Caption = 'Scheduler ID';
                    ApplicationArea = all;
                    trigger OnAction();
                    var
                        i95PullWebservice: Codeunit "i95 Pull Webservice";
                    begin
                        i95PullWebservice.ProcessPullData(CurrentAPIType::SchedulerID);
                    end;
                }
            }
        }
    }
    var
        CurrentAPIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerID;
}