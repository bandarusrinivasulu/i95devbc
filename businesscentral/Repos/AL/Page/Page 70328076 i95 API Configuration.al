Page 70328076 "i95 API Configuration"
{
    Caption = 'i95Dev API Configuration';
    PageType = List;
    SourceTable = "i95 API Configuration";
    UsageCategory = Lists;
    ApplicationArea = All;

    layout
    {
        area(Content)
        {
            repeater("i95 API Configuration")
            {
                field("API Type"; "API Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Type of i95Dev Api';
                }
                field(Description; Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Description';
                }
                field("Request Type"; "Request Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Type of Request';
                }
                field("PushData Url"; "PushData Url")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Push Data Url';
                }
                field("PullResponse Url"; "PullResponse Url")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Pull Response Url';
                }
                field("PullResponseAck Url"; "PullResponseAck Url")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Pull Response Acknowledgement Url';
                }
                field("PullData Url"; "PullData Url")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Pull Data Url';
                }
                field("PushResponse Url"; "PushResponse Url")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Push Response Url';
                }
            }
        }
    }
}
