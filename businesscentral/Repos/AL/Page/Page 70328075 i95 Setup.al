Page 70328075 "i95 Setup"
{
    Caption = 'i95Dev Setup';
    PageType = Card;
    SourceTable = "i95 Setup";
    UsageCategory = Lists;
    ApplicationArea = All;

    layout
    {
        area(Content)
        {
            group(General)
            {
                field("Base Url"; "Base Url")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Base i95Dev Url';
                    trigger OnValidate()
                    begin
                        if ("Base Url" <> '') and (CopyStr("Base Url", STRLEN("Base Url"), 1) <> '/') then
                            "Base Url" := copystr("Base Url", 1, 149) + '/';
                    End;
                }
                field("Subscription Key"; "Subscription Key")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the i95Dev Subscription Key';
                }
                field("Client ID"; "Client ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Client ID';
                }
                field(Authorization; APIAuthorizationToken)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the API Authorization';
                    MultiLine = true;
                    Editable = AuthTokenEditable;
                    trigger OnValidate()
                    begin
                        SetAuthorizationToken(APIAuthorizationToken);
                    end;
                }
                field("Instance Type"; "Instance Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the type of Instance';
                }
                field("Endpoint Code"; "Endpoint Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Endpoint code';
                }
                field("Content Type"; "Content Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the type of Content';
                }
                field(PullDataPacketSize; "Pull Data Packet Size")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Pull Data Packet Size';
                }
                field("i95 Default Warehouse"; "i95 Default Warehouse")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the i95 Default Warehouse';
                }
                field("Default Guest Customer No."; "Default Guest Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Default Guest Customer No.';
                }
                field("i95 Customer Posting Group"; "i95 Customer Posting Group")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the i95 Customer Posting Group';
                }
                field("i95 Gen. Bus. Posting Group"; "i95 Gen. Bus. Posting Group")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the i95 General Business Posting Group';
                }
                field("Default UOM"; "Default UOM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Default Unit Of Measure';
                }
                field("i95 Gen. Prod. Posting Group"; "i95 Gen. Prod. Posting Group")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Default Gen. Prod. Posting Group';
                }
                field("i95 Inventory Posting Group"; "i95 Inventory Posting Group")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Default Inventory Posting Group';
                }
                field("i95 Tax Group Code"; "i95 Tax Group Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Default Tax Group Code';
                }
                field("i95 Shipping Charge G/L Acc"; "i95 Shipping Charge G/L Acc")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Shipping Charge G/L Account';
                }
                field("Customer Nos."; "Customer Nos.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Default i95Dev Customer No Series';
                }
                field("Order Nos."; "Order Nos.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Default i95Dev Sales Order No Series';
                }
                field("i95 Use Item Nos. from E-COM"; "i95 Use Item Nos. from E-COM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies whether Item No. assigned same as E-Commerce';
                    trigger OnValidate()
                    begin
                        If "i95 Use Item Nos. from E-COM" then begin
                            "Product Nos." := '';
                            DefaultProductNosEnable := false
                        end else
                            DefaultProductNosEnable := true;
                    end;
                }
                field("Product Nos."; "Product Nos.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Default i95Dev Product No Series';
                    Enabled = DefaultProductNosEnable;
                }
                field("Schedular ID"; "Schedular ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the i95Dev Schedular ID for Sync';
                    Editable = false;
                }
                field("i95 Item Variant Seperator"; "i95 Item Variant Seperator")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the i95Dev Item Variant Seperator';
                }
                field("i95 Item Variant Pattern 1"; "i95 Item Variant Pattern 1")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the i95Dev Item Variant Type 1.Use Item Variant Separator to seperate each Item attributes.';
                }
                field("i95 Item Variant Pattern 2"; "i95 Item Variant Pattern 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the i95Dev Item Variant Type 2.Use Item Variant Separator to seperate each Item attributes.';
                }
                field("i95 Item Variant Pattern 3"; "i95 Item Variant Pattern 3")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the i95Dev Item Variant Type 3.Use Item Variant Separator to seperate each Item attributes.';
                }
            }
        }
    }

    trigger OnOpenPage()
    begin
        RESET();
        IF NOT GET() THEN BEGIN
            INIT();
            INSERT();
        END;
    end;

    trigger OnAfterGetCurrRecord()
    begin
        APIAuthorizationToken := GetAuthorizationToken();
        AuthTokenEditable := CurrPage.Editable();
        DefaultProductNosEnable := "i95 Use Item Nos. from E-COM";
    end;

    var
        APIAuthorizationToken: Text;
        [InDataSet]
        AuthTokenEditable: Boolean;
        DefaultProductNosEnable: Boolean;
}