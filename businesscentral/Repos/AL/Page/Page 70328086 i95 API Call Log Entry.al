page 70328086 "i95 API Call Log Entry"
{
    PageType = ListPart;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = "i95 API Call Log Entry";
    SourceTableView = ORDER(Ascending);
    Caption = 'i95Dev API Call Log Entries';
    Editable = FALSE;

    layout
    {
        area(Content)
        {
            repeater("API Call Log Entry")
            {
                field("Entry No"; "Entry No")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Entry No.';
                    Style = Attention;
                    StyleExpr = ErrorLine;
                }
                field("Sync Log Entry No"; "Sync Log Entry No")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Sync Log Entry No.';
                    Style = Attention;
                    StyleExpr = ErrorLine;
                }
                field("Sync DateTime"; "Sync DateTime")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Sync Date and Time';
                    Style = Attention;
                    StyleExpr = ErrorLine;
                    Width = 15;
                }
                field("API Type"; "API Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the API Type';
                    Style = Attention;
                    StyleExpr = ErrorLine;
                    Width = 15;
                }
                field("Scheduler Type"; "Scheduler Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Scheduler Type';
                    Style = Attention;
                    StyleExpr = ErrorLine;
                    Width = 15;
                }
                field("Sync Source"; "Sync Source")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Sync source';
                    Style = Attention;
                    StyleExpr = ErrorLine;
                    Width = 15;
                }
                field("HTTP Response Code"; "Http Response Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Http Response Code';
                    Style = Attention;
                    StyleExpr = ErrorLine;
                    Width = 10;
                }
                field("Error Message"; "Error Message")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Error Message';
                    Style = Attention;
                    StyleExpr = ErrorLine;
                    Width = 40;
                }
            }
        }
    }

    var
        ErrorLine: Boolean;
}