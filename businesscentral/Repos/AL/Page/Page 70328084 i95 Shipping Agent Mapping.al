page 70328084 "i95 Shipping Agent Mapping"
{
    ApplicationArea = All;
    Caption = 'i95Dev Shipping Agent Mapping';
    PageType = List;
    SourceTable = "i95 Shipping Agent Mapping";
    UsageCategory = Administration;

    layout
    {
        area(Content)
        {
            repeater(ShippingAgentMApping)
            {
                field("E-Commerce Shipping Method Code"; "E-Com Shipping Method Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies E-Commerce shipping method code.';
                }
                field("E-Commerce Shipping Description"; "E-Com Shipping Description")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies E-Commerce shipping method description.';
                }
                field("BC Shipping Agent Code"; "BC Shipping Agent Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies which BC shipping agent code';
                }
                field("BC Shipping Agent Service Code"; "BC Shipping Agent Service Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the BC shipping agent service';
                }
            }
        }
    }
}