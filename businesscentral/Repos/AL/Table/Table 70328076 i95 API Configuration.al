Table 70328076 "i95 API Configuration"
{
    Caption = 'i95Dev API Configuration';
    fields
    {
        field(1; "API Type"; Option)
        {
            DataClassification = CustomerContent;
            Caption = 'API Type';
            OptionMembers = " ","Product","Customer","CustomerGroup","Inventory","SalesOrder","Shipment","Invoice","TierPrices","CancelOrder","EditOrder","TaxBusPostingGroup","TaxProductPostingGroup","TaxPostingSetup","ConfigurableProduct","CustomerDiscountGroup","ItemDiscountGroup","DiscountPrice","SchedulerId";
            OptionCaption = ' ,Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerId';
        }
        field(2; Description; Text[50])
        {
            DataClassification = CustomerContent;
            Caption = 'Description';
        }
        field(3; "Request Type"; Option)
        {
            DataClassification = CustomerContent;
            Caption = 'Request Type';
            OptionMembers = "POST","GET","DELETE";
            OptionCaption = 'POST, GET, DELETE';
        }
        field(4; "PushData Url"; Text[50])
        {
            DataClassification = CustomerContent;
            Caption = 'PushData Url';
        }
        field(5; "PullResponse Url"; Text[50])
        {
            DataClassification = CustomerContent;
            Caption = 'PullResponse Url';
        }
        field(6; "PullResponseAck Url"; Text[50])
        {
            DataClassification = CustomerContent;
            Caption = 'PullResponseAck Url';
        }
        field(7; "PushResponse Url"; Text[50])
        {
            DataClassification = CustomerContent;
            Caption = 'PushResponse Url';
        }
        field(8; "PullData Url"; Text[50])
        {
            DataClassification = CustomerContent;
            Caption = 'PullData Url';
        }
    }
    keys
    {
        key(Key1; "API Type")
        { }
    }

    procedure Initializei95APIConfiguration()
    begin
        //Insert Product API Config
        InsertAPIConfig("API Type"::Product, 'Product Sync', 'Product/PushData', 'Product/PullResponse', 'Product/PullResponseAck', 'Product/PullData', 'Product/PushResponse');
        //Insert Customer API Config
        InsertAPIConfig("API Type"::Customer, 'Customer Sync', 'Customer/PushData', 'Customer/PullResponse', 'Customer/PullResponseAck', 'Customer/PullData', 'Customer/PushResponse');
        //Insert Customer Group API Config
        InsertAPIConfig("API Type"::CustomerGroup, 'Customer Group Sync', 'CustomerGroup/PushData', 'CustomerGroup/PullResponse', 'CustomerGroup/PullResponseAck', 'CustomerGroup/Pulldata', 'CustomerGroup/PushResponse');
        //Insert Inventory API Config
        InsertAPIConfig("API Type"::Inventory, 'Inventory Sync', 'Inventory/PushData', 'Inventory/PullResponse', 'Inventory/PullResponseAck', '', '');
        //Insert SalesOrder API Config
        InsertAPIConfig("API Type"::SalesOrder, 'Sales Order Sync', 'SalesOrder/PushData', 'SalesOrder/PullResponse', 'SalesOrder/PullResponseAck', 'SalesOrder/PullData', 'SalesOrder/PushResponse');
        //Insert Shipment API Config
        InsertAPIConfig("API Type"::Shipment, 'Sales Shipment Sync', 'Shipment/PushData', 'Shipment/PullResponse', 'Shipment/PullResponseAck', '', '');
        //Insert Invoice API Config
        InsertAPIConfig("API Type"::Invoice, 'Sales Invoice Sync', 'Invoice/PushData', 'Invoice/PullResponse', 'Invoice/PullResponseAck', '', '');
        //Insert TierPrices API Config
        InsertAPIConfig("API Type"::TierPrices, 'Sales Price Sync', 'TierPrices/PushData', 'TierPrices/PullResponse', 'TierPrices/PullResponseAck', '', '');
        //Insert CancelOrder API Config
        InsertAPIConfig("API Type"::CancelOrder, 'Cancel Sales Order Sync', 'cancelorder/PushData', 'cancelorder/PullResponse', 'cancelorder/PullResponseAck', '', '');
        //Insert EditOrder API Config
        InsertAPIConfig("API Type"::EditOrder, 'Edit Order Sync', 'EditOrder/PushData', 'EditOrder/PullResponse', 'EditOrder/PullResponseAck', '', '');
        //Insert TaxBusPostingGroup API Config
        InsertAPIConfig("API Type"::TaxBusPostingGroup, 'Tax Business Posting Group Sync', 'TaxBusPostingGroup/PushData', 'TaxBusPostingGroup/PullResponse', 'TaxBusPostingGroup/PullResponseAck', '', '');
        //Insert TaxProductPostingGroup API Config
        InsertAPIConfig("API Type"::TaxProductPostingGroup, 'Tax Product Posting Group Sync', 'TaxProductPostingGroup/PushData', 'TaxProductPostingGroup/PullResponse', 'TaxProductPostingGroup/PullResponseAck', '', '');
        //Insert TaxPostingSetup API Config
        InsertAPIConfig("API Type"::TaxPostingSetup, 'Tax Posting Setup Sync', 'TaxPostingSetup/PushData', 'TaxPostingSetup/PullResponse', 'TaxPostingSetup/PullResponseAck', '', '');
        //Insert ConfigurableProduct API Config
        InsertAPIConfig("API Type"::ConfigurableProduct, 'Configurable Product Sync', 'ConfigurableProduct/PushData', 'ConfigurableProduct/PullResponse', 'ConfigurableProduct/PullResponseAck', '', '');
        //Insert CustomerDiscountGroup API Config
        InsertAPIConfig("API Type"::CustomerDiscountGroup, 'Customer Discount Group Sync', 'CustomerDiscountGroup/PushData', 'CustomerDiscountGroup/PullResponse', 'CustomerDiscountGroup/PullResponseAck', '', '');
        //Insert ItemDiscountGroup API Config
        InsertAPIConfig("API Type"::ItemDiscountGroup, 'Item Discount Group Sync', 'ItemDiscountGroup/PushData', 'ItemDiscountGroup/PullResponse', 'ItemDiscountGroup/PullResponseAck', '', '');
        //Insert DsicountPrice API Config
        InsertAPIConfig("API Type"::DiscountPrice, 'Discount Price Sync', 'DiscountPrice/PushData', 'DiscountPrice/PullResponse', 'DiscountPrice/PullResponseAck', '', '');
	//Insert SchedulerID API Config
        InsertAPIConfig("API Type"::SchedulerId, 'Pull Scheduler ID', '', '', '', 'Index', '');
    end;

    local procedure InsertAPIConfig(APIType: Option " ",Product,Customer,CustomerGroup,Inventory,SalesOrder,Shipment,Invoice,TierPrices,CancelOrder,EditOrder,TaxBusPostingGroup,TaxProductPostingGroup,TaxPostingSetup,ConfigurableProduct,CustomerDiscountGroup,ItemDiscountGroup,DiscountPrice,SchedulerId; DescriptionTxt: Text; PushDataURL: Text; PullResponseURL: Text; PullResponseACkURL: Text; PullDataURL: Text; PushResponseURL: Text)
    begin
        if not Get(APIType) then begin
            Init();
            "API Type" := APIType;
            Description := CopyStr(DescriptionTxt, 1, 50);
            "PushData Url" := CopyStr(PushDataURL, 1, 50);
            "PullResponse Url" := CopyStr(PullResponseURL, 1, 50);
            "PullResponseAck Url" := CopyStr(PullResponseACkURL, 1, 50);
            "PullData Url" := CopyStr(PullDataURL, 1, 50);
            "PushResponse Url" := CopyStr(PushResponseURL, 1, 50);
            Insert();
        end else begin
            if Description = '' then
                Description := CopyStr(DescriptionTxt, 1, 50);
            if "PushData Url" = '' then
                "PushData Url" := CopyStr(PushDataURL, 1, 50);
            if "PullResponse Url" = '' then
                "PullResponse Url" := CopyStr(PullResponseURL, 1, 50);
            if "PullResponseAck Url" = '' then
                "PullResponseAck Url" := CopyStr(PullResponseACkURL, 1, 50);
            if "PullData Url" = '' then
                "PullData Url" := CopyStr(PullDataURL, 1, 50);
            if "PushResponse Url" = '' then
                "PushResponse Url" := CopyStr(PushResponseURL, 1, 50);
            Modify();
        end;
    end;
}