PageExtension 70328083 "i95 Sales Order Ext" extends "Sales Order"
{
    layout
    {
        addafter("Foreign Trade")
        {
            group(i95)
            {
                Caption = 'i95Dev';
                Visible = Showi95Fields;
                field("i95 Created By"; "i95 Created By")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specified who created the Sales Order';
                    Caption = 'Created By';
                }
                field("i95 Created DateTime"; "i95 Created DateTime")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the date time of creation';
                    Caption = 'Created DateTime';
                }
                field("i95 Creation Source"; "i95 Creation Source")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Source of creation';
                    Caption = 'Creation Source';
                }
                field("i95 Sync Status"; "i95 Sync Status")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the i95Dev sync status';
                    Caption = 'Sync Status';
                }
                field("i95 Last Modified By"; "i95 Last Modified By")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies who last modified the sales order';
                    Caption = 'Last Modified By';
                }
                field("i95 Last Modification DateTime"; "i95 Last Modification DateTime")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the date time of last modification';
                    Caption = 'Last Modification DateTime';
                }
                field("i95 Last Modification Source"; "i95 Last Modification Source")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the source of last modification';
                    Caption = 'Last Modification Source';
                }
                field("i95 Last Sync DateTime"; "i95 Last Sync DateTime")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the date time of last i95Dev sync';
                    Caption = 'Last Sync DateTime';
                }
                field("i95 Reference ID"; "i95 Reference ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the i95Dev Reference ID';
                    Caption = 'Reference ID';
                }
                field("i95 Sync Message"; "i95 Sync Message")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the error message before Sync';
                    Caption = 'Sync Message';
                }
                field("i95 Order Status"; "i95 Order Status")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the order status';
                    Caption = 'Order Status';
                }
                field("i95 EditOrder Sync Status"; "i95 EditOrder Sync Status")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the Sync Status for Edit Order';
                    Caption = 'EditOrder Sync Status';
                }
                field("i95 EditOrder Updated DateTime"; "i95 EditOrder Updated DateTime")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the updated datetime of Edit Order';
                    Caption = 'EditOrder Updated DateTime';
                }
                field("i95 EditOrd Last SyncDateTime"; "i95 EditOrd Last SyncDateTime")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the last Sync DateTime of Edit Order';
                    Caption = 'EditOrder Last Sync DateTime';
                }
            }
        }
        modify("Sell-to Customer No.")
        {
            ShowMandatory = true;
        }
        modify("No.")
        {
            ShowMandatory = true;
        }
    }

    var
        [InDataSet]
        Showi95Fields: Boolean;
        i95SkipOnDelete: Boolean;

    trigger OnOpenPage()
    var
        UserSetup: Record "User Setup";
    begin
        if UserSetup.Get(UserID()) then
            Showi95Fields := UserSetup."i95 Show i95 Data"
    end;

    trigger OnAfterGetCurrRecord()
    begin
        CheckIfi95SyncAllowed();
        if "i95 Reference ID" = '' then
            if "i95 Sync Message" <> '' then
                "i95 Sync Status" := "i95 Sync Status"::"InComplete Data"
            else
                if "i95 Sync Status" > "i95 Sync Status"::"Waiting for Sync" then begin
                    if i95SalesLineModificationExists() then
                        "i95 Sync Status" := "i95 Sync Status"::"Waiting for Sync";
                end else
                    if ("i95 Sync Message" = '') and ("i95 Sync Status" = "i95 Sync Status"::"InComplete Data") then
                        "i95 Sync Status" := "i95 Sync Status"::"Waiting for Sync";
    end;

    trigger OnDeleteRecord(): Boolean
    var
        i95PushWebService: Codeunit "i95 Push Webservice";
        ConfirmDeleteTxt: Label 'Sales Order already Synced. Do you wish to Cancel Order %1 ?';
    begin
        if ("Document Type" = "Document Type"::Order) and ("i95 Reference ID" <> '') then
            if Confirm(StrSubstNo(confirmDeleteTxt, Rec."No."), false) then begin
                i95SkipOnDelete := true;
                i95PushWebService.CreateSyncLogforCancelSalesOrder(Rec);
            end;

        i95SkipOnDelete := false;
    end;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        if ("i95 Reference ID" = '') then begin
            if i95SkipOnDelete then
                exit;

            CheckIfi95SyncAllowed();

            if "i95 Sync Message" <> '' then begin
                "i95 Sync Status" := "i95 Sync Status"::"InComplete Data";
                modify();
            end else
                if "i95 Sync Status" > "i95 Sync Status"::"Waiting for Sync" then begin
                    if i95SalesLineModificationExists() then begin
                        "i95 Sync Status" := "i95 Sync Status"::"Waiting for Sync";
                        modify();
                    end;
                end else
                    if ("i95 Sync Message" = '') and ("i95 Sync Status" = "i95 Sync Status"::"InComplete Data") then begin
                        "i95 Sync Status" := "i95 Sync Status"::"Waiting for Sync";
                        modify();
                    end;
        end;
    end;

}