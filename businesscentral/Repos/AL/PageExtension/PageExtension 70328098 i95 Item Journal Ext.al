pageextension 70328098 "i95 Item Journal Ext" extends "Item Journal"
{
    layout
    {
        addafter("Location Code")
        {
            field("i95 Variant Code"; "Variant Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies Variant Code for the Item Choosed';
                Caption = 'Variant Code';
            }
        }
    }
}