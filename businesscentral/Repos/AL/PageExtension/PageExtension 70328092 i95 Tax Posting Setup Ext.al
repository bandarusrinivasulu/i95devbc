pageextension 70328092 "i95 Tax Posting Setup Ext" extends "VAT Posting Setup"
{
    layout
    {
        addafter("Tax Category")
        {
            field("i95 Created By"; "i95 Created By")
            {
                ApplicationArea = All;
                Visible = Showi95Fields;
                ToolTip = 'Specifies who Created the item ';
                Caption = 'Created By';
            }
            field("i95 Created DateTime"; "i95 Created DateTime")
            {
                ApplicationArea = All;
                Visible = Showi95Fields;
                ToolTip = 'Specifies the Created Date Time';
                Caption = 'Created DateTime';
            }
            field("i95 Last Modification DateTime"; "i95 Last Modification DateTime")
            {
                ApplicationArea = All;
                Visible = Showi95Fields;
                ToolTip = 'Specifies the date time of last modification';
                Caption = 'Last Modification DateTime';
            }
            field("i95 Sync Status"; "i95 Sync Status")
            {
                ApplicationArea = All;
                Visible = Showi95Fields;
                ToolTip = 'Specifies the i95Dev Sync Status';
                Caption = 'i95Dev Sync Status';
            }
            field("i95 Last Sync DateTime"; "i95 Last Sync DateTime")
            {
                ApplicationArea = All;
                Visible = Showi95Fields;
                ToolTip = 'Specifies the date time of last sync';
                Caption = 'Last Sync DateTime';
            }
            field("i95 Reference ID"; "i95 Reference ID")
            {
                ApplicationArea = All;
                Visible = Showi95Fields;
                ToolTip = 'Specifies the i95Dev Reference ID';
                Caption = 'Reference ID';
            }
        }
        modify("VAT Bus. Posting Group")
        {
            ShowMandatory = true;
        }
        modify("VAT Prod. Posting Group")
        {
            ShowMandatory = true;
        }
    }

    var
        [InDataSet]
        Showi95Fields: Boolean;

    trigger OnOpenPage();
    var
        UserSetup: Record "User Setup";
    begin
        if UserSetup.Get(UserId()) then
            Showi95Fields := UserSetup."i95 Show i95 Data";
    end;
}